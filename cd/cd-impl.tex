One particular problem with \emph{scope analysis} presented earlier is that, because of R's semantics,
any instruction that evaluates potentially effectful code taints the
abstract environment.
There are two kinds of non-local effects: callees may
affect functions upwards the call stack by accessing the caller's
environment, and callers pass promises that may affect functions downwards
the call stack, when those functions force the promises.
To make scope resolution useful in practice, the impact of these
non-local effects should be somewhat mitigated. For instance, we
rely on inlining to reduce the number of \Call and \Force instructions.
There is also the possibility to speculatively assume calls do not change
the environment using stub environments.

However, promises are a main source of imprecision when \R tries to analyze R
code. When a function argument is accessed for the first time the optimizer must
be able to rule out non-local effects to the current function. To that end we
compile versions of functions under the assumption that already evaluated
values, or at least non-reflective promises are passed to it. This
specialization, among many others, is
implemented using \cd. Given the complexity
of function calls in R, our design focuses on properties that can optimize
the function call sequence and allow the compiler to generate better code
within the function.

\subsubsection{Contexts}
The goal of \cd is to drive optimizations. Accordingly, we
design contexts in \R mainly driven by the seven headaches for optimizing R
introduced in \autoref{sec:headache}.
Contexts are represented by the \ccode{Context} structure presented in
\autoref{lst:cont}, which consists of two bit vectors (\ccode{argFlags} and
\ccode{flags}) and a byte (\ccode{missing}).  The whole
structure fits within 64 bits, with two bytes (\ccode{unused}) reserved for future use.  The
\ccode{EnumSet} class is a set whose values are chosen from an enumeration.

\begin{figure}
\centering
\begin{lstlisting}[caption={Context data structure},
  label={lst:cont}, style=Cpp]
enum class ArgAssumption {
  Arg0Eager,     ..., Arg7Eager,
  Arg0NotObj,     ..., Arg7NotObj,
  Arg0SimpleInt, ..., Arg7SimpleInt,
  Arg0SimpleReal, ..., Arg7SimpleReal,
};
enum class Assumption {
  NoExplicitlyMissingArgs, CorrectArgOrder, NotTooManyArgs, NoReflectiveArg,
};
struct Context {
  EnumSet<ArgAssumption, uint32_t> argFlags;
  EnumSet<Assumption, uint8_t> flags;
  uint8_t missing = 0;
  int16_t unused = 0;
};
\end{lstlisting}
\end{figure}

More specifically, \ccode{argFlags} is the conjunction of argument predicates
(\ccode{ArgAssumption}) for the first eight arguments of a function.  For each
argument position \ccode{N < 8}, we store whether the argument has already been
evaluated (\ccode{ArgNEager}), whether the argument is not an object, \ie it does not
have a \c{class} attribute (\ccode{ArgNNotObj}), whether the value is a scalar integer with no
attributes (\ccode{ArgNSimpleInt}), and whether the value is a scalar double with
no attributes (\ccode{ArgNSimpleReal}).  Any subsequent arguments will not be
specialized for. The limit is informed by data obtained by \citet{ecoop12},
suggesting that the majority of frequently called functions have no more
than three arguments and that most arguments are passed by position.

The \ccode{flags} field is a set of \ccode{Assumption} values that summarize
information about the whole invocation. The majority of the predicates are
related to argument matching. In R, the process of determining which actual
argument matches with formal parameters is surprisingly complex.
The GNU R interpreter does this by
performing three traversals of a linked list for each function call.  The \R
compiler tries to do this at compile time, but some of the gnarly corners of
R get in the way. For this reason, contexts encode information about the order
of arguments at the call site.  Thus \ccode{flags} has a predicate,
\ccode{NoExplicitlyMissingArgs}, to assert whether any of the arguments is
\emph{explicitly} missing.  This matches in three cases: when an argument is explicitly
omitted (\code{add(,2)}), when an argument is skipped by matching
(\code{add(y=2)}), and when a call site has more missing arguments than
expected in the compiled code.
\ccode{CorrectArgOrder} holds if the
arguments are passed in the order expected by the callee.
\ccode{NotTooManyArgs} holds if the number of arguments passed is less than or
equal to the number of parameters of the called function.
\ccode{NoReflectiveArg} holds if none of the arguments invoke reflective
functions.
Finally, missing arguments that occur at the end of an argument list are treated
specially; \ccode{missing} records the number of \emph{trailing} missing arguments (up to
255).


\subsubsection{Ordering}
Recall that contexts have a computable partial order, which is used to determine
if a function version can be invoked at a particular program state. For example,
let $C'$ be the current context of program state $S$, $F$ be a function invoked
at $S$, and $\langle C, V \rangle$ be a version in $F$. Then the implementation
can dispatch to $\langle C, V \rangle$ if $C' < C$.

In \R, the order between contexts, $C' < C$ , is defined mainly by set inclusion
of both assumption sets. For trailing missing arguments, there are two cases
that need to be considered.
%
First, if $C$ assumes \ccode{NotTooManyArgs}, then $C'$ must have at least as
many trailing missing arguments as $C$. Otherwise, this implies $C'$ has
more arguments than $C$ expects, contradicting \ccode{NotTooManyArgs}.
%
Second, if context $C$ allows any argument to be missing,
then it entails a context $C'$ with fewer trailing missing arguments (\ie more
arguments). The reason is that \emph{missing arguments} can be passed
as explicitly missing arguments, reified by an explicit marker value for
missing arguments. If we invert that property, it means that a context
with \ccode{NoExplicitlyMissingArgs} does not accept more trailing missing
arguments.

\begin{figure}\begin{lstlisting}[caption={Implementation of Context ordering},
      label={lst:ord}, style=Cpp
]
bool Context::smaller(const Context& other) const {
  // argdiff positive = "more than expected",
  //         negative = "less than"
  int argdiff = (int)other.missing - (int)missing;
  if (argdiff > 0 &&
      other.flags.contains(Assumption::NotTooManyArgs))
    return false;
  if (argdiff < 0 &&
      other.flags.contains(Assumption::NoExplicitlyMissingArgs))
    return false;
  return flags.includes(other.flags) &&
         typeFlags.includes(other.typeFlags);
}
\end{lstlisting}\end{figure}

\begin{figure}
\centering
\small
\ifx\HCode\undefined
\tikzstyle{level 1}=[level distance=20mm]
\tikzstyle{level 2}=[level distance=20mm, sibling distance=20mm]
\tikzstyle{level 3}=[level distance=30mm, sibling distance=35mm]
\begin{tikzpicture}[grow=left,->,>=angle 60]
    \node {$\top = \emptyset$}
    child {node {\ccode{Arg0Eager}}
      child {node{\ccode{Arg0Eager,Arg1Eager}}
      }
      child {node {\ccode{Arg0Eager, missing=1}}
        child[level distance=40mm, sibling distance=20mm] {node[text width=3.2cm]
        {\ccode{Arg0Eager, missing=1, NoExplicitlyMissingArgs}}
        }
        child[sibling distance=20mm] {node{\ccode{Arg0Eager, missing=2}}
        }
      }
    };
\end{tikzpicture}
\else
  \includegraphics{figs/cd-order.png}
\fi
\caption{An example for the order of some Contexts}\label{fig:ord-example}
\end{figure}

Some example contexts with their order relation (increasing from left to right) are shown in
\autoref{fig:ord-example}. Contexts with more flags are smaller, contexts with
a greater \ccode{missing} value are smaller, and contexts with \ccode{NoExplicitlyMissingArgs}
require the same number of missing arguments to be comparable.
The comparison is implemented by the code of
\autoref{lst:ord}.  Excluding \c{mov} and \c{nop} instructions, the \ccode{smaller}
comparison is compiled to fewer than 20 x86 instructions by GCC 8.4.

\subsubsection{Evaluating Contexts}

For every call the current context is needed, which is partially computed
statically and completed dynamically. The \R optimizer enables static
approximation of many of the assumptions. For example, laziness and
whether values might be objects are both represented in the type system of its
IR. Therefore, those assumptions can sometimes be precomputed. Call sites with
varargs passed typically resist static analysis. 
On the other hand, \ccode{NoExplicitlyMissingArgs}, \ccode{CorrectArgOrder}, and
\ccode{NotTooManyArgs} are static assumptions for call sites without named
arguments, or if the call target is known and the argument matching can be done
statically. Similarly, the number of missing trailing arguments are only
statically known if the call target is static.

\ccode{NoReflectiveArg} is the most interesting assumption in terms of its
computation.
Since the context has to be computed for every dispatch, the time budget is very
tight. Therefore, this assumption is only set dynamically if all arguments
are eager, which is a very conservative over-approximation.
However, we perform a static analysis on the promises to detect
benign ones, which do not perform reflection.
This shows that even computationally heavy assumptions can be approximated by a
combination of static and dynamic checks.

The static context is computed at compile time and added to every call
instruction. At call time, a primitive function implemented in C++
supplements all assumptions which are not statically provided. This seems
like a gross inefficiency---given the static context, the compiler could
for each call site generate a specific and minimal check sequence.  We plan
to add this optimization in the future. So far we have observed the overhead
of computing the context to be small compared with the rest of the call
overhead.

\subsubsection{Dispatch Operation}

All the versions of the same function are kept in a \emph{dispatch table}
structure, a list of versions sorted by increasing
contexts. Versions with smaller contexts (\ie with more assumptions) are found
at the front. To that end we
extend the partial order of contexts to a total order: if two
contexts are not comparable then the order is defined by their bit patterns.

\autoref{lst:dispatch} shows a
pseudocode implementation of the dispatching mechanism.
Dispatching is performed by a linear search for the first matching
context (see \autoref{lst:ord}).
The result of a dispatching operation can be cached, since given the same
dispatch table and context, the result is deterministic.
If the context of the dispatched version is strictly larger than the current
context, it means that there is still an opportunity to further specialize.
We rely on a counter based heuristic to trigger the optimizer. At the time of
writing, dispatch tables are
limited to 15 elements; to insert an entry into a full table, a
random entry (except the first) is evicted.

\begin{figure}
\begin{lstlisting}[style=Cpp,
    caption={Dispatching to function versions under the current context},
    label={lst:dispatch},
]
Version* dispatch(Context staticCtx, Cache* ic, DispatchTable* dt) {
  Context cc = computeCurrentContext(staticCtx);
  if (ic->dt == dt && ic->context == cc) 
    return ic->target;  // Cache hit
  Version* res = dt->find([&](Version* v){ return cc.smaller(v->context); });
  // Maybe compile a better version
  if (res->context != cc && jitThresholdReached(res))
    res = optimize(dt, res, cc);
  updateCache(ic, dt, res, cc);
  return res;
}
\end{lstlisting}
\end{figure}

\subsubsection{Optimization under Assumptions}\label{sec:opts}

Below, we briefly illustrate some optimizations relying on the contextual assumptions introduced
previously.
For the following examples, it is important to remember that values in PIR
(as in R) can be lazy. When a value is used in an eager operation,
it needs to be evaluated first, by the \Force instruction.
PIR uses numbered registers to refer to the result of instructions.
Those are not to be confused with source-level R variables, which must be
stored in first-class environments. Environments are also first-class entities in PIR, represented by the \MkEnv
instruction.

As a simple example, consider the R expression \c{f(1)} which calls the
function \c{f} with the constant \c{1}. This translates to the following PIR instructions:
\vskip -1em
\OttLstA
\vskip -1em

\noindent
The first instruction loads a function called f from the global
environment. The second instruction loads the constant argument, which is a
unitary vector \c{[1]}. This instruction has type
integer and additionally the value is known to be scalar ($\$$) and eager
($\sim$). The third instruction is the actual call. The static context for this call
contains the \ccode{Arg0SimpleInt} and \ccode{Arg0Eager} flags. Assuming the call target is
unknown, the result can be lazy and of any type.

As an example, consider
a call \c{f(1)} which invokes the identity function, \c{function(x) x}. This reads as follows in
PIR:
\vskip -1em
\OttLstB
\vskip -1em

\noindent
The first instruction \LdArg loads the first argument.
In general, the arguments can be
passed in different orders, and the presence of varargs might further complicate
matters. However, all functions optimized using PIR are compiled under the
\ccode{CorrectArgOrder} assumption. This allows us to refer to arguments by
their position, since it is now the caller's responsibility to reorder arguments
as expected.
Additionally, R variables are stored in first-class environments and GNU R has a calling convention
where the environment with bound arguments is created by the caller.
In \R it is created by the callee by the \MkEnv instruction. In this example
the name \c{x} is bound to the first argument and the global
environment is the parent. The later means that this closure was defined at the
top level.
Finally, the argument, which is a promise, is evaluated to a value (indicated by the $\sim$
annotation) by the \Force instruction and then returned. Overall, the
\ccode{CorrectArgOrder} assumption enables a calling convention where the callee
creates the environment, which is the first step towards eliding it completely.

In our example we notice that the environment is kept alive by a dependency from
the \Force instruction.
\vskip -1em
\OttLstC
\vskip -1em

\noindent
While \Force does not access the environment directly,
the forced promise can cause arbitrary effects, including
reflective access to the local environment of the function. To ensure that the
environment is not tainted, reflective access has to be excluded. To that end
the \ccode{NoReflectiveArg} assumption asserts that no argument will invoke a
reflective function and thus allows the compiler to remove the dependency.
Since the dependency was the only use of the local
environment, it can be completely removed.

At this point the \Force instruction is still effectful. However, if we can show that the input is
eager, then the \Force does nothing.
Under the \ccode{Arg0Eager} assumption, we know the first argument is
evaluated and therefore the \Force instruction can be removed.

\vskip -1em
\OttLstD
\vskip -1em

\noindent
In summary, if we establish the three assumptions \ccode{CorrectArgOrder}
\ccode{NoReflectiveArg} and \ccode{Arg0Eager} we conclude that the function
implements the identity function.

Another problem we target with \cd is R's argument matching.  Consider the
following \c{function(a1, a2=a1) \{a2\}}, which has a default expression for
its second argument.  This function translates to the following PIR:
\OttLstE

\noindent
As
can be seen, the second argument must be explicitly checked against the
missing marker value.  The default argument implies that we must dynamically
check the presence of the second argument and then evaluate either \c{a1} or
\c{a2} at the correct location. Default arguments are, like normal
arguments, evaluated by need.

Optimized under a context where the last trailing argument is missing, this
test can be statically removed.
With this optimization, basic block 2 is
unreachable.
\OttLstF

\noindent
Note that as in the simpler example before,
under the additional assumption \ccode{Arg0Eager}, the \Force
instruction and the local environment can be statically elided and the closure
does not need an R environment.

\smallskip

Almost all of these specializations can also be applied using speculative
optimizations. For instance, the previous example could be speculatively
optimized as follows:
\OttLstG

\noindent
instead of contextual assumptions, the speculative assumptions that \c{a1} is
missing and that \c{a0} is eager are explicitly tested. The \Assume instruction
guards assumptions and triggers deoptimization through the last checkpoint.
Creation of the local environment is delayed and only happens in case of a
deoptimization. As can be seen here, the need to materialize a local environment
on deoptimization is a burden on the optimizer. Additionally, this method does
not allow us to specialize for different calling contexts separately. However,
there are of course many instances where speculative optimizations are required,
since the property of interest cannot be checked at dispatch time. For
instance, the value of a global binding might change between function entry and
the position where speculation is required.
