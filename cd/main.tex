%% \renewcommand{\ULdepth}{1.8pt}
%% \contourlength{0.8pt}

\newcommand{\MkEnv}{\ensuremath{\pirI{MkEnv}}\xspace}
\newcommand{\MkArg}{\ensuremath{\pirI{MkArg}}\xspace}
\newcommand{\Call}{\ensuremath{\pirI{Call}}\xspace}
\newcommand{\MkClosure}{\ensuremath{\pirI{MkClosure}}\xspace}
\newcommand{\StVar}{\ensuremath{\pirI{StVar}}\xspace}
\newcommand{\LdVar}{\ensuremath{\pirI{LdVar}}\xspace}
\newcommand{\LdArg}{\ensuremath{\pirI{LdArg}}\xspace}
\newcommand{\Force}{\ensuremath{\pirI{Force}}\xspace}
\renewcommand{\Phi}{\ensuremath{\pirI{Phi}}\xspace}
\newcommand{\Framestate}{\ensuremath{\pirI{Framestate}}\xspace}
\newcommand{\Deopt}{\ensuremath{\pirI{Deopt}}\xspace}
\newcommand{\Checkpoint}{\ensuremath{\pirI{Checkpoint}}\xspace}
\newcommand{\LdFun}{\ensuremath{\pirI{LdFun}}\xspace}
\newcommand{\LdConst}{\ensuremath{\pirI{LdConst}}\xspace}
\newcommand{\Branch}{\ensuremath{\pirI{Branch}}\xspace}
\newcommand{\get}[3]{\ensuremath{#1_{#2,#3}}\xspace}

%% \renewcommand{\ottpremise}[1]{\premiseSTY{#1}}
%% \renewcommand{\ottusedrule}[1]{\usedruleSTY{#1}}
%% \renewcommand{\ottdrule}[4][]{\druleSTY[#1]{#2}{#3}{#4}}
%% \renewenvironment{ottdefnblock}[3][]{\defnblockSTY[#1]{#2}{#3}}{\enddefnblockSTY}
%% \renewcommand{\c}[1]{{\lstinline[style=Rin]!#1!}\xspace}
%% 
%% \renewcommand{\c}[1]{{\lstinline[style=Rin]!#1!}\xspace}

\newcommand{\PIR}{\emph{PIR}\xspace}
\renewcommand{\c}[1]{{\lstinline[style=Rin]!#1!}\xspace}
%% \newcommand{\code}[1]{{\lstinline[style=Rin]!#1!}\xspace}

%% Just-in-time compilers are omnipresent in todays technology stacks and the
%% performance of the code they generate is central to the growing adoption of
%% dynamic languages.  That performance is increasingly dependent on
%% sophisticated on-line optimizations. Compilers specialize programs according to
%% observed behaviors, identify likely invariants, such as the types of the
%% arguments of a given function, and generate code that leverages those
%% invariants.

After discussing the \assumemodel for speculation and deoptimization, we turn our attention
to specialization by splitting.
In our experience, to achieve performance for dynamic languages,
a compiler needs information about the calling context of a function. This
can be information about the type and shape of the function's arguments,
or any other predicates about
program state that hold when the function is invoked.  We have observed that
for instance inlining
and speculation
work well together to expose that contextual
information to the optimizer. Inlining allows to optimize the body of a
function together with its arguments and speculation is needed to enable
inlining for dynamic calls.
The drawbacks of this approach are that inlining grows the size of compilation
units, that speculation may fail causing the compiled code to be discarded, and
that the caller needs to be recompiled to support a new dynamic context.


This chapter presents an approach to structure a just-in-time compiler
to better leverage information available at run time. Our starting point
is a compiler for a dynamic language geared to perform run-time
specialization: it uses speculative optimizations, guesses about
potential invariants, and deoptimizes functions if any of their
assumptions fails to hold.
%% In addition, our baseline also performs optimizations such as
%% dead code elimination, loop unrolling, and function inlining.  Low-level
%% code transformations are outsourced to a highly optimizing back-end
%% compiler.
Our goal
is to extend this baseline compiler with a new technique called \cd that
provides contextual information by specializing functions for different
calling contexts. For every call, one of the best versions given the current state
of the system is invoked. A key property of \cd is that it allows us
to combine static and dynamic analysis for optimizations.

\begin{figure}
  \centering
  \includegraphics[width=0.6\columnwidth]{figures/cd.png}
  \caption{Specialization}
  \label{fig:jit-intro-cd}
\end{figure}

When specializing to contextual information at the call-site, we get a graph as shown in
\autoref{fig:jit-intro-cd}.
At the call-site differently optimized versions can be selected. The choice
happens at the time of the call.
In contrast to \autoref{fig:jit-intro-deopt},
where the speculatively optimized version is always called and when the guard
fails it is discarded.\footnote{Of course the two can be combined, having specialized
versions, which also include speculation.}
At this abstract level we already observe one of the main trade-offs
between the two approaches. Specialization implies code duplication. If we insert multiple
splits, or additionally perform tail-duplication,
this quickly leads to a path explosion problem, as can be observed for instance in tracing
JITs. This is not an issue with speculative optimizations. Typically the optimized code is
orders of magnitude smaller than the source, since all the unlikely behavior can be trimmed.
As we have seen, the drawback of
speculation is that it can be costly, because the optimized code needs to be discarded and then
re-compiled again, possibly several times, until all assumptions are stable.
Also it does not allow us to narrowly specialize to two different contexts at
the same time.

%% Slight REWRITE: -- removed self-ref, reformulated intro, and added
%%                      discussion of ordering and default version/ Jan
The inspiration for the approach in this chapter comes from customized compilation, pioneered
by~\citet{cha89}, an optimization that systematically specializes functions
to the dynamic type of their arguments.  We extend this approach by
specializing functions to arbitrary contexts and dynamically selecting
optimized versions of a specialized function depending on the run-time state
of the program.
%% Polymorphic inline caches also dynamically select an optimized version depending on the run-time state(type)
We refer to the proposed approach as \emph{\cd}, since at its heart, it
describes how to efficiently select a specialized version of a code fragment,
given a particular program state as context.

As an illustration, consider \autoref{lst:egone} written in R. The semantics
\begin{figure}[h]
\begin{lstlisting}[caption={\c{max} function},label={lst:egone},style=R]
max <- function(a, b=a, warning=F) {
  if (warning && any(is.na(c(a,b))))
    warn("NA Value")
  if (a < b) b else a
}
max(x) + max(y,0)
\end{lstlisting}
\end{figure}
of R is complex: functions can be invoked with optionally named arguments that can be reordered and omitted. Furthermore, arguments are
lazy and their evaluation (when the value is needed) can modify any
value, including function definitions.  In the above example, the \c{max}
function is expected to return the largest of its first two parameters,
mindful of the presence of missing values (denoted \c{NA} in R). The third,
optional, parameter is used to decide whether to print a warning in case a
missing value is observed. If \c{max} is passed a single argument, it
behaves as the identity function. Since R is a vectorized language, the arguments of
\c{max} can be vectors of any of the base numeric types of the language.
Consequently, compiling this function for all possible calling contexts is
likely to yield inefficient code.

\CD is motivated by the observation that, for any execution
of a program, there are only a limited, and often small, number of different
calling contexts for any given function.  For example, if \c{max(y,0)} and
\c{max(x)} are the only calls to \c{max}, then we may generate two versions
of that function: one optimized for a single argument and the other for
two. Further specialization can happen on the type and shape of the first
argument, this may either be a scalar or a vector of one of the numeric
types.  This shows that part of a context can be determined statically, \eg
the number of arguments, but other elements are only known at run time, \eg
the type and shape of arguments. \CD thus, in general,
requires run-time selection of an applicable call target.

We define a \emph{context} to be a predicate on the
program state, chosen such that there exists an efficiently computable partial
order between contexts and a distinguished maximal element. \CD provides a
framework on how to incorporate dynamic information. Which properties make up a
context and how they are represented or implemented is up
to the concrete implementation. We will present a number of options here and also
detail \R's implementation in \autoref{chpt:implementation}. The contexts have
to be efficiently comparable, since the comparison is used for dispatching.

A \emph{version}
of a function is an instance of that function compiled under the assumption
that a given context holds at entry. The idea is for the compiler to be able to
use predicates from the context for optimizations. For instance if the context
declares that it is Thursday and the compiler can prove that the function
terminates within 24 hours, then a call to \code{isWeekend()} can be replaced by
\code{false}.

To leverage versions the compiler emits a \emph{dispatch} sequence that computes the call
site context and invokes a version of the target function that most closely
matches the calling context. The dispatch, which can use a combination of static
and dynamic information, ensures that a good candidate version is invoked, given
the current program state.
The unoptimized baseline version of the function is
associated to the maximal context and is the default version that will be
called when no other applies.
% Does it makes sense that after optimizations/inlining this dispatch only exists mostly for "polymorphic functions"?
%% REWRITE/


We evaluated \cd in the context of \R and, as will be discussed extensively
later, found it to
significantly improve the performance of several benchmark programs with negligible regressions.
We consider R an interesting host to study compiler optimizations because of
the challenges it presents to language implementers.  However, \cd
is not specific to R.  We believe that the approach carries over to
other dynamic languages such as JavaScript or Python. In more static languages
\cd could be employed to propagate inter-procedural information, such as a dynamic escape analysis.  Moreover, we
emphasize that \cd is not a replacement for other optimization techniques; instead, it is synergistic.

\subsection{Compared to Other Techniques}

With \cd we provide the means to keep several differently specialized function versions
and then dynamically select a good candidate, given the dynamic context at the
call-site.
To gain some intuition, let us revisit the example of \autoref{lst:egone}
and contrast speculation, inlining and \cd. \autoref{fig:com} shows
\emph{idealized} compilation of that code. On the left we observe the two call-sites to the
\c{max} function. In the first case, since both callers omit the
\lstinline{warning} parameter, a compiler could speculatively optimize it
away, by leaving an \c{assume} to catch calls that pass the third
argument. However any unrelated call-site in the program could invalidate this
assumption and undo the speculative optimization for all callers simultaneously.
Second, inlining allows us to specialize the \c{max} function for each call
site independently. In R, inlining is generally unsound as functions can be
redefined by reflection. Therefore the assumption of a static target for the inlined
call-site is a speculative optimization. Also, inlining increases code size,
reduces sharing and is limited to specialize on statically known information. For instance in
\code{max(deserialize(readline()), 1)}, the argument type is dynamic and inlining
does not allow us to specialize for it.

\begin{figure}
  \centering
  \ifx\HCode\undefined
    \resizebox{1\textwidth}{!}{\includegraphics{figs/cd-example.tikz}}
  \else
    \includegraphics{figs/cd-example.png}
  \fi
  \caption{Speculation, Inlining and \CD}\label{fig:com}
\end{figure}

In contrast, as depicted in the last example in \autoref{fig:com}, \cd allows the compiler to create two additional versions of the target
function, one for each calling context. At run-time the dispatch mechanism
compares the information available at the call-site with the
required contexts of each available version and dynamically dispatches to one of them.
The types and length of \c{x} and \c{y} can generally not be inferred from the source code, but
can be checked at run-time. \CD consists of first approximating a current
context $C$. For instance if \c{x} is a scalar integer,
then at the call-site \code{max(x)}, where just one argument is passed, a current context \code{Integer[1]},
representing these facts, is established at runtime.
Given $C$, a target version with a compatible context $C'$ is
invoked. In our example the context \code{Eager,Missing,Missing} is
chosen. If no compatible specialized version is available, then we dispatch to
the original version of the function.
Compatibility is expressed in terms of ordering: contexts form a partial order,
such that any smaller context logically entails
the bigger one. Intuitively, a larger context matches more program states.
In other words, a function version can be invoked if its context is bigger
than the current one, \ie $C < C'$.

The target might not be unique. There might be several optimal compatible
versions, as we will discuss later.
In our implementation a context is an actual data structure
with an efficient representation and comparison. For each call-site the compiler
infers a static approximation for the upper bound of the current context.
Additional dynamic checks further concretize the approximation and create a
smaller current context.
This dynamically inferred context has two uses. First, as described, it serves as the
lower bound when searching for a target version of a particular function.
Secondly, if no good
approximation is found, then the current context serves as the assumption context to
compile a fresh version to be added to the function.

\begin{figure}
  \centering
  \ifx\HCode\undefined
    \resizebox{1\textwidth}{!}{\includegraphics{figs/cd-example-split.tikz}}
  \else
    \includegraphics{figs/cd-example-split.png}
  \fi
  \caption{Splitting}\label{fig:com2}
\end{figure}

\CD shares some similarities with splitting, as depicted in \autoref{fig:com2}.
Specialized clones of functions are created, for instance here
\code{max2} is a copy of \code{max} which takes two arguments. Additionally, if
there are multiple static candidates, then those are disambiguated at runtime
by rewriting the call-site into a fixed sequence of branches.
In this example we test for the length and in case of \c{1}
call the copy \code{max2s}, specialized to receiving two scalar arguments.
However, the specialization happens at compile-time and cannot be extended without
recompilation.
All those four techniques can be easily combined. For instance the performance
of inlining can be improved by inlining an already optimized version using a
static approximation of \cd. Or statically known candidates of likely contexts
can be used statically by splitting on contexts.

\section{Related Work}

There is an abundance of code specialization techniques, besides speculation.
The common theme here is that the compiler tries to
identify common contexts, which can include dynamic types of arguments, values of local variables, but also
meta-properties, such as whether an object escaped from a thread, and so on. Then a piece of code,
often a whole function, is cloned and optimized for that context.
Overall, keeping specialized
copies of the same function is a well-known optimization technique.
Existing approaches perform the selection by, either, piggy-backing
onto existing dispatching mechanisms in the VM, by implementing an ad-hoc
and fragmented approach, or by statically splitting at each call site.

\paragraph{Inlining} This powerful optimization has been used in static
languages for over forty years~\citep{sch77}. Replacing a call to
a function with its body has several benefits: it exposes the calling
context thus allowing the compiler to optimize the inlined function, it
enables optimizations in the caller, and it removes the function call
overhead.
In dynamic languages, function calls are usually expensive, so inlining is
particularly beneficial.
The limitations of inlining are related to code growth:
compilation time increases and cache locality may be negatively impacted.

\paragraph{Customization} \citet{cha89} describe
customized compilation as the compilation of several copies of a method,
each customized for one receiver type.
Method dispatch ensures that the correct version is invoked. This idea of keeping several customized
versions of a function is generalized in the Jalape\~{n}o VM, which
specializes methods to the types and values of arguments, static fields, and
object fields~\citep{wha99}. Some specialization is enabled by static
analysis, some by dynamic checks.
The Julia compiler specializes functions
on all argument types and uses multimethod dispatch to ensure the proper
version is invoked~\citep{bez18}.
In that sense the dispatching is used in two different settings, once user-facing,
allowing users to override functions for particular signatures, with different
behavior, but then also by the compiler for specializing the same function to different signatures.
An interesting generalization of multimethod dispatch is proposed by \citet{ern98},
who generalize multimethod dispatch to arbitrary Boolean properties.
But the focus lies on exposing the dispatching to users, thus statically ensuring that
method selection is unambiguous and complete.

%%% EXTENSION -- specializing to erased types
\citet{ken01} present dynamic specialization for
parametrized types in the intermediate representation of the .NET virtual
machine; similarly for Java by \citet{car98}, or using user-guidance by \citet{dra09}.
%%% EXTENSION -- specialization to values
\citet{cos13} specialize on arbitrary values for JavaScript functions with
a singular calling context and
%%% EXTENSION -- combining static + dynamic analysis
\citet{hac12} introduce a type-based customization which combines dynamic and
static information.
\citet{liu19} propose to specialize methods
under a dynamic thread-escaping analysis to have lock-free versions of
methods for thread-local receivers.
\citet{hos90} argue for customized compilation of
persistent programs to specialize code based on assumptions about the
residency of their arguments.

%%% EXTENSION -- specialization in aot
For ahead-of-time compilers, \citet{hal91} introduces method cloning,
for instance to support inter-procedural constant propagation.
Many similar context-sensitive optimization approaches follow, \eg by
\citet{coo93}. \citet{ple95} use a static technique to partition instances and
calling contexts, such that more specialized methods can be compiled and then
dynamically invoked.
\citet{ari17} present dynamic specialization to concrete
arguments using dynamic binary rewriting.
\citet{poe20} introduce technique to reduce the number of clones, when applying context-sensitive optimizations in
the presence of longer call strings.
\citet{dea95a} propose to limit overspecialization by specializing
to sets of types.
Overall, keeping customized
copies of the same function is a well-known optimization technique.
In contrast to existing techniques, the contribution of \cd
is to provide a unified framework for implementing all customization needs in a JIT, and
support sharing of compatible versions among different contexts.

%%% EXTENSION -- distinguish splitting/IC from cloning
\paragraph{Splitting}
Whereas customization duplicates methods, \citet{cha89} also introduce
splitting to duplicate call-sites. Control flow is split on the receiver,
causing each branch to contain a statically resolved call-site. This transformation
effectively pulls the target method selection out of the dispatch sequence and embeds
it into the caller, thereby exposing it to the optimizer, which leads to further
optimization opportunities. For instance
the combination with tail-duplication allows SELF to statically
resolve repeated calls to the same receivers. Splitting is a common optimization
in ahead-of-time compilers, for instance LLVM~\citep{lat04} has a pass to split call-sites to specialize for non-null
arguments.  In a dynamic language splitting can be thought of as the frozen
version of a polymorphic inline cache~\citep{hol91}. Both, inline caches and splitting,
are orthogonal to \cd.  \R uses (external) caches for the
targets of \cd and we could use splitting to split
call-sites for statically specializing to the most commonly observed
contexts.

Only few approaches besides trivial tail-duplication perform specialization at a lower granularity than a whole function.
A noteworthy exception is the work by \citet{che15}, where dispatch happens at the basic block level. The approach was later adopted in the
HHVM compiler \citet{gui18}.
In trace based approaches there is a related problem of stitching traces, where identical regions of different traces are
to be identified and de-duplicated \citep{gal09, hak12}.

\section{Context Dispatch in a Nutshell}

This section provides a precise definition of contexts and \cd.
Then, we present a more detailed account on the performance trade-offs.
The actual instance of \cd as implemented in \R is detailed in
\autoref{chpt:implementation}.
We envision a number of possible implementations of \cd. The following
provides a general framework for the approach and defines key concepts.

\paragraph{Context} Contexts $C$ are predicates over program
states $S$, with an efficiently computable, reflexive partial order defined as $C_1 <
C_2~\mathit{iff} \ \forall \; S : C_1(S)\Rightarrow C_2(S)$, \ie $C_1$ entails $C_2$.
For instance given $C_I\equiv\; $\code{type(arg0)==int} and
$C_N\equiv\; $\code{type(arg0)==num}, then
$C_I < C_N$ since every integer is also a number.
Let $\top$ be
the context that is always true; it follows that $C < \top$ for all contexts
$C$.

\paragraph{Current Context} A context is called current with respect to a
state $S$ if $C(S)$ holds. For instance if currently \code{arg0} contains
the integer $3$, then $C_I$, $C_N$, and $\top$ are all current.

\paragraph{Version} $\langle C, V \rangle$ is called a version,
where $V$ is code optimized under the assumption that $C$ holds at entry. A
function is a set of versions including $\langle \top, V_u \rangle$,
where $V_u$ is the baseline version, \ie compiled without contextual assumptions.

\paragraph{Dispatch} To invoke a function $F$ in state $S$, the implementation
chooses any current context $C'$ (with respect to S) and a version $\langle
C,V \rangle \in F$ such that $C'<C$ and transfers control to $V$. For instance
if the current context is $C_N \equiv\; $\code{type(arg0)==num}, then $\langle C_N, V \rangle$ is reachable by dispatch, but
$\langle C_I, V \rangle$ where $C_I\equiv\; $\code{type(arg0)==int}  not.
A simple strategy for efficient dispatching is to sort versions according to a
compatible complete order (for instance using the binary representation when
elements are incomparable) and then using a linear search for the first
compatible context.

\bigskip

\noindent
The above definitions imply a notion of correctness for an
implementation.

\begin{theorem}
  Dispatching to version $\langle C,V\rangle \in F$ from a state $S$ and a
  current context $C'$ implies $C(S)$.
\end{theorem}

\noindent This follows immediately from the definition of the order relation. It
means that dispatch transfers control to a version of the function
compiled with assumptions that hold at entry.

\subsection{Applying Context Dispatch}

The above definitions may not necessarily lead to performance
improvements; indeed, an implementation may choose to systematically
dispatch to $\langle \top,V_u \rangle$.  This is a correct choice as the
version is larger than any current context but it also provides no
benefits.
The definition allows for imprecision from both sides. First, we can choose an
arbitrary loose current context. Second, given a current context we can choose
an arbitrarily bigger target version. Both of these imprecisions are by design,
as they are in fact implementation trade-offs.
An implementation could try to always compute a smallest current
context, as such a context captures the most information about the program
state at the call site. On the other hand, increased precision might be costly,
and thus an approximation may be warranted --- a less precise current context
is cheaper to compute and prevents over-specialization. Dispatching to a
larger than necessary target version again prevents compiling too many versions
and enables fast dispatching implementations.

An implementation may
also compute the current context by combining static and dynamic
information.
For that we might want to require contexts be closed under conjunction. The benefits are that a unique smallest current context exists and
the intersection of two current contexts is a more precise current context.
In other words two independent measurements of the current state can be
combined for a more precise context.
For instance, one may be able to determine statically that
$C\equiv\; $\code{type(arg0)==int} holds, perhaps
%
\begin{figure}\colorlet{circle edge}{black}
\centering
  \ifx\HCode\undefined
\tikzset{circ/.style={draw=circle edge}}
\begin{tikzpicture}
  \coordinate[] (C1) at (0,0);
  \coordinate[] (C2) at (0,1cm);

  \coordinate[label = below:{$\langle C_{I?}, V_{I?} \rangle$}] (C1L) at (0,-0.3cm);
  \coordinate[label = above:{$\langle C_{?I}, V_{?I} \rangle$}] (C2L) at(0,1.3cm);

  \draw[circ, even odd rule]
      (C1) circle (1.2cm)  node {}
      (C2) circle (1.2cm)  node {}
      (2.5cm,0.5cm) circle (1.1cm) node{{$\langle C_{SS}, V_{SS} \rangle$}}
      ;

  \coordinate[label = above:$S$] (S) at (0,0.5cm);
  \coordinate[label = above:{$\langle \top, V_u \rangle$}] () at (-2cm,1cm);
  \node at (S)[circle,fill,inner sep=1pt]{};
\end{tikzpicture}
  \else
    \includegraphics{figs/cd-contexts.png}
  \fi
\caption{Versions and current program state}\label{fig:context}
\end{figure}%
%
because that argument
appears as a constant, whereas $C'\equiv\; $\code{type(arg1)==string} must be
established by a run-time check. Given  $C\land C'$ exists, it
is a more precise current context. Thus the compiler can emit a check for
only $C'$, but then dispatch on $C\land C'$.

Similarly, dispatch can select any version that is larger than the current
context. Typically, one would prefer the smallest version larger than the
current context, as it is optimized with the most information.
But this choice can be ambiguous, as illustrated in \autoref{fig:context}.
There are four versions of the binary function $F$:
$V_u$ can always be invoked, $V_{SS}$ assumes two
strings as arguments, $V_{I?}$ assumes the first
argument is an integer, and $V_{?I}$ assumes the second is an integer.
Given $F$ is invoked with two integers, \ie in state $S$, the smallest current context
$C_{I?} \land C_{?I} = C_{II}$ is not available as a version to invoke.
The implementation can dispatch to either $C_{I?}$
or $C_{?I}$; however, neither is smaller than the other ($V_{u}$  is also
reachable, $V_{SS}$ is not).

Of course a JIT compiler can compile a fresh version
$\langle C_{II}, V_{II} \rangle$ to invoke. This is how we envision an implementation
to gradually populate the dispatch tables.
An implementation must decide when to add (or remove) versions. Each
dispatch where the current context is strictly smaller than the context of
the version dispatched to is potentially sub-optimal. The implementation can
rely on heuristics for when to compile a new version that more closely
matches the current context.
For instance for every dispatch we count the sub-optimal ones
and then lazily compile new versions for contexts that occur
frequently.

The efficiency of context dispatch depends only on the cost of computing
a current context and the order relation. This allows for arbitrary complex
properties in contexts, if these particular properties do not have to be checked at
run-time. For instance ``the first argument does not diverge'' is a possible context.
Given a call site
\c{f(while(a)\{\})}, we can establish this context using the
conjunction of ``if \c{a==FALSE} then \c{while(a)\{\}} does not diverge''
and ``\c{a==FALSE}.'' The former is static, and the latter is a simple dynamic check.

The compiler may replace context dispatch with a direct call to a version
under a static current context.  This has the benefit of
removing the dispatch overhead, or even allows inlining of context dispatched versions.  The drawback is that
the static context might be less precise than the dynamic one and it
forces the implementation to commit to a version early.
A better strategy is to speed up the dynamic dispatch using an inline cache
to avoid repeating the dispatch for every call. However, conversely to a
traditional inline cache, if the target version is sub-optimal, we should
re-check from time to time, if the dispatch table contains a better
version by now.

\bigskip

\noindent To make matters concrete, we detail two examples of ad-hoc contextual specialization
schemes and how they can be encoded in the \cd framework:

\paragraph{Customized Compilation}

This technique introduced in SELF
  specializes methods to concrete receiver types by duplicating them down
  the delegation tree. The technique can be understood as an instance of
  \cd. The contexts are type tests of the method receiver $C_A \equiv
  \mathsf{typeof}(\mathit{self}) == A$.  The order of contexts is defined as
  $C_A < C_B~\mathit{iff}~A <: B$.  It follows that if the receiver is of
  class $A$, and $A$ is a subtype of $B$, dispatch can invoke a version
  compiled for $B$.  In the Julia language, this strategy is extended to the
  types of all arguments.

\paragraph{Splitting}

It is possible to customize functions to certain contexts by splitting the
call-site. For instance an optimization in LLVM involves converting the call
\code{f(x)} with a potential null pointer argument to
\code{if (x != NULL) fNonnull(x) else f(x)}, to support an optimized version that can
assume the argument to not be \code{NULL}. In terms of \cd this could be
implemented using contexts to check for certain values, e.g., $C_{x,v} \equiv$ \code{x==v}
and $\widetilde{C}_{x,v} \equiv$ \code{x!=v}. This optimization would then be
available under the context $\widetilde{C}_{x,\mathsf{NULL}}$. Compared to
splitting, the advantage is that the target versions do not need to be decided
upon statically, in other words the splitting happens dynamically, only if
actually needed, and we could still decide to add other value based
specializations later.

\paragraph{Global Assumptions}
Contexts can capture predicates about the
  values of global variables, \eg $C \equiv \mathit{debug}==\mathsf{true}$
  or $C' \equiv \mathit{display}==\mathsf{headless}$. If we allow such
  contexts to be combined, we get a natural order from $C \land C' < C$,
  \ie a smaller context is one that tests more variables. The smallest
  current context is the conjunction of all current singleton contexts.
  An interesting application is shown by \citet{liu19}, where a dynamic analysis
  detects thread-local objects. The property is then used to dispatch to
  versions of their methods that elide locks.

\section{An Illustrated Example}

To illustrate the trade-offs when specializing functions consider
\autoref{lst:cd-example}, a map-reduce kernel written in R. The \code{reduce}
function takes a vector or list~\code{x} and iteratively applies \code{map}.
The \code{map} function has two optional arguments, \code{op} which defaults
to \code{"m"}, and \code{b}, which defaults to~\code{1} when \code{op} is \code{"m"}.
\code{map} is called twice
from \code{reduce}: the first call passes a single argument and the second
call passes two arguments. The type of the result depends on the type of
\code{x} and the argument~\code{y}.  As a driver, we invoke
\code{reduce} ten times with a vector of a million integers, once with a
list of tuples, and again ten times with an integer vector.  This example
exposes the impact of polymorphism on the performance.

\begin{lstlisting}[style=R, caption={An example program}, label={lst:cd-example}]
map <- function(a,
                b = if(op=="m") 1,
                op= "m") {
       if (op=="m")  a * b
  else if (op=="a")  a + b
  else error("unsupported")
}
reduce <- function(x, y=3, res=0) {
  for (i in x)
    res <- res + map(i) + map(i, y)
  res
}
for (i in 1:10)
  system.time(reduce(1L:1000000L))
reduce(list(c(1L,1L), c(2L,2L)))
for (i in 1:10)
  system.time(reduce(1L:1000000L))
\end{lstlisting}

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{experiments/motivation/litmus.pdf}
\caption{Optimization strategies for \autoref{lst:cd-example}}\label{fig:opt}
\end{figure}

\autoref{fig:opt}
illustrates the execution time of each of the twenty measurements in seconds (smaller is better).
The red line describes the results with inlining and speculation enabled.
In this case, \code{map} is inlined twice. The point marked with
\textcolor{Pallete1}{(1)} shows optimal performance after the compiler has
finished generating code.  However, the call to \c{reduce} with a list of
tuples leads to deoptimization and recompilation
\textcolor{Pallete1}{(2)}. Performance stabilizes again \textcolor{Pallete1}{(3)},
but it does not return to the optimal, as the code remains compiled with
support for both integers and tuples.
%
The green line shows the results with inlining of the \c{map} function
manually disabled.  After performance stabilizes \textcolor{Pallete2}{(4)},
the performance gain is small.  This can be attributed to the high cost of
function calls in R.  Again, we observe deoptimization \textcolor{Pallete2}{(5)}
and re-optimization \textcolor{Pallete2}{(6)}. The curve mirrors inlining,
but with smaller speedups.
%
Finally, the blue line exposes the results when we enable \cd (without
inlining).  The first iteration \textcolor{Pallete3}{(7)} is fast because
\c{reduce} can benefit from the compiled version of \code{map} earlier,
thanks to \cd of calls.  Performance improves further when
the \c{reduce} function is optimized \textcolor{Pallete3}{(8)}. We see a
compilation event at \textcolor{Pallete3}{(9)}. Finally, we return to the
previous level of performance \textcolor{Pallete3}{(10)}, in contrast to the
two previous experiments, where the deoptimization impacted peak
performance. The reason is that the version of \c{map} used to process
integer arguments is not polluted by information about the tuples, since
they are handled by a different version.

\graphicspath{ {./deoptless/} }
\section{Deoptless: Context Dispatched OSR}\label{sec:deoptless}
\input{deoptless/deoptless-theory}

\section{Discussion}

%% Just-in-time compilers optimize programs by specializing the code they
%% generate to past program behavior. The combination of inlining and
%% speculation is widely used in dynamic languages, as inlining enlarges the
%% scope of a compilation unit and thus provides information about the context
%% in which a function is called, and speculation enables inlining and allows
%% to avoid uncommon branches.
%% 
In conclusion, \cd allows the
compiler to manage multiple specialized versions of a function, and to
select the most appropriate version dynamically based on information
available at the call site. The difference with inlining and speculation is
that \cd allows a different version of a function to be chosen at each and
every invocation of that function with modest dispatching overhead. Whereas
inlining requires the compiler to commit to one particular version, and
speculation requires the compiler to deoptimize the function each time a
different version is needed. We envision \cd to allow just-in-time compilers to
rely more on inter-procedural optimizations for performance in the future.

Like inlining, \cd allows to specialize a function to its calling
context. Unlike inlining, the specialized function can be shared across
multiple call sites.  While speculation needs deoptimization to undo wrong
assumptions, \cd does not. \CD applies at call boundaries, while speculation
can happen anywhere in a function.  Finally, let us repeat that these
mechanisms are not mutually exclusive: the implementation of \R supports all
of them and we look forward to studying potential synergies in future work.

The key design choice for an implementation of this approach is to pick
contexts that have an efficiently computable partial ordering. We envision
compilers for different languages defining their own specific contexts.  The
choice of context is also driven by the cost of deriving them from current
program state, and the feasibility of approximation strategies.

The \cd framework was initially designed
to select versions at the function
level. But,
as we have shown with deoptless, it is also possible to apply it for dispatching
code fragments at a finer granularity.

%% Our implementation of \cd in \R was evaluated on a benchmark suite composed
%% of a number small algorithmic problems and a few real-world programs.
%% Relative to the GNU R reference implementation, \R with \cd achieves an
%% average speedup of $\resultoverallGnurMedRounded\times$, with the worst
%% being a $\resultoverallGnurMinRounded\times$ slowdown and the best being a
%% $\resultoverallGnurMaxRounded\times$ speedup.  Evaluating the contribution
%% of \cd, we observed that it improves performance in \resultHRejectOne\xspace
%% out of \resultBenchmarksInTotalNonMu programs in our benchmark suite, and in
%% \resultHRejectOneMu\xspace out of \resultBenchmarksInTotalMu
%% micro-benchmarks.


