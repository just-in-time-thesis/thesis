metavar id            ::= {{ lex alphanum0 }} {{ tex \mathit{id} }}
metavar num           ::= {{ lex numeral   }} {{ tex \mathit{n} }}

indexvar i ::=
indexvar j ::=
indexvar k ::=
indexvar n ::=
indexvar m ::=
indexvar p ::=
indexvar q ::=

grammar

identifier {{ tex \mathit{id} }} :: 'PirLabel' ::=
 | id  :: :: Id  {{ tex \textsf{[[id]]} }}
Rvar {{ tex \mathit{x} }} :: 'RVariable' ::=
 | id  :: :: Id  {{ tex \textsf{[[id]]} }}

type {{ tex \mathit{T} }} :: 'Type' ::=
 | baseType :: :: T1
 | baseType1 / baseType2 :: :: T3              {{ com union }} {{ tex [[baseType1]]|[[baseType2]] }}
 | type typeModifier1 ... typeModifiern :: :: T2  {{ com with flags }}

baseType {{ tex \mathit{t} }} :: 'BaseType' ::= {{ com Base Types }}
 | any :: :: A  {{ tex \textsf{any} }}
 | int :: :: I   {{ tex \textsf{int} }}
 | real :: :: R  {{ tex \textsf{real} }}
 | lgl :: :: L   {{ tex \textsf{lgl} }}
 | cls :: :: C   {{ tex \textsf{cls} }}
 | envir :: :: E {{ tex \textsf{env} }}
 | stub :: :: ES {{ tex \textsf{stub} }} {{ com Environment stub }}
 | cp :: :: cp  {{ com Checkpoint }} {{ tex \textsf{cp} }}
 | fs :: :: fs  {{ com Framestate }} {{ tex \textsf{fs} }}
 | bool :: :: BL   {{ tex \textsf{bool} }} {{ com native bool }}
 | v :: :: V {{ tex {} }}

typeModifier {{ tex \mathit{m} }} :: 'TypeModifier' ::= {{ com Type Modifiers }}
 | S :: :: S {{ com scalar }} {{ tex {\kern -1pt \small\$} }}
 | ~ :: :: L {{ com eager }} {{ tex {\kern -1pt \small\sim} }}
 | + :: :: A {{ com might have attributes }} {{ tex {\kern -1pt \small+} }}


instr :: 'Instruction' ::= {{ com  }}
 | Binop arg1 , arg2 , env         :: :: Binop {{ tex [[Binop]]~    ([[arg1]],~[[arg2]])~[[env]]  }}
                                                     {{ com binary op. }}
 | Goto BBId                       :: :: Goto  {{ tex \pirI{Jump}~  [[BBId]] }}
                                                     {{ com jump }}
 | Branch arg , BBId1 , BBId2      :: :: Branch {{ tex \pirI{Branch}~  (\,[[arg]],~[[BBId1]],~[[BBId2]]) }}
                                                     {{ com branch }}
 | Call arg0 ( args ) env          :: :: Call  {{ tex \pirI{Call}~  [[arg0]]~(\,[[args]])~[[env]]     }}
                                                     {{ com apply closure }}
 | Checkpoint BBId                 :: :: Cp     {{ tex \pirI{Checkpoint}~[[BBId]] }}
                                                     {{ com deoptimization safepoint }}
 | Assume args , arg0              :: :: Assumption {{ tex \pirI{Assume}~(\,[[args]]) ~ [[arg0]] }}
                                                     {{ com assumption, $[[arg0]] : \textsf{cp}$ }}
 | IsType type arg                :: :: Typecheck {{ tex \pirI{Is}\langle[[type]]\rangle(\,[[arg]]) }}
                                                     {{ com typecheck }}
 | Framestate identifier , args , env , arg0  :: :: Framestate {{ tex
 \pirI{Framestate}~(\,[[identifier]], \langle [[args]] \rangle, \textsf{env}=[[env]], \textsf{next}=[[arg0]]) }}
 {{ com framestate, $[[arg0]] : \textsf{fs}$ }}
 | Deopt arg   :: :: Deopt {{ tex \pirI{Deopt}~(\,[[arg]]) }}
                                                     {{ com deoptimization, $[[arg]] : \textsf{fs}$ }}
 | Force arg , env                :: :: Force {{ tex \pirI{Force}~        (\,[[arg]])~[[env]]       }}
                                                     {{ com force promise }}
 | LdArg num                       :: :: LdArg {{ tex \pirI{LdArg}~        (\,[[num]])               }}
                                                     {{ com load argument }}
 | LdConst lit                     :: :: LdConst {{ tex \pirI{LdConst}~[[lit]]               }}
                                                     {{ com load constant }}
 | LdFun Rvar , env               :: :: LdFun {{ tex \pirI{LdFun}~        (\,[[Rvar]],~[[env]])      }}
                                                     {{ com load function }}
 | LdVar Rvar , env               :: :: LdVar {{ tex \pirI{LdVar}~        (\,[[Rvar]],~[[env]])      }}
                                                     {{ com load variable }}
 | MkArg ( identifier , env )     :: :: MkArg1 {{ tex \pirI{MkArg}~    (\,[[identifier]],~[[env]])   }}
 | MkArg ( identifier , env , lit )     :: :: MkArg {{ tex \pirI{MkArg}~(\,[[identifier]],~[[env]],~[[lit]])   }}
                                                     {{ com create promise }}
 | MkEnv ( bindings , scoping )   :: :: MkEnv {{ tex \pirI{MkEnv}~  (\,[[bindings]]~[[scoping]])    }} 
                                                    {{ com create environment, $\mathit{env} : \textsf{envir}$ }}
 | MkClosure ( identifier , env ) :: :: MkClosure {{ tex \pirI{MkClosure}~(\,[[identifier]],~[[env]])   }}
                                                     {{ com create closure }}
 | Phi ( phiInputs )               :: :: Phi   {{ tex \pirI{Phi}~  ([[phiInputs]])                  }}
                                                     {{ com $\phi$ function }}
 | Return arg                      :: :: Return {{ tex \pirI{Return}~       (\,[[arg]])               }}
                                                     {{ com return }}
 | StVar Rvar , arg , env         :: :: StVar {{ tex \pirI{StVar}~     (\,[[Rvar]],~[[arg]],~[[env]])}}
                                                     {{ com store variable }}

st :: 'Statement' ::=                    {{ com Statements }}
 | type ans = instr      :: :: NonVoidInstr  {{ tex [[type]] ~ [[ans]] = [[instr]] }}
                                        {{ com non-void instruction }}
 | instr            :: :: VoidInstr     {{ tex           [[instr]] }}
                                        {{ com void instruction }}

args {{ tex \mathit{a}^{*} }} :: 'argueList' ::=
 | arg1 , .. , argn :: :: Nargs

% instruction parts
bindings {{ tex {(\mathit{x}=\mathit{a})}^{*} }} :: 'Bindings' ::=
 | binding1 , .. , bindingn :: :: BindingList
binding :: 'EnvironmentDefinition' ::=
 | Rvar = arg :: :: NamearguePair
scoping {{ tex : \mathit{env} }} :: 'OuterScope' ::=
 | parent = env :: :: E  {{ tex : [[env]] }}

Binop {{ tex \textit{Binop} }} :: 'Binop' ::=
 | Add :: :: Add {{ tex \pirI{Add} }}
 | Eq  :: :: Eq {{ tex \pirI{Eq} }}
 | someMore :: :: More {{ tex ... }}

BBId {{ tex \mathit{L} }} :: 'BBId' ::= {{ com Basic Block Label }}
 | BB ( num ) :: :: BBID  {{ tex \textsf{BB}_{ [[num]] } }}

phiInputs {{ tex {(\mathit{L} : \mathit{v})}^{*} }} :: 'PhiInputs' ::=
 | phiInput1 , .. , phiInputn :: :: List
phiInput :: 'PhiInput' ::=
 | BBId arg :: :: Inp  {{ tex [[BBId]] : [[arg]] }}

% Instruction arguments
arg {{ tex \mathit{a} }} :: 'InstrArg' ::=
 | ans           :: :: Identifier
 | lit           :: :: Literal

% Instruction arguments
argOrEnv {{ tex \mathit{a} }} :: 'InstrArgOrEnv' ::= {{ com Arguments }}
 | ans           :: :: Identifier     {{ com register }}
 | lit           :: :: Literal        {{ com literal }}


% identifiers

ans {{ tex \textsf{ \%}n }} :: 'PirIdentifier' ::=
 | varName    :: :: ConcretePirVarname
 | envVarName :: :: ConcreteEnvVar

varName {{ tex \mathtt{\%}\mathit{n} }} :: 'VarName' ::=
 | @ num  :: :: ConcretePirVarname    {{ tex \textsf{\%\kern 1pt [[num]] \kern -1pt} }}
 | @i     :: :: PirVarnamei   {{ tex \textsf{\%}\mathit{i} }}
 | @j     :: :: PirVarnamej   {{ tex \textsf{\%}\mathit{j} }}
 | @k     :: :: PirVarnamek   {{ tex \textsf{\%}\mathit{k} }}
 | @n     :: :: PirVarnamen   {{ tex \textsf{\%}\mathit{n} }}
 | @m     :: :: PirVarnamem   {{ tex \textsf{\%}\mathit{m} }}
 | @p     :: :: PirVarnamep   {{ tex \textsf{\%}\mathit{p} }}
 | @q     :: :: PirVarnameq   {{ tex \textsf{\%}\mathit{q} }}
 | @1ton  :: :: PirVarnameseq {{ tex \textsf{\%}\mathit{1}, \dots, \mathtt{\%}\mathit{n} }}
envVarName {{ tex \mathtt{\%}\mathit{n} }} :: 'EnvVarName' ::=
 | @@ num  :: :: ConcreteEnv         {{ tex \textsf{\%\kern 1pt [[num]] \kern -1pt} }}
 | @@i     :: :: ConcreteEnvi        {{ tex \textsf{\%}\mathit{i} }}
 | @@j     :: :: ConcreteEnvj        {{ tex \textsf{\%}\mathit{j} }}
 | @@k     :: :: ConcreteEnvk        {{ tex \textsf{\%}\mathit{k} }}
 | @@n     :: :: ConcreteEnvn        {{ tex \textsf{\%}\mathit{n} }}
 | @@m     :: :: ConcreteEnvm        {{ tex \textsf{\%}\mathit{m} }}
 | @@p     :: :: ConcreteEnvp        {{ tex \textsf{\%}\mathit{p} }}
 | @@q     :: :: ConcreteEnvq        {{ tex \textsf{\%}\mathit{q} }}

%% We use this internally to avoid typos (we only allow envVarNames)
env {{ tex \mathit{env} }}:: 'Env' ::=   {{ com NOT THE ACTUAL ENV DEFINITON }}
 | envVarName :: :: ConcreteEnvVar
 | Global     :: :: GlobalEnv
 | Open       :: :: NotClosed
 | Elide      :: :: Elide       {{ tex }}

lit :: 'Lit' ::=  {{ com Literals }}
 | Global     :: :: GlobalEnv             {{ com global env.}}
 | missing    :: :: Undef                 {{ com missing argument }} {{ tex \textsf{missing} }}
 | nil        :: :: Nil                   {{ com nil }} {{ tex \textsf{nil} }}
 | true       :: :: True  {{ tex \textsf{true} }}  {{ com true }}
 | [ numsf1 , ... , numsfm ] :: :: NumericVector {{ com vector }}
 | someMore   :: :: More

numsf {{ tex \mathit{n} }} :: 'numinsf' ::=
 | num :: :: Nsf {{ tex \textsf{[[num]]} }}

someMore :: 'MoreLits' ::= {{ tex ... }}
 | num :: :: N {{ tex \textsf{[[num]]} }}

% helpers for textsetting

P :: 'Promise' ::=      {{ com Promise }}
 | identifier : Bs :: :: C

F :: 'Function'   ::=
 | identifier : V1 , .. , Vn :: :: List

C :: 'Context' ::=   {{ com Context }}
 | todo  :: :: T

V :: 'Version' ::=         {{ com Function }}
 | C : Bs  Ps :: :: main    {{ com context body and promises }}

Ps {{ tex \mathit{P^*} }} :: 'Promises' ::=
 | P1 .. Pn :: :: Ps

Bdef {{ tex \mathit{B} }} :: 'BasicBlock' ::=   {{ com Basic Block }}
 | BBId = BBBody :: :: BB  {{ tex [[BBId]]: [[BBBody]] }}


B :: 'BasicBlockarray' ::=
 | BBId = BBBody :: :: BB  {{ tex & [[BBId]]: & & & \\
                                  [[BBBody]] }}

BBBody {{ tex \mathit{ {st}^{*} } }} :: 'BasicBlockBody' ::=
 | stP1 .. stPn :: :: Instructions

stP :: 'PrintedStatement' ::=
 | type ans = instr   :: :: NonVoidInstr         {{ tex & [[type]] & [[ans]] & = & [[instr]] \\ }}
 | type ans = skip    :: :: NonVoidInstrSkipped  {{ tex & [[type]] & [[ans]] & = & ... \\ }}
 | instr              :: :: VoidInstr            {{ tex &          &         &   & [[instr]] \\ }}
 | skip               :: :: VoidInstrSkipped     {{ tex &          &         &   & ... \\ }}
 | # comment          :: :: Comment              {{ tex & \multicolumn{4}{l}{\ \ \textsf{\/\/ [[comment]]} } \\ }}

printableProgram :: 'PrintableProgram' ::=
 | prg ( printableWithLabel ) :: :: T2
       {{ tex {\small \ttfamily
                  \[
                      \begin{array}{rrlll}
                          \multicolumn{5}{l}{
                             [[printableWithLabel]]
                          }
                      \end{array}
                  \]
              } }}
 | prg ( printable ) :: :: T1
       {{ tex {\small \ttfamily
                  \[
                      \begin{array}{@{}rrlll}
                         [[printable]]
                      \end{array}
                  \]
              } }}

printableWithLabel :: 'PrintableWithLabel' ::=
 | namedPrintableFunction1 .. namedPrintableFunctionn :: :: List

Bs {{ tex \mathit{B^*} }} :: 'Bs' ::=
 | B1 .. Bn :: :: BBn

printable :: 'Printable' ::=
 | BBBody :: :: BB1
 | Bs :: :: Bn

namedPrintableFunction :: 'NamedPrintableFunction' ::=
 | none : BBBody          :: :: RenderNoname {{ tex } \\ [[BBBody]] \multicolumn{5}{l}{ }}
 | identifier : BBBody    :: :: Render1 {{ tex \textsf{[[identifier]]} } \\ [[BBBody]] \multicolumn{5}{l}{ }}
 | identifier : Bs        :: :: Rendern  {{ tex \textsf{[[identifier]]} } \\ [[Bs]]\multicolumn{5}{l}{ }}

comment :: 'Comment' ::=
 | identifier commentPart1 .. commentPartn :: :: Strings
commentPart :: 'CommentPart' ::=
 | identifier  :: :: Render {{ tex \kern-0.68em [[identifier]] }}

terminals :: 'Terminals' ::=
 | Open   :: :: Q {{ tex \textsf{O} }}
 | Global :: :: G {{ tex \textsf{G} }}
