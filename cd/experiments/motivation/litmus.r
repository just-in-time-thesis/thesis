map <- function(a, b = if (op=="mul") 1 else 0, op="mul") {
  if (op == "add")
    a + b
  else if (op == "mul")
    a * b
  else
    error("unsupported operation")
}

reduce <- function(x) {
  s <- 0
  for (i in x)
    s <- s + map(i) + map(i, 3)
  s
}

runTest <- function(name) {
  runs <- 10
  for (i in 1:runs) {
    time = system.time(reduce(1L:1000000L))
    cat(paste0(name,",",i,",scalar,",time[1],"\n"))
  }
  reduce(list(c(1L,1L), c(2L,2L)))
  for (i in 1:runs) {
    time = system.time(reduce(1L:1000000L))
    cat(paste0(name,",",i+(1*runs),",scalar,",time[1],"\n"))
  }
}

resetTest <- function() {
  body(reduce) <<- body(reduce)
  invisible(rir.compile(reduce))
  rir.markFunction(reduce, DisableInline=T, DisableAllSpecialization=T)

  body(runTest) <<- body(runTest)
  invisible(rir.compile(runTest))
  rir.markFunction(runTest, DisableInline=T, DisableAllSpecialization=T)

  body(map) <<- body(map)
  invisible(rir.compile(map))
}

cat("experiment,run,type,s\n")

resetTest()
rir.markFunction(map, ForceInline=T, DisableAllSpecialization=T)
runTest("inlined")

resetTest()
rir.markFunction(map, DisableInline=T, ForceInline=F, DisableAllSpecialization=T)
runTest("optimized");

resetTest()
rir.markFunction(map, DisableInline=T, DisableAllSpecialization=F)
rir.markFunction(reduce, DisableAllSpecialization=F)
runTest("specialized");
