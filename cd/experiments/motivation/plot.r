require(ggplot2)
require(ggrepel)
d = read.csv("results.csv")

d$anotation = ""

opt = 4
runs = 10

d[d$experiment == "inlined" & d$run == opt,]$anotation = "1"
d[d$experiment == "inlined" & d$run == runs+1,]$anotation = "2"
d[d$experiment == "inlined" & d$run == runs+opt-1,]$anotation = "3"
d[d$experiment == "optimized" & d$run == opt,]$anotation = "4"
d[d$experiment == "optimized" & d$run == runs+1,]$anotation = "5"
d[d$experiment == "optimized" & d$run == runs+opt-1,]$anotation = "6"
d[d$experiment == "specialized" & d$run == 1,]$anotation = "7"
d[d$experiment == "specialized" & d$run == opt,]$anotation = "8"
d[d$experiment == "specialized" & d$run == runs+1,]$anotation = "9"
d[d$experiment == "specialized" & d$run == runs+opt-1,]$anotation = "10"

d$iteration = d$run
d$seconds = d$s
cairo_pdf("litmus.pdf")
print(ggplot(d, aes(iteration,seconds))) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~experiment, ncol=1, strip.position="left") +
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
  geom_text_repel(aes(label = anotation,
                       color = experiment
                       ),
                       size = 6,
                       seed = 1,
                       nudge_y=0.3,
                       nudge_x=1.4,
                       show.legend=F) +
 # geom_vline(xintercept=runs+0.5, linetype="dashed", color="gray", size=0.1) +
  geom_line(aes(color=experiment), size=1.5)

dev.off()
