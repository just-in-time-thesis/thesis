\def\coqin#1{\lstinline[language=Coq]{#1}}
\def\specin#1{\lstinline[language=specir,basicstyle=\sffamily]{#1}}

\newcommand{\JITname}{CoreJIT}
\newcommand{\JIT}{\emph{\JITname}\xspace}
\newcommand{\JITplain}{{\JITname}\xspace}
\newcommand{\IRname}{CoreIR}
\newcommand{\IR}{\emph{\IRname}\xspace}
\newcommand{\IRplain}{\IRname\xspace}
\newcommand{\FSname}{Anchor}
\newcommand{\FS}{\FSname\xspace}
\newcommand{\FSsmall}{\FSname\xspace}

\newcommand{\bhvm}{\mathcal{B}}
\newcommand{\bhv}{$\bhvm$}
\newcommand{\bhvmj}{\bhvm_{JIT}}
\newcommand{\bhvj}{$\bhvmj$}

\def\reg{\ensuremath{r}}
\def\val{\ensuremath{v}\xspace}
\newcommand{\addr}{\ensuremath{a}\xspace}
\newcommand{\funid}{\ensuremath{f}}

\renewcommand\V{\ensuremath{\mathit{V}}}
\providecommand\M{}
\renewcommand\M{\ensuremath{\mathit{M}}}

\newcommand*\anchorsymb{\vcenter{\hbox{\includegraphics[height=0.3cm]{img/anchor_symb}}}}
\newcommand{\Anchor}{\ensuremath{\pirI{Anchor}}\xspace}

\newcommand{\jstep}{\coqin{jit_step}\xspace}
\newcommand{\jstate}{\coqin{jit_state}\xspace}

\lstdefinestyle{generic}{ %
  xleftmargin=2\parindent,
  captionpos=b,
}
\lstdefinestyle{lua}{ %
  style=generic,
  language={[5.0]Lua},
}
\lstdefinestyle{luain}{ %
  style=lua,
  breaklines=false
}
\def\lua#1{\lstinline[style=luain]{#1}}

Eventually the building blocks presented in this thesis could be the
foundation for writing an end-to-end verified JIT compiler. First steps
were already made by \citet{popl21}.
The main differences with the formalization presented so far are that \JIT also
models the creation of optimized code at run-time, that the formalization is
mechanized, and that the \emph{deoptimization invariant} was made more precise, by
splitting \Assume into two instructions \Anchor and \Assume.
This section summarizes our findings from
that paper, with the focus on the comparison to the \sourir
model.

\IR is inspired by CompCert's RTL~\cite{Leroy-backend}. As a simplification over
sourir, \IR only features two versions per function, an optimized and a baseline
one.
Two \IR instructions are related to speculation, \Anchor and \Assume.
Together they look and behave very similar to \assume from \sourir. The split was
made to better describe the dynamics of the optimization process.
The \Anchor instruction represents a potential deoptimization point, \ie a
location in an optimized function where the correspondence with its baseline
version is known to the compiler and thus deoptimization can occur. For
instance, in \specin{Anchor F.l [r = r+1]} the target \specin{F.l} specifies the
function (\specin F) and label (\specin l) to jump to, the mapping \specin{[r = r+1]} is
the \textit{varmap}.

Anchors are inserted first in the optimization pipeline, before any changes
are made to the program.  Choosing where to insert them is important as they
determine where speculation \emph{can} happen.  Speculation itself is
performed by inserting \Assume instructions. An assume based on the previous
anchor is for instance \specin{Assume x=1 F.l [r = r'+1]}, which expresses the
expectation that register \specin{x} has value \specin{1}. This instruction
behaves like the \assume instruction in \sourir.
Unlike anchors,
assumes can be inserted at any time during compilation.

The role of anchors is subtle. As already shown it is possible to move an assume
instructions to support \emph{deoptimization at any location}. To make this more
practical, the \Anchor stays at its original location, without tying it to
additional data-dependencies by guard expressions.
To add an \Assume, the
compiler finds the dominating \Anchor and copies its deoptimization
metadata. If there is no anchor, then the assumption cannot be made.

For the proofs, anchors have yet another role. They justify the insertion
of \Assume instructions.  For this, the \Anchor instructions have a
non-deterministic semantics, an anchor can randomly choose to deoptimize.
Crucially, deoptimization is always semantically
correct, nothing is lost by returning to the baseline code eagerly other
than performance. An inserted \Assume is thus correct if it follows
an \Anchor and the observable behavior of the program is unchanged
regardless which instruction deoptimizes.
%
\begin{figure}
\small\centering
\ifx\HCode\undefined
\begin{tikzpicture}[%
        every node/.style={rectangle, rounded corners, minimum size=4pt,minimum
        height=4pt, inner sep=5pt, text width=3.3cm, align=center},
        shorten >=2pt,
        node distance=0.2cm, >=latex
      ]
      \node [] (ver1) [text width=2cm] {\specin{before}};
      \node [] (ins1) [draw,below=of ver1] {\specin{Anchor tgt vm sl}};
      \node [] (ins2) [draw,below=of ins1] {\specin{Branch e ltrue lfalse}};
      \node [] (ins3) [draw,below=of ins2] {\specin{ltrue: instr}};
      \node [] (ver2) [text width=2cm, right=of ver1, xshift=2.5cm] {\specin{after}};
      \node [] (assume) [draw,below=of ver2] {\specin{Anchor tgt vm sl}};
      \node [] (ins1a) [draw,below=of assume] {\specin{Branch e l lfalse}};
      \node [] (ins2a) [draw,below=of ins1a] {\specin{l: Assume e tgt vm sl}};
      \node [] (ins3a) [draw,below=of ins2a] {\specin{ltrue: instr}};
      \path [draw] (ins1.east) edge[-, above]  node {} (assume.west)
      (ins1.east) edge[-, dashed, above]  node {} (ins1a.west)
      (ins1.east) edge[-, dashed, below]  node {} (ins2a.west)
      (ins3.east) edge[-, above]  node {} (ins3a.west)
      (ins2.east) edge[-, above]  node {} (ins1a.west)
      (ins2.east) edge[-, above]  node {} (ins2a.west)
      (ins1.south) edge[->, below]  node {} (ins2.north)
      (ins2.south) edge[->, below]  node {} (ins3.north)
      (ins1a.south) edge[->, below]  node {} (ins2a.north)
      (assume.south) edge[->, below]  node {} (ins1a.north)
      (ins2a.south) edge[->, below]  node {} (ins3a.north);
    \end{tikzpicture}
  \else
    \includegraphics{tikz/sim.png}
  \fi
    \caption{The $\approx$ relation for delayed \Assume insertion}
    \label{fig:delay_proof}
\end{figure}%
%
Let's consider the example in \autoref{fig:delay_proof}.
On the left is the version before inserting an assume, on the right after. In
this case there is an \Anchor followed by a conditional \pirI{Branch} and we insert
the \Assume instruction only in one of the branches. As mentioned, \Anchor has a
non-deterministic semantic, it can either continue or deoptimize. Therefore,
the respective states must be matched for all
its successor instructions, \ie for both the case where it
falls through (solid lines), as well as when it deoptimizes (via dashed lines). To that end the states between
\Anchor and \Assume are matched both to a fall-through \Anchor and a
deoptimizing \Anchor trace.
In other words the assumption insertion pass codifies the \emph{assumption
transparency} invariant.
The benefit of having anchors is
that the assumes they dominate can be placed further down the instruction
stream.  The compiler must make sure that the intervening instructions do
not affect deoptimization.  This separation is important in practice as it
allows a single \Anchor to justify speculation at a range of different
program points. All \Anchor instructions are removed in the last step of the
optimization pipeline.
Initially the varmap of an \Assume instruction
will be identical to its dominating \Anchor, but, as the following example shows,
this can change through subsequent program transformations.

\paragraph{Illustrative Example}

Assume that, for the program in \autoref{lst:ex1:src}, a profiler detected that at
%
\begin{figure}
\begin{lstlisting}[language=specir,caption={Baseline},label={lst:ex1:src}]
Function F(x,y,z):
Version Base:
     d <- 1
 l1: a <- x*y
     Branch (z == 7) l2 l3
 l2: b <- x*x
     c <- z*y
     Return b+c+d
 l3: Return a
\end{lstlisting}
\end{figure}%
%
label \specin{l2} of function \specin{F} registers \specin z and \specin x always have values 7
and 75. Function \specin{F} can thus be specialized. \autoref{lst:ex1:optim} adds
an \specin{Opt} version to \specin{F} where an anchor has been added at \specin{l4}.  
\begin{figure}
\begin{lstlisting}[language=specir,caption={Optimized},label={lst:ex1:optim}]
Function F(x,y,z):
Version Base:
     ...

Version Opt:
 l4: Anchor F.l1 [x,y,z,d=1]
     Assume z=7 [x,y,z,d=1]
     c <- 7*y
     Assume x=75 [x,y,z=7,d=1]
     Return 5626+c
\end{lstlisting}
\end{figure}%
%
In
order to deoptimize to the baseline, the anchor must capture all of the
arguments of the function (\specin x, \specin y, \specin z) as well as the local register
\specin d. The compiler is able to constant propagate \specin d, so the anchor remembers
its value.  The speculation is done by \eg \specin{Assume z=7 ...}
which specifies what is expected from the state of the
program and the dominating anchor. The optimized version has eliminated dead
code and propagated constants.  If the speculation holds, then this version
is equivalent to \specin{Base}. Despite the overhead of checking validity of the
speculation, the new version should be faster: the irrelevant computation
of \specin{a} has been removed and \specin{x*x} is speculatively constant folded.  If
the speculation fails, then the execution should return to 
\specin{Base}, at label \specin{l1} where the closest \Anchor is
available, and reconstruct the original environment. This involves for
instance materializing the constant folded variable \specin{d}.  As we see
here, \Assume does not have to be placed right after an \Anchor instruction.
This will cause deoptimization to appear to jump back in time and some
instructions will be executed twice. It is up to the compiler to ensure
these re-executed instructions are idempotent.
As can be seen in the example, different \Assume
instructions originating from the same \Anchor, can end up with different
varmaps.
