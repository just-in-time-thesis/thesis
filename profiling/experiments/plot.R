url <- "https://rir-benchmarks.prl.fit.cvut.cz/job/"

WARM_RUNS = 10
COLD_RUNS = 5

fetch <- function(job) {
  dir.create("data", showWarnings=F)
  setwd("data")
  file <- paste0(job, ".csv")
  if (!file.exists(file))
    download.file(paste0(url, file), file)
  res <- read.csv(file, header=TRUE, strip.white=TRUE)
  res$row <- 1:nrow(res)
  setwd("..")
  res
}

printResult <- function(name, result, digits=3, space=F) {
   name = gsub("μ", "mu", name)
   r = format(result, digits=digits)
   cat(paste0("\\newcommand{\\result",name,"}{",r))
   if (space)
     cat("\\xspace")
   cat("}\n")
}

remove_warmup <- function(data) {
  data[data$warm == 1,]
}

fetch_jobs <- function(jobs) {
  data <- NULL
  for (i in seq_along(jobs)) {
    job <- jobs[i]
    d <- fetch(job)
    d$experiment = names(job)
    if (is.null(data))
      data <- d
    else
      data <- merge(data, d, all=TRUE)
  }
  data
}

normalize <- function(data, removeNormalized, baseline) {
  data$speedup = 1
  for (b in unique(data$benchmark)) {
    if (missing(baseline))
      e1 = unique(data$experiment)[1]
    else
      e1 = baseline
    m = median(data[data$benchmark == b & data$experiment == e1 & data$warm == 1, ]$ms)
    data[data$benchmark == b, ]$speedup <-
      m/data[data$benchmark == b, ]$ms
    if (removeNormalized && nrow(data[data$benchmark == b & data$experiment == e1, ]) > 0)
      data = data[-which(data$benchmark == b & data$experiment == e1), ]
  }
  data
}

require(ggplot2)
require(ggformula)
require(psych)

position_var_nudge <- function(l_off=0, r_off=0) {
  ggproto(NULL, PositionVarNudge, l_off=l_off, r_off=r_off)
}

PositionVarNudge <- ggproto("PositionVarNudge", Position,
  l_off = 0,
  r_off = 0,

  setup_params = function(self, data) {
    list(l_off = self$l_off, r_off = self$r_off)
  },

  compute_layer = function(self, data, params, layout) {
    nudge = data
    nudge$ord = nrow(nudge):1
    nudge = nudge[order(nudge$row),]
    nudge$nudge = 0

    for (p in unique(data$PANEL)) {
      for (e in unique(data$x)) {
        n = nrow(nudge[nudge$x == e & nudge$PANEL == p,])
        if (n == 0) next()
        n_tot = n + params$l_off + params$r_off
        steps <- (0.9:(n_tot))/n_tot
        width <- 0.6
        steps <- steps * width - (width/2)
        nudge[nudge$x == e & nudge$PANEL == p,]$nudge <- steps[(params$r_off+1):(n_tot-params$l_off)]
      }
    }
    nudge = nudge[order(nudge$ord),]
    nudge = nudge$nudge
    f <- function(x) {
      x - nudge
    }
    transform_position(data, f, NULL)
  }
)

plot_bm <- function(title, d, warmup, trend=FALSE, log=TRUE, relative=FALSE, ncol=6, hide_legend=F, baseline=relative, scales="free") {
  multipleBm <- length(unique(d$benchmark)) > 1
  sz <- if (multipleBm) 0.8 else 1.5

  nwarm = WARM_RUNS
  ncold = COLD_RUNS
  cold_position <- position_var_nudge(r_off = nwarm)
  warm_position <- if (warmup)
                     position_var_nudge(l_off = ncold)
                   else
                     position_var_nudge()

  experiments = length(unique(d$experiment))


  d$experiment = factor(d$experiment, levels=c("100k/50","500k/20","1M/20"))

  print(ggplot(d, if (relative) aes(experiment, speedup) else aes(experiment, ms)) +
    (if (multipleBm) facet_wrap(~benchmark, ncol=ncol, strip.position="left", scales=scales)) +
    # (if (!multipleBm) theme_minimal()) +

  #  (if (experiments == 2)
  #    scale_color_manual(values = scales::viridis_pal()(10)[c(1,9)])
  #  else
  #    scale_colour_viridis_d()) +

    theme_bw() +
    scale_colour_brewer(palette = "Dark2")+
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.title.y=element_blank(),
          axis.ticks=element_blank(),
          panel.border = element_rect(color="gray"),
          legend.title = element_blank(),
          legend.position = "right", #legend.position=(if (hide_legend) "none" else "right"),
          ) +

    ggtitle(title) +
    (if (log) scale_y_continuous(trans = "log10")) +
    (if (baseline) geom_hline(yintercept=1, color="#666666", linetype="dashed", size=1)) +

    (if (warmup)
       geom_point(position=cold_position,
                 shape=4,
                 size=sz,
                 color="black",
                 aes(row=row),
                 data=subset(d, warm == 0))) +
    geom_point(position=warm_position,
              size=sz,
              aes(color=experiment, row=row),
              data=subset(d, warm == 1)) +
    geom_boxplot(outlier.shape = NA, aes(color=experiment), data=subset(d, warm == 1)) +

    (if (trend != F && !multipleBm) {
       # TODO find out how to draw trend on multiple ones
       st = trend[trend$benchmark == d$benchmark_, ]$prediction_start[1]
       ed = trend[trend$benchmark == d$benchmark_, ]$prediction_end[1]
       printResult(paste0("TrendLineEnds", d$benchmark_[1]), (ed-1)*100, 2)
       geom_segment(aes(x = 1, y = st, xend = 7, yend = ed), size=0.01, color="blue")
    })
    # theme(axis.text.x = element_text(angle=90))
  )
}

pl <- function(cmp, name, selection) {
  shorten_suite = function(s) switch(s, "are-we-fast-r"="awf", "real_thing"="re", "shootout"="sht", "simple"="μ")

  cmp$benchmark_ = cmp$benchmark
  cmp$suite_short = lapply(cmp$suite, shorten_suite)
  cmp$benchmark = paste0("[",cmp$suite_short,"] ",cmp$benchmark)

  ncol=3

  cmp <- normalize(cmp, T, "baseline")

  cmp_ = cmp[cmp$benchmark %in% selection, ]
  bms_ = length(unique(cmp_$benchmark))

  cairo_pdf(paste0(name,".pdf"), width=ncol*2/(ncol/min(ncol,bms_)), height=2.5*bms_/ncol)
  plot_bm(NULL, cmp_, warmup=F, relative=T, ncol=ncol, hide_legend=T, scales="fixed", baseline=F)
  dev.off()
}

setwd("experiments")
dir.create("final", showWarnings=F)
setwd("final")

require(ggrepel)

# motivation
#############

d = read.csv("../motivation.csv")
d$annotation = ""

d[d$experiment == "baseline" & d$run == "3",]$annotation = "1"
d[d$experiment == "baseline" & d$run == "10",]$annotation = "2"
d[d$experiment == "baseline" & d$run == "20",]$annotation = "3"
d[d$experiment == "profiler" & d$run == "23",]$annotation = "4"

d$experiment = factor(d$experiment, levels=c("profiler", "baseline"))

cairo_pdf("motivation.pdf",width=3, height=1.5)
print(ggplot(d, aes(run,time))) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=10),
        text=element_text(size=10)
        ) +
  ylab("time (s)") +
  xlab("iteration") +
  geom_text_repel(aes(label = annotation, color=experiment),
                       size = 4,
                       seed = 1,
                       nudge_y=0.2,
                       nudge_x=1) +
 # geom_vline(xintercept=runs+0.5, linetype="dashed", color="gray", size=0.1) +
  geom_line(aes(color=experiment), size=0.5)

dev.off()

#  Experiment 1
###############

only_oh_100k=625260522
only_oh_1M=625194097
only_oh_500k=626774621

overhead <- list(
  `baseline`=630106305,
  `1M`=only_oh_1M,
  `500k`=only_oh_500k,
  `100k`=only_oh_100k)
oh <- fetch_jobs(overhead)
oh <- oh[oh$suite != 'simple' & oh$warm == 1,]
bms <- unique(oh$benchmark)
oh2 <- data.frame()
oh2$benchmark = character()
oh2$experiment = character()
oh2$overhead = numeric()
bl = oh[oh$experiment == "baseline",]

for (o in c("1M", "500k", "100k")) {
  t <- oh[oh$experiment == o, ]
  for (b in bms) {
    time <- median(t[t$benchmark == b,]$ms)
    base <- median(bl[bl$benchmark == b,]$ms)

    oh2[nrow(oh2)+1,] <- list(
      benchmark=b,
      experiment=o,
      overhead=time/base)
  }
}

dump <- function(experiment) {
  print(paste("experiment: ", experiment))
  print(paste("geom.mean: ", geometric.mean(oh2[oh2$experiment == experiment, ]$overhead)))
  print(paste("over 3: ", length(oh2[oh2$experiment == experiment & oh2$overhead >= 1.03,]$overhead)))
  print(paste("over 10: ", length(oh2[oh2$experiment == experiment & oh2$overhead >= 1.1,]$overhead)))
  print(paste("worst: ", oh2[oh2$experiment == experiment & oh2$overhead == max(oh2[oh2$experiment == experiment,]$overhead),]$overhead))
  print(paste("worst: ", oh2[oh2$experiment == experiment & oh2$overhead == max(oh2[oh2$experiment == experiment,]$overhead),]$benchmark))
  print(".")
}

dump("1M")
dump("500k")
dump("100k")

oh2$experiment = factor(oh2$experiment, levels=c("100k","500k","1M"))
cairo_pdf(paste0("overhead",".pdf"), width=10, height=4)
print(ggplot(oh2, aes(x=benchmark, y=overhead, color=experiment, group=experiment)) +
    facet_wrap(~experiment, ncol=3, strip.position="left", scales="fixed") +
    theme_bw() +
    scale_colour_brewer(palette = "Dark2")+
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.title.y=element_blank(),
          axis.ticks=element_blank(),
          panel.grid.major=element_blank(),
          panel.border = element_rect(color="gray"),
          legend.title = element_blank(),
          legend.position="none") +
    geom_hline(yintercept=1, color="#666666", linetype="dashed", size=1) +
    geom_text_repel(aes(label=ifelse(overhead>1.045,as.character(benchmark),'')),hjust=1.1, vjust=-1.2) +
    geom_point())

dev.off()

#  Experiment 2
###############

miss1M <- list(
  `T10`= 628689517,
  `T15`= 630127269,
  `T20`= 630148558,
  `T50`= 630191289,
  `T75`= 630578770,
  `T100`= 630101209,
  `T150`=631560542,
  `T200`=631550493
)
miss500k <- list(
  `T10`= 630134419,
  `T15`=631517665,
  `T20` = 630801978,
  `T50`=630858589,
  `T75`=631529475,
  `T100`= 630586859,
  `T150`=630868005,
  `T200`=631539984
)
miss100k <- list(
  `T10`= 629298563,
  `T15`= 630123446,
  `T20`= 630141686,
  `T50`= 630183783,
  `T75`=630086509,
  `T100`= 630116687,
  `T150`= 630093006,
  `T200`= 630096953
)

miss_baseline = list(`100k`=only_oh_100k, `500k`=only_oh_500k,
                     `1M`=only_oh_1M)
miss_baseline=fetch_jobs(miss_baseline)


miss1M <- fetch_jobs(miss1M)
miss100k <- fetch_jobs(miss100k)
miss500k <- fetch_jobs(miss500k)
miss1M$samples = miss1M$experiment
miss100k$samples = miss100k$experiment
miss500k$samples = miss500k$experiment
miss1M$experiment="1M"
miss100k$experiment="100k"
miss500k$experiment="500k"

miss <- merge(merge(miss100k, miss1M, all=TRUE), miss500k, all=TRUE)
miss <- miss[miss$suite != 'simple' & miss$warm == 1,]
miss2 <- data.frame()
miss2$experiment = character()
miss2$samples = character()
miss2$outliers = numeric()
miss2$rawout = numeric()

thr = 1.1
for (e in unique(miss$experiment))
  for (s in unique(miss$samples)) {
    miss2[nrow(miss2)+1,] = list(experiment=e, samples=s, outliers=0)
    for (b in unique(miss$benchmark)) {
      d = miss[miss$experiment == e & miss$benchmark == b & miss$samples == s,]
      bl = miss_baseline[miss_baseline$experiment == e & miss_baseline$benchmark == b,]
      m = median(bl$ms)
      l = m*thr
      out = length(d[d$ms > l,]$ms)
      out = out + miss2[miss2$experiment == e & miss2$samples == s,]$outliers
      miss2[miss2$experiment == e & miss2$samples == s,]$outliers = out
      miss2[miss2$experiment == e & miss2$samples == s,]$rawout = out
    }
    miss2[miss2$experiment == e & miss2$samples == s,]$outliers =
      miss2[miss2$experiment == e & miss2$samples == s,]$outliers /
      length(miss[miss$experiment == e & miss$samples == s,]$ms)
  }


miss2$samples = as.integer(substring(miss2$samples,2))

print(miss2)

miss2$experiment = factor(miss2$experiment, levels=c("100k","500k","1M"))

miss2$period = miss2$experiment

cairo_pdf(paste0("miss",".pdf"), width=4, height=4)
print(ggplot(miss2, aes(x=samples, y=outliers, color=period, group=period)) +
    theme_bw() +
    scale_colour_brewer(palette = "Dark2")+
    theme(#axis.title.x=element_blank(),
          axis.ticks=element_blank(),
          panel.border = element_rect(color="gray"),
          legend.position="bottom") +
    ylab("outliers (relative)")+
    #geom_smooth(method='lm', formula=(y~poly(x,2)), se=F, size=0.5) +
    #geom_spline(aes(x=samples, y=outliers,color=experiment, group=experiment), all.knots) +
    geom_line()
    +geom_point()
     )

dev.off()

#  Experiment 3
###############

exp <- list(
  `baseline`=630106305,
    `100k/50`= 630086509,
    `500k/20`=630801978,
    `1M/20` = 630148558)

selection <- c(
 "[μ] profiler-microbenchmark",
 "[μ] profiler-rsa",
 "[μ] profiler-shared"
)
res <- fetch_jobs(exp)
pl(res, "improvement", selection)


system("touch done")
