lcg <- function(n) for (i in 1:n) state <<- (state * 48271) %% 0x7fffffff

state <- c(1,2,3)
for (i in 1:10) {
  t=system.time(lcg(5000000))
  cat(paste0(i,",",t[[1]],"\n"))
}
state <- c(1,2,3,4,5,6)
for (i in 1:10) {
  t=system.time(lcg(5000000))
  cat(paste0(10+i,",",t[[1]],"\n"))
}
state <- 1
for (i in 1:10) {
  t=system.time(lcg(5000000))
  cat(paste0(20+i,",",t[[1]],"\n"))
}
