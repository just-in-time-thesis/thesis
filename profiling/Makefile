SHELL := /bin/bash

LATEXMK := $(shell command -v latexmk 2> /dev/null)
LATEX   := pdflatex
TEXFILE := main
JAN     := $(shell git config user.name | grep vitek)

BIB    := bib/pir.bib

EXPERIMENT := experiments/plot.R
EXPERIMENT_DONE := experiments/final/done

all: main.pdf
jan: main.pdf

$(TEXFILE).pdf: $(TEXFILE).tex $(BIB)
ifdef JAN
	$(LATEX) $<
	bibtex $(subst .tex,.aux,$<)
else
ifndef LATEXMK
	$(LATEX) $<
	bibtex $(subst .tex,.aux,$<)
	$(LATEX) $<
	$(LATEX) $<
else
	latexmk -pdf $<
endif
endif

$(EXPERIMENT_DONE): $(EXPERIMENT)
	Rscript $(EXPERIMENT)

watch:  all
	evince main.pdf&
	while true; do inotifywait $(TEXFILE).tex $(BIB); make; done

clean:
	rm -f *.aux *.blg *.bbl *.cut *.log *~ *.out *.pdf

spellcheck:
	hunspell -d en_US -p hunspell_dictionary main.tex

ifdef JAN
PHONY := $(TEXFILE).pdf
endif
.PHONY: all clean spellcheck $(PHONY)
