This dissertation makes several contributions to the field of
just-in-time compilers. \Sourir is the first formalization of
deoptimization. It allows for disentangling the problem of
speculative optimizations and how to correctly undo them, from the problem of
how to implement the mechanism that does it. \Sourir marks a shift in how
verification and compiler correctness for just-in-time compilation is approached.
Instead of starting with properties about self-modifying code, \ie at the bottom
of any abstractions, it starts
with invariants and contracts for JITs and reasoning at a higher level
of abstraction. JIT compilation does not rely on unlimited self-modifying code,
instead a small number of well-defined interfaces, such as dispatch tables for
specialization, small patch-point regions for invalidation, or lookup caches
suffice. As such we can model these requirements at a high level and then
reason about how to lower them to actual hardware later. This approach already
led to further progress in an extended formalization called \JIT, which does not only include the optimizer
in the formalization, but also the run-time code generation and modification of
dispatch tables. This focus on the \emph{interface of speculation} allows us
to understand the idea of speculative optimizations detached from the actual
implementation details. A trend that has also picked up in practice, where we
see more and more speculative optimization approaches using high-level, sometimes
even source-to-source translation, implementation techniques.

In a similar vein,
the second contribution \cd does not provide a fundamentally new optimization
for JIT compilers. Instead it unifies a very fragmented landscape of
code specialization. The way \cd supports and provides optimizations up to a dynamic context
of assumptions proved itself fruitful in a number of situations. It
allow us to describe many existing dynamic code specialization
techniques in a unified way. But, it also provides a middle ground between
a hard call boundary and an inlined call, when an optimizer tries to
optimize code with the help of contextual information from the caller.
Furthermore, \cd leads to a novel way of handling failing speculation
with deoptless continuations. The idea is motivated by the observation
that deoptimization produces code that is getting gradually more and more
generic. Like a call-site, an OSR-exit point from a failing speculation, naturally provides
a context of dynamic information, that can be used to optimize the remainder
of the function. This allows for optimized-to-optimized recovery of speculation and thus
sub-method sized specialization, which could be understood as an on-demand
exploration of traces with similar behavior.

With this dissertation I have shown that the assume instruction and \cd can tend to all the
speculative needs of a language implementer.
The \R virtual machine has an
optimizing compiler that uses speculation as suggested by \sourir and \cd for
all its dynamic optimizations.
It is therefore possible to
directly apply these design recipes to implement a competitive compiler and combined
they are sufficient to implement any required optimization up to dynamic assumptions.

\section{Future Work}

\paragraph{Speculative Optimizations}

There are multiple avenues of future investigation. The optimizations presented
here rely on intraprocedural analysis and the granularity of deoptimization
is a whole function.  If we were to extend this work to interprocedural
analysis, it would become much trickier to determine what functions are to
be invalidated as a speculation in one function may allow optimizations in
many other functions. When we interprocedurally analyze a
callee function with an \assume instruction, then we have to consider that this
assumption could fail and the function could effectively do anything before
returning to the caller. It is an open problem how the effect on the
analysis state could be contained in such a situation.

The current style of \assume instructions forces to check predicates
before each use, but some predicates are cheaper to check by monitoring
operations that could invalidate them. We discuss an extension where
a global array of properties is used for monitored assumptions. Still, it would
be a good idea to incorporate this extension natively into the model.
This would require changes as the \assume instruction would need to be split between a
monitor and a deoptimization point. Lastly, the expressive power of
predicates is an interesting question as there is a clear trade-off ---
richer predicates may allow more optimizations but are likely to be costlier
to monitor.

\JIT includes a semantic for a JIT optimization loop, where at every call
boundary it is possible to decide to either execute or first optimize the target
function.
Future work includes proving more optimizations
and extend \IR and \JIT to a more realistic language such as RTL.
To establish the correctness of a translation to a native code backend the methodology would need to be
extended.
It remains an open questions, whether end-to-end
verification of a JIT is possible, using a black-box native backend.
This raises the challenge of modular verification
and linking across languages.

\paragraph{Context Dispatch}

As is the explored set of predicates that make
up a context are basic.  To add richer properties and increase flexibility, we
may have to change the dispatching technique. One idea would be to
develop a library of simple building blocks, such as predicates,
decision trees and numerical ordering; such that their combination
still results in a context with efficient implementation and
representation. The key challenge will be to control the cost of
deriving contexts at run-time. For this we are considering improving
our compiler's ability to evaluate contexts statically. Another
direction comes from the observation that different contexts can lead
to code that is almost identical, it is an interesting question how to prevent
generating versions that do not substantially improve performance.

As for broader applicability, we believe contextual dispatch can be used
even in typed languages to capture properties that are not included in the
type system of the language. For instance, in Java one could imagine
dispatching on the erased type of a generic data structure, on the length of
an array, or on the fact that a reference is unique. Whether this will lead
to benefits is an interesting research question.

An interesting avenue for future work, in particular in combination with
\emph{deoptless}, would be to try to recombine
contextually optimized fragments into one function. The information from the contexts
could be used
to fuse all versions into one optimized function, which is still specialized to
the observed contexts, but gets rid of dispatching and code-size overhead.
