\input{experiments/results}


The evaluation of my thesis is twofold. For one, it is a statement about
feasibility. It is possible to build a JIT that closely follows the proposed designs.
This is already shown in the previous \autoref{chpt:implementation},
which describes \R in detail. Secondly, it states that it is possible to build a
competitive JIT compiler. This is what this chapter intends to demonstrate.


\section{Methodology}

Non-determinism in processors, \eg due to frequency scaling or power
management, combined with the adaptive nature of just-in-time compilation,
make measuring performance challenging. Instead of relying on the one
perfect measurement, we continuously monitor performance on every commit. This
allows us to spot unstable behavior and sanity check all the reported numbers.
All experiments are run on the same, dedicated machine. We do not
lock the CPU frequency, as this would not correspond to a real-world scenario.

To deal with warmup phases of the virtual machine,
\ie iterations of a benchmark during which compilation events dominate
performance, we run each benchmark fifteen times in the same process and
discard the first five iterations.
To further mitigate the danger of incorrectly categorizing
the warmup phase~\citep{bar17}, we plot individual measurements in the
order of execution.

For the experiments we use the major benchmarks from the \R
benchmark suite.
The suite consists
of several programs that range from micro-benchmarks, solutions to small
algorithmic problems, and real-world code. Some programs are variants; they use
different implementations to solve the same problem. We categorize the programs by
their origin: \newcommand{\Awf}{{[awf]}\xspace} \renewcommand{\Re}{{[re]}\xspace} \newcommand{\Sht}{{[sht]}\xspace}
\newcommand{\Mu}{{[mi]}\xspace}
\begin{itemize}
  \item[\textsf{awf}] We translated three benchmarks to R
    from~\citet{mar16}: \c{Bounce}, a bouncing balls physics simulation;
    \c{Mandelbrot}, to compute the Mandelbrot set; and \c{Storage}, a
    program that creates trees.
  \item[\textsf{sht}] The Computer Language Benchmarks Game~\citep{shootout},
    ported to R by~\citet{vee14}.  The suite contains
    multiple versions of classic algorithms, written to explore different
    implementation styles. Most of the original programs had explicit
    loops, so the suite provides more idiomatic R versions that rely on
    vectorized operations.
  \item[\textsf{re}]  \c{Flexclust} is a clustering algorithm from the
    flexclust package~\citep{flexclust}. It exercises many features that are
    hard to optimize, such as generic methods, reflection, and
    \c{lapply}. The \c{convolution} benchmark consists of two nested loops updating a
    numerical matrix; it is an example of code that is typically rewritten in C for
    performance. Finally \c{volcano} is a raycast renderer.
  \item[\textsf{mi}] Code fragments known by the R community to be slow.
  These are microbenchmarks, such as simple loops, which are well optimized for, but too small to draw
  conclusions for real programs.
\end{itemize}

\noindent
Experiments are run on a dedicated benchmark machine, with all background
tasks disabled. The system features an Intel i7-6700K CPU, stepping 3, microcode
0xea with 4 cores and 8 threads, 32 GB of RAM.
Unless noted otherwise, the experiments are run on Ubuntu
18.04 on a 4.15.0-151 Linux kernel. Experiments are built as
Ubuntu 20.04.1 based containers, and executed on the Docker runtime 20.10.7.\footnote{We use a containerized environment to automate
measurements and verified that it does not distort the results.} Measurements are recorded repeatedly and we keep a historical
record to spot unstable behavior.
We used GNU R version 4.1, FastR from GraalVM 22.0.0.2; and \R commit 6a40e7ed.

\graphicspath{ {./cd/} }
\section{Baseline}

Studying the performance of GNU R, FastR, and \R allows us to compare a
lightly optimizing bytecode interpreter and two optimizing just-in-time
compilers.
The systems feature different implementation strategies and trade-offs. This
comparison allows us to answer if \R is competitive with regards to the reference
implementation and also a state of the art optimizing compiler.

An important question when comparing implementations is their compliance;
partial implementations can get speedups by ignoring features that are
difficult to optimize. The GNU R interpreter is the reference
implementation, so it is compliant by definition.  As of this writing, \R is
compliant with version 4.1 of GNU R, verified by running the full GNU R test
suite and the tests of its recommended packages. The extent of FastR's compliance
is unclear.%
\footnote{In earlier experiments, we were unable to make FastR 3.6.1 from GraalVM
19.3.1 pass 5 out of 15 of
the recommended packages in GNU R's test suite.}

{\small
\rshSpeedupOverGnur%
\rshSpeedupOverFastr%
\rshColdSpeedupOverFastr%
\rshColdSpeedupOverGnur%
}

\begin{figure}
\centering
\includegraphics[width=0.48\textwidth]{experiments/arewefast-warm}
\hfill
\includegraphics[width=0.5\textwidth]{experiments/realthing-warm}
\vfill
\includegraphics[width=1\textwidth]{experiments/shootout1-warm}
\vfill
\includegraphics[width=1\textwidth]{experiments/shootout2-warm}
\caption{Speedup of \R (left) and FastR (right) over GNU R (log scale)}
\label{fig:perf-graphs}
\end{figure}


We first report the geometric mean of the speedup over the parts of the benchmark
suite, normalized to the median execution time of GNU R (higher is better).
On the microbenchmarks \R is on average $28\times$ faster than GNU R, however we
consider it of limited value for predicting performance of real-world code and
thus exclude it from further reporting.
On the remainder, the speedup of \R over GNU R is reported in \autoref{tab:rshSpeedupOverGnur} and the speedup,
or slowdown over FastR in \autoref{tab:rshSpeedupOverFastr}. To summarize these
findings, \R can be expected to run shy of two times faster than the reference
implementation and it is
sometimes competitive with FastR, though with a very different performance profile. Over
the whole suite \R regresses on only 8 out of 64 benchmarks over GNU R and never
more than 35\%.
Compared to FastR, a JIT compiler written by a well financed and large team, the
performance of \R is mixed, but more stable, with less severe regressions over
GNU R. Additionally, if we focus on the warmup behavior and only measure
the first in-process iteration, we notice that in many cases \R warms up
quicker, as can be seen in \autoref{tab:rshColdSpeedupOverFastr}. Compilation overhead is still
significant due to our expensive optimizations and the LLVM backend. As can
be seen in \autoref{tab:rshColdSpeedupOverGnur} this can lead to very large
slowdowns in the worst case.

Finally, for a more fine-grained understanding of our performance, we show the
performance for each benchmark separately in \autoref{fig:perf-graphs}.
Each graph shows the relative speedup over GNU R for \R (left) and FastR (right) separately.
This graph shows only peak performance, the first 5 in-process iterations
are excluded. The small dots represent the individual iterations from left to
right. The large dot and (if visible) bars show the mean and confidence interval.

In summary \R can achieve similar performance to FastR, but can also be significantly slower when the
benchmark relies on features that are not optimized in \R.  For example in
\c{flexclust}, because the benchmark
uses features that currently cause \R to give up compiling some methods.
While FastR can indeed be fast, it is worth noting that
there is a large variance for peak performance in both directions, when compared
to the GNU R interpreter. For instance \c{Storage}, \c{regexdna} and some
\c{knucleotide} implementations are much
slower in FastR than in GNU R, \c{pidigits} and \c{binarytrees} have very large
amounts of variability.
Overall \R shows a more balance performance profile and is equally
capable of very large speedups for some benchmarks.

\section{Speculation}

Next I report how much of the performance of \R can be
attributed to speculation. To that end we disable speculative optimizations and
run the same benchmark suite. In particular we investigate the following
configurations:

\begin{itemize}
  \item[\textsf{C0}]No speculation on environments not escaping. This is used to
    speculate that an otherwise only locally accessed environment is not accessed
    reflectively by callees.
  \item[\textsf{C1}]No speculative dead-branch removal. This affects the ability
    to optimize for integer sequences, or exclude some slow array access, when the
    vector could be an object.
  \item[\textsf{C2}]No speculative static call targets. This affects
    monomorphization of calls, and this inter-procedural optimizations and
    inlining.
  \item[\textsf{C3}]No speculation on types. This mainly prevents unboxing of
    primitive types and elision of environments, due to reduced analysis precision
    because of lazy evaluation.
  \item[\textsf{C4}]All speculation disabled, \ie all of the above.
\end{itemize}

It is difficult to attribute performance differences to particular
optimizations in isolation. Many optimizations in \R are designed to work in
combination and thus some of the observed effects here are exaggerated. For instance
disabling type speculation has a big effect on \R because the native backend
only unboxes static types. Thus, as an example, if it is not statically known
modulo speculation, if the
result of an addition is an integer or a floating point number, then the
addition will be performed on tagged numbers. Nevertheless these measurements
should give us some intuition and in particular an upper bound for how much \R
relies on speculation to achieve its performance.

{\small
\rshWhithoutSpeculation
}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{experiments/rsh-nospec}
\caption{Slowdown of \R without speculation}
\label{fig:perf-graphs-assume}
\end{figure}

The results are summarized in
\autoref{tab:rshWhithoutSpeculation}, normalized to the median execution time of
normal \R. Overall,
excluding microbenchmarks, disabling speculation leads to slowdowns of
$0.56\times$ to $0.27\times$ on the benchmark suites, with individual benchmarks
ranging between $0.016\times$ to $1.19\times$. On the microbenchmarks the effect
is even more dramatic.
Detailed results for each configuration by
benchmark suite can be seen in \autoref{fig:perf-graphs-assume}.
Again, the large dots show means and confidence interval, additionally a violin
plot reveals the large variance in the individual benchmarks.
In some of these plots two or or several modes are revealed in the data, with
some benchmarks heavily affected, while others
not at all. This is expected for two reasons. First, some benchmarks are not
sped up by \R in general, thus disabling speculation is expected to have a small
effect on that subset. Second, since many of the benchmarks are not that large,
the typically few, but different impactful optimizations tend to focus on some key functions.
These numbers show that the speedups reported in
\autoref{tab:rshSpeedupOverGnur} would be very difficult
to achieve without speculation.

\section{Context Dispatch}%

This section tries to answer the question of how much \cd contributes to the baseline
performance of \R.
To that end, we disable individual assumptions
that make up a context and study their impact on performance.
Importantly, each and every assumption has an equivalent substitute
speculative optimization in \R's optimizer as described in \autoref{sec:opts}.
Performance improvements in this section are therefore not due to additional
speculative capabilities, but solely due to splitting into multiple versions and the
specialization to multiple contexts, or due to reduced overhead from having less
deoptimization points.

The experiments in this section are based on the \R version bundles by \citet{flu20-artifact} and were run on
a Linux kernel version 4.15.0-88, GitLab runner version 12.9.0,
in Docker version 19.04.8.

Unfortunately, it is not possible to turn off \cd altogether
as it is an integral part of the \R compiler.
Each function starts with a
dispatch table of size one, populated with the unoptimized version of the
function. To achieve a modicum of performance, it is crucial to add at least
one optimized version to the dispatch table.  The unoptimized version
cannot be removed as it is needed as a deoptimization target.  What we can
do is to disable some of the assumptions contained within a context.
Thus, to evaluate the impact of \cd, we define seven,
cumulative, optimization levels:

\begin{itemize}
  \item[\textsf{L0}]\ccode{NotTooManyArgs} and \ccode{CorrectOrder} are
    fundamental assumptions required by \R;
  \item[\textsf{L1}]\ccode{ArgNEager} for arguments that are evaluated promises;
  \item[\textsf{L2}]\ccode{NoReflectiveArg} specifies that promises do not
    use reflection;
  \item[\textsf{L3}]\ccode{ArgNNotObj} for arguments that do not have the
    \c{class} attribute;
  \item[\textsf{L4}]\ccode{ArgNSimpleInt} or \ccode{ArgNSimpleReal} for arguments
    that are scalars of integers or doubles;
  \item[\textsf{L5}]\ccode{missing} for a lower bound on missing arguments
    (from the end of argument list); and
  \item[\textsf{L6}]\ccode{NoExplicitlyMissingArgs} to ensure that \ccode{missing} is the
    exact number of missing arguments.
\end{itemize}


\noindent For this experiment we, pick L0 as the baseline, as it is the
optimization level with the fewest assumptions in the context. For each
benchmark, we report results for each of L0 to L6, normalized to the median
execution time of L0 (higher is better).
%
\begin{figure}
  \centering
  \includegraphics[width=.42\textwidth]{experiments/performance/final/spectralnorm-specialization.pdf}
  \caption{Impact of optimization levels 0 to 6 (from left to right)}
  \label{fig:perf-specialization-example}
\end{figure}
%
\autoref{fig:perf-specialization-example} shows the results of the
experiment for \c{spectralnorm}. Each level has its own box plot.  The first
box plot from the left is for L0 (and its median is set to one) and the last
corresponds to L6. Dots show individual measurements.  The blue line is the
lower bound of the 95\% confidence interval of a linear model.  In other
words, \c{spectralnorm} is predicted to improve at least
\resultTrendLineEndsspectralnorm\% due to \cd. The largest changes in the
emitted code can be seen in L2. The \ccode{NoReflectiveArg} assumption
enables the optimizer to better reason about several functions. These
optimizations are preconditions for the jump in L6, but yield fewer gains
themselves. The improvement in L6 can be pinpointed to the builtin
\c{double} function, with the signature \c{function(length=0L)}.  The
\ccode{NoExplicitlyMissingArgs} assumption allows us to exclude the default
argument.  The function is very small and is inlined early. However,
statically approximated \cd allows the compiler to inline a version of the
\c{double} function, which is already more optimized. This gives the
optimizer a head start and leaves more optimization budget for the
surrounding code.

\paragraph{Results}

\autoref{fig:perf-specialization} shows the performance impact of \cd on a
representative sample of 16
of the \resultBenchmarksInTotal benchmarks.
%
\begin{figure}
  \includegraphics[width=0.99\textwidth]{experiments/performance/final/specialization.pdf}
  \caption{Impact of optimization levels 0 to 6 (left to right)}
  \label{fig:perf-specialization}
\end{figure}
%
In general, we see a trend for higher levels to execute faster. The
effects are sometimes fairly small; note that each graph has a different
scale. The outliers in \c{binarytrees} are caused by garbage collection. Some
benchmarks have a large response on L1 or L2. The reason is that code
that invokes all of the benchmarks is passing a constant, to specify
the workload, and the assumptions from L1 and L2 allow us to rely
that this argument is benign (e.g. that it does trigger reflection).

The aim of our experiment is to test if \cd significantly contributes to the
overall performance of \R. Often, optimizations do not benefit all programs
uniformly, and can even degrade performance in some cases.  We are therefore
interested in the number of benchmarks which are significantly improved (or
not worsened) over a certain threshold.  We formulate the null hypothesis:

\begin{itemize}
  \item[\textsf{H0}] \CD does not speed up the execution of a benchmark by
    more than N\%.
\end{itemize}

\noindent
We test H0 for various improvement thresholds, by fitting a linear model and
testing its prediction for the lower bound of the 95\% confidence interval
at L6 (see the blue line in \autoref{fig:perf-specialization-example}).  As
can be seen in the summarized results from
\autoref{fig:perf-specialization-test}, we conclude that \cd might slow
down the execution of two benchmarks by more than 5\%, improve 39\% of
benchmarks, and improves four benchmarks by more than 10\%. Additionally,
more than half of the benchmarks in \Mu see a speedup greater than 20\%.

\begin{table}
\caption{Number of benchmarks significantly improved ($\neg$H0 with $p=.05$) out
  of \resultBenchmarksInTotalNonMu, and \resultBenchmarksInTotalMu (\Mu)}
\centering \small \begin{tabular}{|r || r | r | r |}
  speedup & \Awf \Sht \Re & \Mu \\\hline
  -5\%    &  \resultHRejectMinusFive & \resultHRejectMinusFiveMu \\
  0\%     &  \resultHRejectOne & \resultHRejectOneMu \\
  2\%    &  \resultHRejectPlusTwo & \resultHRejectPlusTwoMu  \\
  5\%    &  \resultHRejectPlusFive & \resultHRejectPlusFiveMu  \\
  10\%    &  \resultHRejectPlusTen & \resultHRejectPlusTenMu  \\
  20\%    &  \resultHRejectPlusTwenty & \resultHRejectPlusTwentyMu  \\
\end{tabular}
\label{fig:perf-specialization-test}
\end{table}

\begin{table}
\caption{\cd statistics}
\centering \small \begin{tabular}{|l || r | r | r |}
  measurement & min & max & mean \\\hline
  contexts per call-site & 1 & 4 & 1.00003 \\
  call-sites per version & 1 & 88 & 1.89 \\
  versions per callee & 1 & 38 & 1.58 \\
  occurrence per context & 1 & 1131 & 31.37 \\
\end{tabular}
\label{fig:perf-specialization-stats}
\end{table}

Executing the benchmark suite with level 6 we observe 199 distinct contexts.
Across all call-site and context combinations the
most common context is
\ccode{NoExplicitlyMissingArgs}, \ccode{CorrectArgOrder},
\ccode{NotTooManyArgs}, \ccode{NoReflectiveArg},
\ie the context with
no explicitly missing arguments, where the caller can pass arguments in the
correct order, does not pass too many arguments and all the passed arguments do
not invoke reflection. This context occurs 1131 times, closely followed by the
one without \ccode{NoReflectiveArg}, which is also the minimal context for optimized code.
There is a long tail of specialized contexts; 145 contexts occur less than 10
times and 61 contexts just once. \autoref{fig:perf-specialization-stats} shows
some key numbers regarding the call-site and target version pairs. Almost all
call-sites in our benchmark suite observe a single dynamic context, in other
words the contexts employed by \R are almost exclusively monomorphic.
The individual function versions are shared between 1 to 88
call-sites, on average every version has almost 2 originating call-sites. The
number of invoked versions per closure is surprisingly small, indicating that the
benefit of \cd is focused at a few call-sites. For these numbers we use the AST
node of a call-site as a proxy for call-sites and we have to exclude bogus
call-sites, \ie function version pairs from the \code{flexclust} benchmark, which
occur multiple times due to a bug causing excessive re-compilation.

\paragraph{Discussion}
The effects reported in this section can sometimes be subtle. Arguably
\R is already a fairly good optimizer without \cd.  It employs a
number of optimizations and speculative optimizations, which speculate on
the same properties.  We investigated the number of versions per function in
\c{pidigits} and found them to range between 1 and 6. Many functions that
belong to the benchmark harness or are inlined stay at 1 or 2 versions with
few invocations. The functions with many versions concentrate on a few. A
big hurdle for \cd in R is that it is not possible to check the types of
lazy arguments at the time of the call. For instance, there is a
user-provided \c{add} function that has 12 call sites with several different
argument type combinations. However, \R is not able to separate the types
with \cd, because all call sites pass both arguments lazily. As predicted, this results in several deoptimizations and
re-compilations, leading to a fairly generic version in the end. We see this
as an exciting opportunity for future work, as it seems that \cd should be
extended from properties that \emph{definitely} hold at function entry to
properties that are \emph{likely} to hold at function entry. This would
allow for multiple versions, each with different speculative optimizations,
to be dispatched to depending on how likely a certain property is.

We investigated if garbage collection interferes with measurements. To that
end, we triggered a manual garbage collection before each iteration of the
experiment. Indeed, we observed slightly more significant results for the
numbers reported in \autoref{fig:perf-specialization-test}. To keep the
methodology consistent with the previous section, where manually triggering
a garbage collection would distort the results, we decided to keep the
unaltered numbers.

We find the results presented in this section very encouraging, as they show a
significant advantage of \cd over speculation.
Additionally, and this is difficult to quantify, we believe
that \cd has helped improve \R in two important ways. First, there is a
one-stop solution for specialization. This makes it easy to add new
optimizations based around customizing functions, but we also use it
extensively in the compiler itself. The compiler uses static contexts to
keep different versions of functions in the same compilation run, to drive
splitting and for more precise inter-procedural analysis. The second benefit
is that \cd has helped to avoid having to implement each and
every one of the painstakingly many corner cases of the R language. For
instance, we can assume that arguments are passed to functions in stack
order, and if for one caller our system does not manage to comply with this
obligation, \cd automatically ensures that the baseline
version without this assumption is invoked.

\graphicspath{ {./deoptless/} }
\section{Deoptless}

The final contribution to evaluate is deoptless.
Speculation and \cd are part of \R in its default configuration.
Therefore in the previous sections we presented how much they contribute to the
baseline performance. Deoptless is a feature still under evaluation and the
reported numbers state how we would improve the baseline by enabling deoptless.
The stated goals of deoptless are to
\begin{enumerate}
  \item reduce both the frequency and amplitude of the temporary slowdowns due
    to deoptimizations, and
  \item prevent the long-term over-generalization of code due to deoptimization and
    recompilation.
\end{enumerate}
According to these goals, we try to answer the following questions:
(1) Given the same deoptimization triggering events, what is the speedup of using deoptless?
(2) Is deoptless able to prevent over-generalization?

The nature of deoptless makes it challenging to answer these questions as the events we are
trying to alleviate are by definition rare. In particular the code produced by
\R is not going to cause many deoptimizations in known benchmark suites.
Therefore, we decided to perform our main evaluation of deoptless on the worst-case situation, where we
randomly fail speculations.
Secondly, we will
evaluate deoptless on bigger programs, with known deoptimizations, due to the
nature of their computations.

The experiments in this section are based on a published artifact
\citep{flu22-artifact} and were run with the same configuration as the baseline
experiment.

\paragraph{Speedup over Deoptimization}

First we want to evaluate the performance gains of deoptless from avoiding
deoptimization alone. To that end we take the default \R main benchmark suite and
randomly invalidate 1 out of 10k assumptions. To be precise, we only trigger
deoptimization without actually affecting the guarded fact. This is achieved by
instrumenting the compiler to add a random trigger to every run-time check of an
assumption. This is an already existing feature of \R used in development to test the
deoptimization implementation. Enabling this mode causes a large slowdown of
the whole benchmark suite. We then measure how much of
that slowdown can be recovered with deoptless. Note that this is a worst-case
scenario that does not evaluate
the additional specialization provided by deoptless, as the triggered
deoptimizations largely correspond to assumptions that in fact still hold.
We run this experiment with 30 in-process iterations times 3 executions.
The results are presented in \autoref{fig:deoptless-speedup}. The
large dots in the graph show the speedup of deoptless over the baseline on a log
scale on average.
Improvements range from $\perfMin\times$ to
$\perfMax\times$,
with most benchmarks
gaining by more than $\perfMed\times$. The small dots represent in-process iterations from left
to right, averaged over all executions. We exclude the first 5 warmup
iterations, as they add more noise and only slightly affect the averages. Normalization is done for every dot
individually against the same iteration number without deoptless. From the main benchmark suite we
had to exclude the \c{nbody_naive} benchmark, as it takes over one
hour to run in the deoptimization triggering test mode. Though, we would like to
add, that with deoptless this time is cut down to less than five minutes.
Overall this experiment shows that deoptless is significantly faster then
falling back to the interpreter for the \R benchmark suite.

\begin{figure}[t]
  \includegraphics[width=1.0\columnwidth]{results/fig6}
  \caption{Deoptless speedup on mis-speculation (log scale)}
  \label{fig:deoptless-speedup}
  \medskip
\end{figure}

\paragraph{Memory Usage}

Deoptless causes more code to be compiled, which can lead
to more memory being used. The R language
is memory hungry
due to its value semantics and running more optimized code leads to fewer allocations.
Thus we expect deoptless to not use more memory overall. In this
worst-case experiment with randomly failing assumptions we measured a median
decrease of \memMed\% in the maximum resident set size. There is one outlier increase
in flexclust by \memMax\% and several decreases, the largest being \memMin\% in
fannkuchredux. The trade-off could be different for other languages or implementations.
However, the overhead can always be limited by the maximum number of deoptless
continuations.

\bigskip
\noindent
In the following we report the effects of deoptless on a selection of
benchmarks with known deoptimization events.

\paragraph{Volcano}

Deoptimizations can happen when user interaction leads to events which are not
predictable.
To demonstrate the effect we package a ray-tracing
implementation~\citep{url-throwing-shade} as a shiny
app~\citep{package-shiny}. It allows the user to
select properties, such as the sun's position, selecting the functions for
numerical computations and so on. The app renders a picture using
ggplot2~\citep{package-ggplot2} and the aforementioned
ray-tracer with a height-map of a volcano.
At the core of the computation is a loop nest which incrementally updates the
pixels in the image, by computing the angle at which rays intersect the terrain.
We record two identical sessions of a user clicking
on different features in the app. We then measure for each interaction how long the
application takes to compute and render the picture. In \autoref{fig:volcano-comparison} we
show the relative speedup of deoptless for that interactive session, separate
for the ray-tracing and the rendering step. The application exhibits
deoptimization events when the user chooses a different
numerical interpolation function. Deoptless results
in up to 2$\times$ faster computations for these particular iterations. In
general deoptless is always computes faster, except for one warmup iteration with a
longer compile pause. The produced image is then rendered by ggplot where we see
deoptless' ability to prevent over-generalization. The
code consistently runs about 2.5$\times$ faster after warmup than without
deoptless.

\begin{figure}
  \centering
  \includegraphics[width=0.55\columnwidth]{experiments/shiny/plot.pdf}
  \caption{Volcano app speedup (log scale)}
  \label{fig:volcano-comparison}
\end{figure}

\paragraph{Versus Profile-Driven Reoptimization}

Finally, we compare the performance profile of deoptless with a
profile-driven reoptimization strategy for \R \citep{dls20}.
The corresponding paper contributes three benchmarks which exhibit problematic cases for
dynamically optimizing compilers. First, a
microbenchmark for stale type-feedback. Then, an RSA implementation, where a key parameter
changes its type, triggering a deoptimization and a subsequent more generic
reoptimization. Finally, a benchmark where a function is shared by multiple
callers and thus merges unrelated type-feedback.
For the three benchmarks they report on speedups of up to 1.2$\times$,
1.4$\times$, and 1.5$\times$ respectively. For deoptless, we expect to
improve only on RSA. In the other two cases the phase change is not accompanied by a
deoptimization, therefore there is no chance for deoptless to improve
performance. We ran these benchmarks against our deoptless implementation
with 3 invocations and 30 iterations;
\autoref{fig:profiler1} presents the results. Each dot
represents the relative speedup of deoptless, for
one iteration of the benchmark each. As expected, the microbenchmark
and the shared function benchmark are unchanged.
The RSA benchmark is sped up by the same amount as in the best case of profile-driven
recompilation.

\begin{figure}
  \centering
  \includegraphics[height=1.3in]{results/fig11}
  \caption{Speedup on reoptimization benchmarks}
  \label{fig:profiler1}
\end{figure}
