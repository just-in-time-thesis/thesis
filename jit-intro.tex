\section{On-Stack Replacement}\label{sec:background-osr}

Before going into details about how to correctly create and use the \assume
instruction in the compiler IR, this section takes a step back and presents the underlying
implementation techniques and identifies the different pieces in a real-world
deoptimization.
This includes a preview of how to eventually lower an
\assume instruction to something a CPU can execute.
In general the relevant implementation technique is known as on-stack
replacement. OSR refers to an exceptional transfer of control
between two versions of a function. It is employed by just-in-time compilers in situations
where a function can or has to be replaced at once, without waiting for it
to exit normally. To the user, this exchange is not observable, the new function
transparently picks up where the old one stopped. \emph{On-stack} refers to
the fact that the involved functions have active stack frames that need to be
rewritten.
OSR is an umbrella term used in literature and practice to describe exceptional
transfer of control for different reasons and using different kind of mappings
between stack frames or program states.
The term deoptimization and on-stack replacement are often used
interchangeably.
Although their meanings overlap, we should be more precise in their use.%
\footnote{Adding to the confusion some authors use OSR
exclusively to refer to tiering-up, a convention we do not want to follow in this
work.}
The term deoptimization highlights the fact, that optimizations are being
undone. A deoptimization transfers control from a speculatively optimized
version with a failing assumption, to a less speculatively optimized version,
undoing the failing assumption. On the other hand, the term OSR focuses on the implementation technique,
whereby stack-frames are rewritten. OSR can be used for other applications, such
as implementing exceptions, or tiering-up, \ie changing from a less to a more
optimized version. Similarly, deoptimization can be implemented without OSR,
for instance by relying on a execution environment with support for stack-switching, or simply
using tail-calls and lazy replacement.
The latter strategy is employed by \R.

As shown in the previous section,
speculative optimizations and in
particular deoptimization, needs a way of exiting and entering functions in the
middle of execution, with low performance impact on the case where the guards
hold. When the guard fails, then instead of continuing normally, the function is
exceptionally terminated and replaced with the unoptimized baseline function,
as visualized in \autoref{fig:jit-intro-deopt}.
A correct deoptimization requires the system to extract the state of the optimized
function, transform it into a corresponding unoptimized state and then
materialize it to continue the execution.
% This dissertation will put the emphasis on the question of how to
% correctly map these states, which is in my opinion the difficult part of the
% problem. There is however also the practically challenging one, of how
% to extract and materialize the states, \ie how to lower the models to actual
% hardware. I will briefly discuss the terminology here, but for an in depth
% exploration refer to
% \autoref{chpt:implementation}, where an example implementation for \R is described.

% We call the currently active function, that should be exited, the \emph{origin}
% of an OSR. The replacement function, that should be entered, is the
% \emph{target}. Origin and target might not have compatible execution states,
% they might even rely on a different \emph{execution engine}, for example when one is
% native code executed by the underlying hardware and the other is bytecode evaluated
% %% don't put letters at the end of the text but at the beginning
% by an interpreter. \autoref{fig:osr} shows (a) the execution state being extracted from the
% %% don't introduce terms of on the fly. A definition must define. Then use.
% origin, then (b and c) \emph{mapped} to a matching state of the target
% function (b and c), and finally written back to the target execution engine (d).

\paragraph{Definitions}

We call functions that should be exited \emph{origins} and their
replacements \emph{targets}. Each function has an \emph{execution state}, or
\emph{stack frame}, that is dependent
on the code format but typically consists of at least the position in the code
and the values of local variables. The format of origin and target can be vastly
different if, for instance, one of them is interpreted and the other runs natively.
A \emph{mapping} between states captures the steps needed to rewrite origin
states to target states. Since both origin and target are derived from the same source code,
we sometimes use the term \emph{source} to refer to the common ancestry of various
compiled code fragments. \autoref{fig:osr} shows an idealized OSR that (a) extracts
the state of the origin, (b) maps it to the source, (c) maps it
to the target, and finally (d) materializes the target state.
%
\begin{figure}
  \centering
  \includegraphics[width=\columnwidth]{deoptless/figures/osr.png}
  \caption{Parts of an OSR event}
  \label{fig:osr}
\end{figure}
%
Origin and target do not need to be constrained to a single stack frame and a
single function. For example when exiting an inlined function, one origin
function maps to multiple target functions. In other words, the stack frame of the origin needs to be split into
multiple target stack frames.

OSR has been described as black magic due to the
non-conven\-tional control-flow that it introduces. A significant part
of the complexity comes from the fact that most implementations do
not provide clean abstractions for OSR.  For example, extracting and
rewriting the program state, \ie steps (a) and (b) in \autoref{fig:osr}, are
often not separated cleanly.
Both of these two steps provide challenges, but for different
reasons. Extracting the program state is challenging due to low-level concerns.
We need very fine grained access to the internal state of the
computation at the OSR points. This access has to be provided by the
backend of our compiler, \eg by exposing how the execution state is mapped to
the hardware or the interpreter. On the other hand, mapping the extracted program 
state to a target state, \ie creating a correct \emph{varmap}, relies on the optimizer providing the required
information.

\paragraph{Simplifications}

In practice, many implementations follow a simplified design combining (b) and
(c) into one mapping that translates directly from one state to another. This 
works particularly well, if the compiler of one end of the OSR uses the
code of the other end of the OSR as source code. For instance a typical
implementation uses the bytecode of the first tier interpreter as the source
code of the optimizing native compiler:
\begin{align*}
  \textsf{source} \rightarrow &\textsf{BC} \\
  &\textsf{BC} \rightarrow \textsf{native}
\end{align*}
In this architecture, there is only one compiler and one compilation direction
between the two ends of the OSR. In other words, the origin code is the source
code of this compiler, therefore the mapping takes just one step, from
native to bytecode (BC).
On the other hand, in the case where both ends of the OSR are compiled from some
common source code, the mapping of execution states has two steps, as it needs
to pass through the original source. Given the following two compilation tiers:
\begin{align*}
  \textsf{source} &\rightarrow \textsf{BC} \\
  \textsf{source} &\rightarrow \textsf{native}
\end{align*}
the
second compilation mandates a mapping that lifts the state from an origin
(native) state to a source state, the first compilation a mapping that lowers it to a target
(BC) state. Therefore, the generic model is important in cases where
OSR transitions from optimized to optimized code.

In \sourir, the origin and target state of a deoptimization have the same
representation.
In other words the \emph{varmap} corresponds to (c), the mapping (b) is the identity, since
source and origin are identical. Furthermore (a) and (d) are trivial, since
our execution states are semantic states of the \sourir IR.
This simplification was made to focus on the main problem
of creating and maintaining a correct mapping for deoptimization.

\paragraph{Directions}

If OSR jumps from optimized to unoptimized code, we call it \emph{OSR-out}, or
\emph{deoptimization}, when it is used to bail out of failing speculative optimizations.
As a simple example, if the user debugs optimized code with constant-folding
applied to, then deoptimization can be used to restore the constant values.
If OSR jumps from unoptimized to optimized code, we call
it \emph{OSR-in} or \emph{tiering up}. This is useful, for instance, when the program is stuck
in a long-running loop. In the general case where it
jumps from optimized to optimized code, both apply and we simply
call it OSR. Typically OSR cannot happen at arbitrary locations; we call the
possible locations \emph{OSR exit} or \emph{OSR entry} points.
OSR is general as it allows to undo arbitrary transformations. When
OSR is used to transition between different optimization levels, it
must be transparent, \ie deoptimization becomes part of the correctness argument
for optimizations. In turn, deoptimization enables compiler transformations that
would otherwise be unsound.

%% \paragraph{OSR for Deoptimization}
%% Consider, as a simple example, using OSR to undo constant-folding to
%% support debugging. When the debugger is attached to the program,
%% execution is paused and the program counter is moved from the current
%% optimized function to the equivalent location in a version of the same function
%% without the constant-folding applied. Any variables removed by
%% constant-folding are recreated from metadata.
%% OSR is general as it allows to undo arbitrary transformations. When
%% OSR is used to transition between different optimization levels, it
%% must be transparent, \ie deoptimization becomes part of the correctness argument
%% for optimizations. In turn, deoptimization enables compiler transformations that
%% would otherwise be unsound.
%% It allows the compiler to
%% speculate on \emph{likely} behaviors of the program, such as the dynamic type of
%% a variable. The speculation has to be guarded by a test and then the compiler can
%% rely on deoptimization, in case the speculation fails, to go back to the unoptimized
%% function. In other words, the
%% unlikely cases can be completely ignored by the optimizer and relegated to this
%% generic fall-back.
%% 
%% From the optimizer's point of view, the challenge presented by deoptimization is
%% that the mapping information must be preserved during transformations.
%% Every transformation that is applied has to amass enough
%% meta-data for the state mapping to be well defined at every OSR point.
%% One approach to keeping the mapping valid is to represent it by
%% metadata or pseudo instructions inside the instruction
%% stream. In \R so-called \c{Framestate} instructions are
%% inserted in the early translation phase.  These instructions capture the values on the
%% operand stack, local variables, and the program counter, \ie a
%% description of the execution state needed for the target.
%% While optimizing, the compiler keeps the frame states updated.

\paragraph{Implementation Choices for OSR-out}

The lowest overhead to peak performance for OSR exit points can be achieved by
extracting the
execution state by an external mechanism. Typically, at a defined location
execution is conditionally stopped and control transferred to an OSR-out implementation, \eg
by tail-calling it. The OSR-out implementation needs metadata produced by
the compiler that describes how the execution state can be retrieved
from the registers and native run-time stack.
This is only possible with fine-grained control over the code layout produced by
the native backend.
For efficiency, certain implementations will go to great lengths to implement
the conditional guard of \assume instructions such that it has the lowest overhead on the fast-case. This can go as far
as compiling it to a \code{nop}, that will be patched inline, in case some
external condition is invalidated.
A simpler alternative implementation is to pass all the required state as
arguments to a dedicated OSR-out primitive function. This approach generates more code,
as the state extraction is effectively embedded into the emitted code but is
easy to implement and efficient in case deoptimization triggers.

\paragraph{Simplified OSR-in}

Whereas OSR-out relies on the ability to extract the source
execution state at many locations, OSR-in is simpler.
While one could arrange for OSR-in to enter optimized code in the
middle of a function, these entry points would limit optimizations and
would not be easy to implement, in particular if using an off-the-shelf code
generator such as LLVM. Instead, one can compile a
continuation starting from the current program location to the end of
the current function.  This continuation is executed once and on the
next invocation the function is compiled a second time from the
beginning of the function. This approach simplifies the mapping of
execution states, as there is only one concrete state that needs to be
mapped instead of multiple abstract states at every potential entry
point. The current state is simply passed as an argument to the
continuation. This is a popular implementation choice.
