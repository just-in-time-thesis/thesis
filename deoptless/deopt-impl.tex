%% In this section we detail our efforts to introduce deoptless to \R, an
%% optimizing just-in-time compiler for the R language. \R
%% features a two-and-half-tier optimization strategy, with the two traditional
%% tiers, a bytecode interpreter and a native optimizing compiler. Additionally, 
%% it falls back to the AST interpreter from GNU R, as R is
%% a language with a fairly large number of features, some of which are not yet supported.
%% 
\R has an OSR-out
implementation to transition from native code to the interpreter in case of
mis-speculation. It is realized by lowering the deoptimization points from the
previous section to LLVM. We use the same approach for all different kinds of
speculations, be they on the stability of call targets, the declared local variables of closures,
uncommon branches, primitive types, and loops over integer sequences.

\R also features an OSR-in implementation, a direct side-product of our work to
implement deoptless. It can be used to tier-up from the interpreter to optimized code,
and is triggered in long-running loops. Supporting
OSR-in adds little complexity to the compiler. The relevant patch adds 300 and changes
600 lines of code. Mainly, the bytecode to IR translation has to support starting at an
offset, and the current values on the interpreter's operand stack need to be passed into the
optimized continuation.

%% Deoptless can be easily implemented on top of an existing implementation
%% of OSR-out and -in. The patch adds 600 and changes 300 lines of code. Compilation
%% is straightforward using the OSR-in implementation. The additional complexity
%% stems from defining optimization contexts suited for deoptless, and then
%% context dispatching over these contexts. Finally, a new type-feedback inference
%% and cleanup pass is required. In the original case, 
%% the interpreter collects new run-time feedback after a deoptimization and before code is reoptimized. 
%% With deoptless, we try to recompile right after a failing assumption, not having a chance to
%% capture later, secondary changes to the program state that we need to update our assumptions about the code.
%% The feedback inference pass tries to remove profile data which is likely
%% invalid after the failing assumption, and infer new feedback from the remaining data.
%% 
%% Before describing our deoptless implementation in detail, we
%% present the implementation of OSR-out and -in.

\subsubsection{OSR-out}

In \R, OSR exits are not performed by externally rewriting stack frames. Instead, an OSR exit point is
realized as a function call. Let us consider the OSR exit point in
\autoref{lst:pir-osr-exit}. The backend of the \R compiler generates code using LLVM.
As can be seen in \autoref{lst:llvm-osr-exit}, the \c{Assume} is lowered to a conditional branch 
and the OSR exit is lowered to a tail-call.
%
\begin{figure}[h]\begin{lstlisting}[style=llvm,label={lst:llvm-osr-exit},caption={OSR exit from \autoref{lst:pir-osr-exit} in LLVM}]
       br %isType, cont, osr
osr:
       ...
  %f = alloca FrameState
  %r = alloca Reason
     ; store current function,
     ; frame contents, and more
     ; metadata into %f and %r
  %a = tail call Value @deopt(%f, %r)
       ret %a
cont:
\end{lstlisting}\vskip -0.8em\end{figure}
%
\noindent The \c{osr} block executes all the deferred instructions,
notably it populates buffers for the local variables captured by
the framestate and the deoptimization reason.
Finally, a \c{deopt} function is called. This primitive performs the
actual deoptimization, \ie it
invokes the interpreter, or, in the case of deoptless, dispatches to an
optimized continuation.

The \c{deopt} primitive is able to recreate multiple interpreter contexts as we
can see in the pseudocode in \autoref{lst:deopt-impl}.
%
\begin{figure}\begin{lstlisting}[style=C,label={lst:deopt-impl},caption={Pseudocode for deoptimization implementation}]
Value deopt(FrameState* fs, Reason* r) {
  logDeoptimization(r);
  pushInterpreterState(fs);
  if (fs->next)
    push(deopt(fs->next, r));
  return interpret(fs->pc, fs->env);
}
\end{lstlisting}\vskip -0.8em\end{figure}
%
First, the outer interpreter context is synthesized, \ie the necessary values 
pushed to the interpreter's operand stack. Then, the inner frames are recursively 
evaluated, their results also pushed to the operand stack, as expected by the outer
frame. Finally, the outermost code is executed, and the result returned to the
deoptimized native code, which directly returns it to its caller.

The \c{osr} basic block in \autoref{lst:pir-osr-exit}, as well as the \c{deopt}
call, are marked
\emph{cold} in LLVM. This causes LLVM optimization passes and code
generation to layout the function in such a way that the impact of the \c{osr}
code on the performance of the rest of the function is minimal.
However, the mere presence of the additional branch might interfere with LLVM
optimizations, and other OSR implementers therefore chose to use the LLVM \c{statepoint} primitive.
The statepoint API provides access to metadata describing the stack layout of the
generated function. This stack layout allows an external deoptimization
mechanism to read out the local state without explicitly capturing it in LLVM code.
This is a trade-off, and the impact is in our opinion limited. For example, in
the concrete case of \autoref{lst:vector-sum}, we were
not able to measure any effect on peak performance. In fact, when we unsoundly dropped all deoptimization
exit points in the backend, the performance was unchanged. There was, however, an
effect on code size with an overhead of 30\% more LLVM instructions.
The implementation strategy of using explicit calls to \c{deopt} for \R was chosen for ease of implementation long
before deoptless was added. In a lucky coincidence, this strategy is very
efficient in extracting the internal state of optimized code compared to an
external deoptimization mechanism, and therefore very well suited for
deoptless.

\subsubsection{OSR-in}

OSR-in allows for a transition from
long-running loops in the bytecode interpreter to native code. To that end, a
special continuation function is compiled, starting from 
the current bytecode, which is used only once for the OSR-in. The
full function is compiled again from the beginning the next time it is called.
This avoids the overhead of introducing multiple entry-points
into optimized code, for the price of compiling these functions twice. Since
OSR-in is not a very frequent event, the trade-off is reasonable. 

The mechanism is triggered by counting the number of backward jumps in the interpreter.
When a certain number of loop iterations is reached, the remainder of the function is 
compiled using the same compiler infrastructure that is used to compile whole functions.
The only difference is that we choose the current program counter value as an entry point for
the translation from bytecode to IR. Additionally, we pre-seed the abstract stack used by the frontend of
the \R compiler with all values on the interpreter's operand stack.
In other words, the resulting native code will receive the current contents of 
the operand stack as call arguments. OSR adds the
lines, shown in \autoref{lst:osr-in-impl}, to the implementation of the branch bytecode.

\begin{figure}
\begin{lstlisting}[style=C,label={lst:osr-in-impl},caption={Pseudocode for OSR-in implementation}]
case Opcode::branch: {
  auto offset = readImmediate();
  if (offset < 0 && OSRCondition()) {
    if (auto fun = OSRCompile(pc, ...)) {
      auto res = fun(...);
      clearStack();
      return res;
    }
  }
  ...
}
\end{lstlisting}
\end{figure}

An interesting anecdote from adding OSR-in to \R is that out of all the
optimization passes of the normal optimizer, only dead-store elimination was
unsound for OSR-in continuations. The reason is that objects can already
escape before the OSR continuation begins, and thus escape analysis would
mistakenly mark them as local.


