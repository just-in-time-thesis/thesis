source(file="../experiments-common.r") 

require(ggplot2)
require(ggrepel)


d = getDataForExperimentLargeDataframe()  


# dfBaseline = read.csv("data/baseline-1778099985.csv", na.strings=" ")
# dfDeoptless = read.csv("data/deoptless-1778152143.csv", na.strings=" ")
# d = rbind(dfBaseline, dfDeoptless)

scaleFUN <- function(x) sprintf("%.2f", x)

d$iteration = d$run
d$time = d$s
d$benchmark = "dataframe"
d$experiment = factor(d$experiment, c("normal", "deoptless"))
#d = normalize(d, T, "normal", T)
#print(d[2,]$event)
p = ggplot(d, aes(iteration,time)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~experiment, ncol=1, strip.position="left") +
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
  scale_y_continuous(trans='log10', labels=scaleFUN) +
  geom_text_repel(aes(label = event,
                       color = experiment
                       ),
                       size = 6,
                       seed = 3,
                       nudge_y=0.4,
                       nudge_x=1,
                       show.legend=F) +
 # geom_vline(xintercept=runs+0.5, linetype="dashed", color="gray", size=0.1) +
#  geom_vline(xintercept=5.5, color="gray")+
#  geom_vline(xintercept=10.5, color="gray")+
#  geom_line(aes(color=experiment), size=1.2) +
    geom_point(size=2.5, aes(color=experiment), alpha=0.1)
  #+ stat_summary(aes(color=experiment), geom="point", size=3, alpha=0.6)

ggsave(p, filename = "plot.pdf", height=4, device = cairo_pdf)
