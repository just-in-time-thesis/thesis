source(file="../experiments-common.r") 

require(ggplot2)
require(ggrepel)


fetch <- function(arg, sourceFile, readFromFileFunction) {
  name = arg[[1]]
  job = arg[[2]]
  sourceRepo = arg[["sourceRepo"]]
  
  dir.create("data", showWarnings=F)
  setwd("data")
  url <- paste0(sourceRepo, "/-/jobs/", job, "/artifacts/raw/", sourceFile, "?inline=false")
  file <- paste0(sourceFile, job, ".csv")
  if (!file.exists(file))
    download.file(url, file)
  res <- readFromFileFunction(file)
  #res$experiment = name
  setwd("..")
  res
}


myFetch <- function(kind) {
  function(exp) {
    readFromFileFunction <- function(file) {
        res = read.csv(file, na.strings=" ", header=F, col.names=c("iteration","time"))
        res$experiment = exp[[1]]
        res$benchmark = kind
        res
    }
    fetch(exp, paste0("volcano-",kind,".csv"), readFromFileFunction)
  }
}

dataS =  lapply(experimentVolcano, myFetch("simplified"))
dataS = rbind(dataS[[1]], dataS[[2]])
dataF =  lapply(experimentVolcano, myFetch("fun"))
dataF = rbind(dataF[[1]], dataF[[2]])
dataT =  lapply(experimentVolcano, myFetch("type"))
dataT = rbind(dataT[[1]], dataT[[2]])

d = rbind(dataS, dataF, dataT)
d$time = as.numeric(d$time)

print(d)

scaleFUN <- function(x) sprintf("%.1f", x)

d = normalize(d, T, "baseline", T)
d$benchmark = factor(d$benchmark, c("simplified", "type", "fun"))
d$iteration = d$iteration-1

p = ggplot(d, aes(iteration,speedup)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~benchmark, ncol=1, strip.position="left") +
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
    geom_hline(yintercept=1)+
  scale_y_continuous(trans='log10', labels=scaleFUN) +
  scale_x_continuous(breaks=c(0,5)) +
    geom_point(size=2.5, aes(color=benchmark), alpha=0.6)
  #+
  #stat_summary(aes(color=benchmark), geom="point", size=3, alpha=0.6)

ggsave(p, filename = "plot.pdf", height=4, width=5, device = cairo_pdf)
