DEOPTLESS_REPO = "https://gitlab.com/rirvm/deoptless-benchmarks" 
RIR_REPO = "https://gitlab.com/rirvm/rir_mirror"

# ------ LARGE DATAFRAME -----------------------------------------------

# experimentLargeDataframe <- list(
#   list("baseline", 1791622151, sourceRepo=DEOPTLESS_REPO),
#   list("deoptless", 1791622153,sourceRepo=DEOPTLESS_REPO)
# )



experimentLargeDataframe <- list(
  list("baseline", 1799737152, sourceRepo=DEOPTLESS_REPO),
  list("deoptless",1799737155 ,sourceRepo=DEOPTLESS_REPO)
)



experimentVolcano <- list(
  list("baseline", 1799737163, sourceRepo=DEOPTLESS_REPO),
  list("deoptless",1799737167 ,sourceRepo=DEOPTLESS_REPO)
)






getDataForExperimentLargeDataframe = function() {
    
    myFetch <- function(exp) {
        readFromFileFunction <- function(file) { 
            read.csv(file, na.strings=" ")
        }
        
        fetch(exp, "largedataset-data.csv", readFromFileFunction)
    }
    
    data =  lapply(experimentLargeDataframe, myFetch)
    rbind(data[[1]], data[[2]])
    
}

# --------------BASELINE--------------------------------------



# experimentBaseline <- list(
#   list("baseline", 1791622142 , sourceRepo=DEOPTLESS_REPO),
#   list("deoptless",1791622146 , sourceRepo=DEOPTLESS_REPO)
# )


experimentBaseline <- list(
  list("baseline", 1799737145, sourceRepo=DEOPTLESS_REPO),
  list("deoptless",1799737149,sourceRepo=DEOPTLESS_REPO)
)





getDataForExperimentBaseline = function() {
    
    data =  lapply(experimentBaseline, fetchBenchmarkSuiteExperiment)
    data = rbind(data[[1]], data[[2]])
    data
    
}


bmselection <- c(
  "Bounce_nonames",
  "Mandelbrot",
  "Storage",
  "convolution",
  "flexclust",
  "binarytrees",
  "fannkuchredux",
  "fasta_naive_2",
  "fastaredux",
  "knucleotide",
  "nbody",
  "nbody_naive",
  "pidigits",
  "regexdna",
  "reversecomplement_naive",
  "spectralnorm_math"
  )


getDataForExperimentBaselineSelection = function() {
      
  data = getDataForExperimentBaseline()

  data = data[data$benchmark %in% bmselection,]
  data

}


#--------- DLS PROFILER -------------

getDataForExperimentProfilerDLS = function() {
    
  data = getDataForExperimentBaseline()
  data = data[grep("profiler", data$benchmark),]
  data


}



# ------------------------------------ COMMON  ---------------------------------

fetchBenchmarkSuiteExperiment <- function(exp) {

    readFromFileFunction <- function(file) { 
      res <- read.table(file, header=F, strip.white=TRUE, col.names=c("execution", "iteration", "time", "time_unit", "ua", "benchmark", "vm", "suite", "num_iterations", "ub"))
      res$experiment = exp[[1]]
      res
    }
    
    data = fetch(exp, "benchmarks.data", readFromFileFunction)
    #data = data[data$iteration > warmup, ]
    data

}

fetch <- function(arg, sourceFile, readFromFileFunction) {
  name = arg[[1]]
  job = arg[[2]]
  sourceRepo = arg[["sourceRepo"]]

  dir.create("data", showWarnings=F)
  setwd("data")
  url <- paste0(sourceRepo, "/-/jobs/", job, "/artifacts/raw/", sourceFile, "?inline=false")
  file <- paste0(job, ".data")
  if (!file.exists(file))
    download.file(url, file)
  res <- readFromFileFunction(file)
  #res$experiment = name
  setwd("..")
  res
}

normalize <- function(data, removeNormalized, baseline, perIteration) {
  data$speedup = 1
  for (b in unique(data$benchmark)) {
    if (missing(baseline))
      e1 = unique(data$experiment)[1]
    else
      e1 = baseline
    m = mean(data[data$benchmark == b & data$experiment == e1, ]$time)
    data[data$benchmark == b, ]$speedup <-
      (if (perIteration) data[data$benchmark == b & data$experiment == e1, ]$time else m) / data[data$benchmark == b, ]$time
    if (removeNormalized)
      data = data[-which(data$benchmark == b & data$experiment == e1), ]
  }
  data
}
