source(file="../experiments-common.r") 

require(ggplot2)

# exp <- list(
#   list("baseline", 1788243949, sourceRepo=RIR_REPO),
#   list("deoptless",1789042727, sourceRepo=RIR_REPO)
# )


exp <- list(
  list("baseline", 1799737157, sourceRepo=DEOPTLESS_REPO),
  list("deoptless",1799737160, sourceRepo=DEOPTLESS_REPO)
)





# fetch <- function(arg) {
#  name = arg[[1]]
#  job = arg[[2]]
#  dir.create("data", showWarnings=F)
#  setwd("data")
#  url <- paste0("https://gitlab.com/rirvm/rir_mirror/-/jobs/", job, "/artifacts/raw/benchmarks.data?inline=false")
#  file <- paste0(job, ".data")
#  if (!file.exists(file))
#    download.file(url, file)
#  res <- read.table(file, header=F, strip.white=TRUE, col.names=c("execution", "iteration", "time", "time_unit", "ua", "benchmark", "vm", "suite", "num_iterations", "ub"))
#  res$experiment = name
#  setwd("..")
#  res
# }

warmup=5
data = lapply(exp, fetchBenchmarkSuiteExperiment)
data = rbind(data[[1]], data[[2]])
data = data[which(data$iteration > warmup), ]
data = data[data$benchmark %in% bmselection,]
#data = data[which(data$benchmark == "Bounce"),]

#print(data[which(data$benchmark == "spectralnorm"),])
data = normalize(data, T, "baseline", F)
data = aggregate(list(speedup=data$speedup), by=list(benchmark=data$benchmark, iteration=data$iteration ), FUN=mean)
#print(data[which(data$benchmark == "spectralnorm"),])

position_var_nudge <- function(l_off=0, r_off=0) {
  ggproto(NULL, PositionVarNudge, l_off=l_off, r_off=r_off)
}

PositionVarNudge <- ggproto("PositionVarNudge", Position,
  l_off = 0,
  r_off = 0,

  setup_params = function(self, data) {
    list(l_off = self$l_off, r_off = self$r_off)
  },

  compute_layer = function(self, data, params, layout) {
    nudge = data
    nudge$ord = 1:nrow(nudge)
    nudge$nudge = 0

    for (p in unique(data$PANEL)) {
      for (e in unique(data$x)) {
        n = nrow(nudge[nudge$x == e & nudge$PANEL == p,])
        if (n == 0) next()
        n_tot = n + params$l_off + params$r_off
        steps <- (0.9:(n_tot))/n_tot
        width <- 0.6
        steps <- steps * width - (width/2)
        d2 = nudge[nudge$x == e & nudge$PANEL == p,]
        d2 = d2[order(d2$iteration),]
        d2$nudge = -steps[(params$r_off+1):(n_tot-params$l_off)]
        for (d in d2$ord)
          nudge[nudge$ord == d,]$nudge = d2[d2$ord == d,]$nudge
       }
    }
    nudge = nudge$nudge
    f <- function(x) {
      x - nudge
    }
    transform_position(data, f, NULL)
  }
)

cairo_pdf("plot.pdf")
print(ggplot(data, aes(benchmark, speedup, color=benchmark)) +
    theme_bw() +
 #   facet_wrap(~experiment, scales="fixed") +
    #scale_colour_brewer(palette = "Dark2")+
    stat_summary(fun = mean, geom = "point", size = 5, alpha=0.5) +
#    geom_point(position=position_var_nudge(),
#               size=1,
#               aes(iteration=iteration)
#               ) +
    scale_y_continuous(trans='log10', breaks=c(0.1,1,1.6,2,4,10,20,50)) +
#    ylim(0.8,1.2)+

    geom_point(position=position_var_nudge(),
               size=0.5,
               aes(iteration=iteration)
               ) +

    geom_hline(yintercept=1)+
    theme(axis.title.x=element_blank(),
          axis.text.x=element_text(angle = 90, vjust = 0.5, hjust=1),
          #axis.text.x=element_blank(),
          #axis.title.y=element_blank(),
          axis.ticks=element_blank(),
          panel.border = element_blank(),
          legend.position="none") #+
    #geom_boxplot(outlier.shape=NA)
)

dev.off()
