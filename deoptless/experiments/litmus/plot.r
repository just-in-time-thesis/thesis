require(ggplot2)
require(ggrepel)
d = read.csv("results.csv", na.strings=" ")

scaleFUN <- function(x) sprintf("%.1f", x)

d$iteration = d$run
d$seconds = d$s
d$experiment = factor(d$experiment, c("normal", "deoptless"))
print(d[2,]$event)
p = ggplot(d, aes(iteration,seconds)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~experiment, ncol=1, strip.position="left") +
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
  scale_y_continuous(trans='log10', labels=scaleFUN) +
  geom_text_repel(aes(label = event,
                       color = experiment
                       ),
                       size = 6,
                       seed = 3,
                       nudge_y=0.4,
                       nudge_x=1,
                       show.legend=F) +
 # geom_vline(xintercept=runs+0.5, linetype="dashed", color="gray", size=0.1) +
#  geom_vline(xintercept=5.5, color="gray")+
#  geom_vline(xintercept=10.5, color="gray")+
#  geom_line(aes(color=experiment), size=1.2) +
  stat_summary(aes(color=experiment), geom="point", size=4)

ggsave(p, filename = "litmus.pdf", height=4.5, device = cairo_pdf)
