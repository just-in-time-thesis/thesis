f = function() {
  s = 0
  for (i in 1:length(q))
    s = s + q[[i]]
  s
}

type=commandArgs(trailing=T)[1]
size = 10000000
q = runif(size)
#cat("experiment, event, run,i s \n")
for (i in 1:5) cat(paste(type, "", "float", i-1, system.time(f())[[1]], sep=","), "\n")
q = as.integer(runif(size))
for (i in 1:5) cat(paste(type, "", "int", 4+i, system.time(f())[[1]], sep=","), "\n")
q = complex(real=runif(size), imaginary=runif(size))
for (i in 1:5) cat(paste(type, "", "complex", 9+i, system.time(f())[[1]], sep=","), "\n")
q = runif(size)
for (i in 1:5) cat(paste(type, "", "float", 14+i, system.time(f())[[1]], sep=","), "\n")

