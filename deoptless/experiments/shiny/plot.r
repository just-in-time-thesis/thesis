source(file="../experiments-common.r")

require(ggplot2)
require(ggrepel)

read = function(kind) {
    setwd("data")
    file = paste0("shiny_", kind, ".csv")
    res = read.csv(file, na.strings=" ", header=F, col.names=c("iteration","args","time","render"))
    res$experiment = kind
    res$benchmark = "shiny"
    setwd("..")
    res
}

baseline = read("baseline")
baselineC = baseline[c("iteration", "args" , "time", "experiment")]
baselineC$benchmark = "cast_rays"
baselineG = baseline[c("iteration", "args" , "render", "experiment")]
names(baselineG)[names(baselineG) == "render"] <- "time"
baselineG$benchmark = "ggplot"

deoptless <- read("deoptless")
deoptlessC = deoptless[c("iteration", "args" , "time", "experiment")]
deoptlessC$benchmark = "cast_rays"
deoptlessG = deoptless[c("iteration", "args" , "render", "experiment")]
names(deoptlessG)[names(deoptlessG) == "render"] <- "time"
deoptlessG$benchmark = "ggplot"

d = rbind(baselineC, baselineG, deoptlessC, deoptlessG)
d$time = as.numeric(d$time)

print(d)

scaleFUN <- function(x) sprintf("%.1f", x)

d = normalize(d, T, "baseline", T)
d$benchmark = factor(d$benchmark, c("cast_rays", "ggplot"))

p = ggplot(d, aes(iteration,speedup,fill=benchmark)) +
  theme_bw() +
  scale_colour_brewer(palette = "Dark2")+
  facet_wrap(~benchmark, ncol=1, strip.position="left", scales="free_y") +
  theme(
        #axis.title.x=element_blank(),
        #axis.text.x=element_blank(),
        #axis.title.y=element_blank(),
        #axis.text.y=element_blank(),
        axis.ticks=element_blank(),
        panel.grid.minor=element_blank(),
        panel.border = element_blank(),
        #panel.grid.major=element_blank(),
        legend.position = "none",
        #strip.background=element_blank(),
        strip.text=element_text(size=14),
        text=element_text(size=14)
        ) +
    geom_hline(yintercept=1)+
    scale_y_continuous(trans='log', labels=scaleFUN) +
    geom_point(size=2.5, aes(color=benchmark), alpha=0.6)
  #+
  #stat_summary(aes(color=benchmark), geom="point", size=3, alpha=0.6)

ggsave(p, filename = "plot.pdf", height=4, width=5, device = cairo_pdf)
