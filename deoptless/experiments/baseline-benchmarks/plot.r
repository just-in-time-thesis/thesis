source(file="../experiments-common.r") 

require(ggplot2)


data = getDataForExperimentBaselineSelection()


printResult <- function(name, result, digits=3, space=F) {
   name = gsub("μ", "mu", name)
   r = format(result, digits=digits)
   cat(paste0("\\newcommand{\\result",name,"}{",r))
   if (space)
     cat("\\xspace")
   cat("}\n")
}




position_var_nudge <- function(l_off=0, r_off=0) {
  ggproto(NULL, PositionVarNudge, l_off=l_off, r_off=r_off)
}

PositionVarNudge <- ggproto("PositionVarNudge", Position,
  l_off = 0,
  r_off = 0,

  setup_params = function(self, data) {
    list(l_off = self$l_off, r_off = self$r_off)
  },

  compute_layer = function(self, data, params, layout) {
    nudge = data
    nudge$ord = 1:nrow(nudge)
    nudge$nudge = 0

    for (p in unique(data$PANEL)) {
      for (e in unique(data$x)) {
        n = nrow(nudge[nudge$x == e & nudge$PANEL == p,])
        if (n == 0) next()
        n_tot = n + params$l_off + params$r_off
        steps <- (0.9:(n_tot))/n_tot
        width <- 0.6
        steps <- steps * width - (width/2)
        d2 = nudge[nudge$x == e & nudge$PANEL == p,]
        d2 = d2[order(d2$iteration),]
        d2$nudge = -steps[(params$r_off+1):(n_tot-params$l_off)]
        for (d in d2$ord)
          nudge[nudge$ord == d,]$nudge = d2[d2$ord == d,]$nudge
       }
    }
    nudge = nudge$nudge
    f <- function(x) {
      x - nudge
    }
    transform_position(data, f, NULL)
  }
)

data = normalize(data, T, "baseline", T)
data = aggregate(list(speedup=data$speedup), by=list(benchmark=data$benchmark, iteration=data$iteration ), FUN=mean)


#print(data[which(data$speedup < 0.8),])
# outlier = round(data[which(data$benchmark == "binarytrees"),]$speedup[[3]],2)

  ggplotInitial = ggplot(data, aes(benchmark, speedup, color=benchmark))

  yLowerLimit <-0.8
  yAnnotations <- yLowerLimit

 
  
  for (b in unique(data$benchmark)) {
    outliers <- sort(data[data$benchmark == b &  data$speedup < yLowerLimit,]$speedup, decreasing=T)
    if (length(outliers) > 0) {
      outliers <- paste(unlist(round(outliers,2)), collapse='\n')
      annotation = annotate(
       "text", x = b, y = yAnnotations, size=2, label = outliers) 

      ggplotInitial = ggplotInitial + annotation
    }
  }


cairo_pdf("plot.pdf")
print(ggplotInitial +
    theme_bw() +
   # facet_wrap(~benchmark, scales="fixed") +
    #scale_colour_brewer(palette = "Dark2")+
    stat_summary(fun = mean, geom = "point", size = 5, alpha=0.5) +
    geom_point(position=position_var_nudge(),
               size=1,
               aes(iteration=iteration)
               ) +
    #scale_y_continuous(trans='log') +
    #ylim(0.8,1.2)+
    ylim(yLowerLimit, 1.2)+


    # geom_segment(aes(x = 0.9, y = 0.81, xend = 0.9, yend = 0.8, color="binarytrees"),
    #               arrow = arrow(length = unit(0.2, "cm"))) +

    #geom_segment(data=data,aes(y=1.05*0.95,yend=1.05, xend=benchmark, arrow = arrow(length = unit(0.1,"cm")))) +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_text(angle = 90, vjust = 0.5, hjust=1),
          #axis.text.x=element_blank(),
          #axis.title.y=element_blank(),
          axis.ticks=element_blank(),
          panel.border = element_blank(),
          legend.position="none")# +
#    geom_boxplot(outlier.shape=NA)
)

dev.off()
