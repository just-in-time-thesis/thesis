%% %% Reworte to remove the use of "assumption" -- it is something that is not fully general and
%% %% it would be one extra bit of jargon
%% At the heart of many high-performance just-in-time compilation strategies lies the ability to
%% replace code while it is being executed. A typical two-tier architecture has an interpreter for
%% quick startup, and a compiler for peak performance. In this architecture, when an implementation
%% needs to tier-up as it is evaluating a long-running function, one should not need to wait for the
%% interpreter to complete, but rather switch immediately to a natively compiled version~\citep{hol94}.
%% Or, when speculative compilation is found to be wrong, execution must not continue and the current
%% code must be replaced with a correct version~\citep{hol92}. In yet another example, the compilation of
%% unlikely code paths can be deferred~\citep{cha91}.
%% 
%% %% Later we used the term stack frame, so I removed activation record.
%% The common theme is replacing code with currently active stack frames, hence the name
%% \emph{on-stack replacement} (OSR). This may refer to pieces of code that have the same format, \eg
%% replacing native code with native code at a different optimization level, or a completely different
%% format, \eg switching from native code to interpreted code.
%% %% * suspend-rewrite-resume execution
%% From a distance, OSR can be described as a mechanism for suspending execution of a function, rewriting
%% its state, and resuming execution in a different version of that function. What makes OSR challenging
%% is the low-level interaction with execution states and the need to establish a mapping between versions
%% of a function. For example, consider a function in which some variable was constant-folded away.
%% In order to transfer control to a version of the same function in which this optimization was not
%% applied, one must first establish a correspondence between program counters, then reconstruct the
%% value of the variable, and store it at the expected position among the function's locals.

An interesting combination of \cd with speculation is to avoid deoptimization.
%% Speculative optimizations with deoptimization
%% Speculative compilation is characterized by code compiled under assumptions.
%% Any of these assumptions may prove to be false at run-time, and thus the compiler inserts
%% guards which trigger OSR to avoid executing mis-compiled code.
A problem of speculative optimizations is that when speculations fail, the optimized code is discarded and eventually replaced with
a more generic version, which might be slower.
%% * profiling
To illustrate, consider a function that operates on a list of numbers. At
run-time, the system observes the type of the values stored in the list.
After a number of calls, if the compiler
determines that the list holds only integers, it will speculate that this will remain true
and generate code optimized for integer arithmetic.
If, at some later point, floating point numbers appear instead, a deoptimization will
be triggered.  As shown in \autoref{fig:compare-deopt}, deoptimization makes it
possible to swap the
optimized code in-place with the baseline version of the function. In subsequent calls to the
function the compiler refines its profiling information to indicate that the function
can operate on lists of integers and floating point numbers. Eventually, the function will
be recompiled to a new version that is slightly more general. That version will not
need to deoptimize for floating point values, but likely will not be as efficient as the
previously optimized one.

\begin{figure}
  \centering

  \includegraphics[width=0.8\columnwidth]{figures/deopt.png}
  \caption{Deoptimization: OSR-out, profile, recompile}
  \label{fig:compare-deopt}
\end{figure}

%% Problems (with deoptimization)
Speculative compilation can cause hard to predict performance pathologies. Failed
speculations lead to two kinds of issues.
%% 1. sudden slowdown
First, deoptimization causes execution to suddenly slow down as the new code being executed
does not benefit from the same level of optimization as before.
%% 2. deterioration
Second, to avoid repeated deoptimizations, the program eventually converges
to code that is more generic, \ie that can handle the common denominator of all
observed executions. From a user's point of view, the program speeds up again,
but it does not regain its previous performance.

%% Solution (deoptless)
\emph{Deoptless} is a strategy for avoiding
deoptimization to a slower tier. The idea is to handle failing assumptions
with an optimized-to-optimized transfer of control.
At each deoptimization point, the compiler maintains multiple optimized continuations,
each specialized under different assumptions. When OSR is triggered, 
a continuation that best fits the current state of execution is selected.
The function that triggered OSR is not retired with deoptless 
(as would occur in the normal case), rather it is retained in the hope that it can be used again.

\begin{figure}
  \includegraphics[width=0.8\columnwidth]{figures/deoptless.png}
  \caption{Deoptless: dispatched OSR to specialization}
  \label{fig:compare-deoptless}
\end{figure}

%% * No tier-down
\autoref{fig:compare-deoptless} illustrates what happens when speculation fails with deoptless.
Instead of going to the baseline, the compiler generates code for the continuation, and
execution continues there. This can result in orders-of-magnitude faster
recovery from failed speculation.
Furthermore, deoptless not only avoids tiering down, it also gives the compiler an
opportunity to generate code that is specific to the current execution context.
As we later demonstrate, this can significantly increase the peak performance of
generated code.
For instance, if an assumption fails, as above, because a list holds
floating point numbers rather than integers, then the continuation can be specialized to
handle floats. In subsequent executions, if the same OSR point is reached,
the continuation to invoke will be selected by using \cd.
If no previously compiled continuation matches the execution context, then a new one
will be compiled. Of course, the number of continuations is bounded, and when that
bound is reached deoptless will deoptimize.

\subsection{Example}

\emph{Deoptless} is a compilation strategy that explores the idea
of having a polymorphic OSR-out as a backup for failed speculation,
while retaining the version of the function that triggered deoptimization.
Consider the \c{sum} function of \autoref{lst:vector-sum}, which
\begin{figure}[h]
\begin{lstlisting}[label={lst:vector-sum},caption={Summing vectors}, style={R}]
sum <- function() {
  total <- 0
  for (i in 1:length) total <- total + data[[i]]
  total
}
\end{lstlisting}
\end{figure}
naively adds up all the elements in the \c{data} vector. Assume the function
is called in situations where the values of data change from float to
integer to complex numbers and back to float.
As a preview, we run this code in our implementation.
\autoref{fig:comparison} shows both normal executions and executions with deoptless.
We see the warmup time spent in the interpreter and compilation to faster native
code in the first phase with 5 iterations. Each of the following 3 phases (also
with 5 iterations each) correspond to a different type of \c{data} vector.
In the normal environment each change of the dynamic type results in
deoptimization, followed by slower execution. In deoptless, there is a slowdown
in the first iteration, as the continuation must be compiled, then code is fast again. 
Complex numbers are slow in both versions as their behavior is more involved. 
Finally, when the function
deals with floats again, deoptless is as fast as the first time, whereas the original version
is stuck with slow code. We show this example here to motivate the technique
and give an intuition for our goals and the expected gains. This graph
effectively illustrates many of the trade-offs with deoptless that we are aware of,
and we'll discuss it again in detail at the end of the section.

\begin{figure}
  \includegraphics[width=0.6\columnwidth]{experiments/litmus/litmus.pdf}
  \caption{Performance comparison (log scale)}
  \label{fig:comparison}
\end{figure}

\subsection{Approach}

Conceptually, deoptless performs OSR-out and OSR-in in one step, to achieve an
optimized-to-optimized and native-to-native handling of failing speculation.
\begin{figure}
  \includegraphics[width=0.7\columnwidth]{figures/deoptless_arch_2.png}
  \caption{Deoptless combines OSR-out with OSR-in}
  \label{fig:osr_deoptless}
\end{figure}
As can be seen in \autoref{fig:osr_deoptless} this is realized by following an OSR-out
immediately with an OSR-in.
By performing this transition directly, it is possible to never leave optimized code.
For deoptless, the OSR-in must be implemented by compiling an optimized
continuation, specifically for that particular OSR exit point. The key idea is
that we can compile multiple specialized continuations, depending on the failing
speculation --- and in general, depending on the current state of the
execution. The continuations are placed in a dispatch table to be reused in future
deoptimizations with compatible execution states.

We effectively turn deopt points into assumption-polymorphic
dispatch sites for optimized continuations.
If the same deoptimization
exit point is taken for different reasons, then, depending on the reason,
differently specialized continuations are invoked.
Going back to \autoref{lst:vector-sum}, the failing
assumption is a typecheck. Given earlier runs, the compiler speculates that
\c{data} is a vector of floats. This assumption allows us to access the vector
as efficiently as a native array. Additionally, based on that assumption, the
\c{total} variable is inferred to be a float scalar value and can be unboxed.
When the variable becomes an integer, this speculation fails. Normally we would
deoptimize the function and continue in the most generic version, \eg in the baseline
interpreter. Deoptless allows us to split out an
alternate universe where we speculate differently and jump to a continuation optimized for that case.
%% The graph at the bottom of \autoref{fig:comparison} shows in the second phase,
%% at iteration 5, this specialized continuation being compiled and in subsequent iterations
%% dispatched to. We see a slight overhead over the float case, but still
%% the performance is much better than in the normal case above, where the function was
%% eventually reoptimized to handle both floats and integers.

We keep all deoptless continuations of a function in a common dispatch table. At
a minimum the continuation we want to invoke has to be compiled for the same
target program location. But we can go further and use the current program
state, that we extracted from the origin function for OSR, to have many
specialized continuations for the same exit. In order to deduce which continuations are compatible
with the current program state we employ a context dispatching mechanism.
%% In this framework, code is optimized under a context of
%% assumptions. Such an optimization context $C$ is a predicate over the program state with an
%% efficiently computable partial order
%% $C_1 < C_2~\mathit{iff}C_1\Rightarrow C_2$.
%% A context is called current with respect to a state $S$ if $C(S)$ holds.
To choose a continuation, we take the current state at the OSR exit point, we
compute a current context $C$ for it, and then select a continuation compiled
for a context $C'$, such that $C < C'$. If there is no such continuation
available, or we find the available ones to be too generic given the current
context, we can choose to compile a new continuation and add it to the dispatch
table.

In our implementation we add an abstract description of the deoptimization reason,
such as "typecheck failed, actual type was an integer vector", to the context.
Our source states are expressed in terms of the state of the bytecode interpreter.
Therefore, the deoptimization context
additionally contains the program counter of the deoptimization point, the names and types of
local variables, and the types of the variables on the bytecode stack.
Deoptimization contexts are only comparable if they have the same deoptimization target, the
same names of local variables, the same number of values on
the operand stack, and a compatible deoptimization reason.
This means, for instance, that a deoptimization on a failing
typecheck is not comparable with a deoptimization on a failing dynamic
inlining, and thus we can't reuse the respective continuation.
Or, if there is an additional local variable that does not exist in the continuation context.
Comparable contexts are then sorted by the degree of
specialization. For instance, they are ordered by the subtype relation of the
types of variables and operands. If the continuation is compiled
for a state where \c{sum} is a number, then it can
for example be called when the variable \c{sum} holds an integer or a
floating-point number. Or, if we have a continuation for a typecheck, where we
observed a float vector instead of some other type, then this continuation
will be compatible when we observe a scalar float instead, as in R
scalars are just vectors of length one.

Dispatching is based on the execution states of the source code of the optimizer,
\eg in our case states of a bytecode interpreter. This does not mean that deoptless
requires these states to be materialized. For instance when dispatching on the type of
values that would be on the operand stack of the interpreter at the
deoptimization point, they are not actually pushed on the stack. Instead their
type is tested where they currently are in the native state.

\subsection{Potential and Limitations}

Deoptless does not add much additional
complexity over OSR-out and OSR-in to an implementation. There are some
considerations that will be discussed when we present our prototype in the next
section. Most prominently,
OSR-out needs to be more efficient than when it is used only for deoptimization,
because we expect to trigger OSR more frequently when dispatching to optimized
continuations.
Currently our proof-of-concept implementation is limited to handle
deoptimizations where the origin and target have one stack frame, \ie we do not
handle inlined functions. This is not a limitation of the technique, but rather
follows from the fact that also the OSR-in implementation currently has the same
limitation. We can therefore not answer how well deoptless would perform in the
case of inlined functions.

There are also a number of particular trade-offs, which are
already visible in the simple example in \autoref{fig:comparison}.
Going through
the four phases of the example, we can observe the
following. In the first phase both implementations warm up equally fast. There
is no difference, as there is also no deoptimization event up to this point. In the second phase,
when the type changes to float, the normal implementation triggers a
deoptimization, we fall back to the interpreter and it takes some time for the
code to be recompiled. This replacement code is more generic as it can handle
floats and integers at the same time and it is much slower than the float-only
case. The effect is inflated here due to the fact that our particular compiler
supports unboxing only if the types are static. This can be seen in the first
phase of the deoptless variant, where a specialized continuation for floats is
compiled and executed very efficiently. We see a small overhead over the integer
case, that is due to the dispatch overhead of deoptless.
Next, in the third phase, yet another specialized
continuation is compiled, this time for the \c{data} vector being a generic R
object. While we avoid going back to the interpreter yet again, this continuation is
slower at peak performance than the generic version from the normal execution.
This is not a fundamental limitation, but does exemplify a difficulty with
deoptless that we will get back to: deoptless operates on partial
type-feedback from the lower tier. Because the remainder of the \c{sum} function has never been
executed with the new type, we cannot fully trust the type-feedback when
compiling
the continuation, as it is likely stale to some extent. We address the problem with a selective
type-feedback cleanup and inference pass, which can, as in this case, lead to less optimal
code. In the final phase of the benchmark deoptless greatly outperforms the
normal implementation. That is because in deoptless we are running the same code
again as in the first phase, as this code was never discarded. On the other hand
in the normal case we replaced the sum function in-place and it is now much more
generic and slow.

Speculative optimizations are key for the performance of just-in-time compilers.
Deoptless presents a way of dealing with failing speculations
that does not tier down, \ie does not have to continue in a slower tier.
Instead of having functions become gradually more
and more generic on every deoptimization, we take this opportunity for
splitting and compile functions which become more and more specialized.
Our preliminary evaluation shows the big potential of the technique. As we will
show, when presented
with randomly failing assumptions, deoptless is able to execute benchmarks up to $\perfMax\times$ faster than with normal deoptimization, with most
benchmarks being at least $\perfMed\times$ faster and none slower.
As with every forward escape strategy, there is a danger of committing follow-up
mistakes. Deoptless struggles with cases where it is hard to infer from the
failing speculation how the remainder of the function will be affected, before
actually running it. We approach this problem by incorporating information from the current state
of the execution at the OSR exit point. Additionally, we use type-inference on the type-feedback
to override stale profile data. Our evaluation shows that this strategy is
robust and able to produce good code for the continuations.
