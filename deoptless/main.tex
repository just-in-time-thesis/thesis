Adding deoptless to a VM with an existing 
implementation of OSR-in and OSR-out requires only minimal changes. Starting with
the code in \autoref{lst:deopt-impl}, we extend it as shown in \autoref{lst:deoptless-impl}.
In this listing we see five functions that we'll detail to explain the
implementation. The \c{deoptlessCondition} decides if deoptless should be
attempted. Certain kinds of deoptimizations do not make sense to be handled, and also
our proof of concept implementation has limitations and is not able to handle
all deoptimizations. Then, \c{computeCtx} computes the current deoptimization
context and \c{dispatch} tries to find an existing continuation that is
compatible with the current context. The \c{recompile} function is our recompilation
heuristic that decides if a continuation, while matching, is not good enough.
Next, the \c{deoptlessCompile} function invokes the compiler to compile a new deoptless
continuation. Finally, we call the compiled continuation, directly passing the
current state. The calling convention is slightly different from normal OSR-in.
As we are originating from native code the values can have native representations, 
whereas if we originate from the interpreter all values are boxed heap objects.

\begin{figure}\begin{lstlisting}[style=C,label={lst:deoptless-impl},caption={Pseudocode for deoptless implementation}]
 Value deopt(FrameState* fs, Reason* r) {
   if (deoptlessCondition(fs, r)) {
     auto ctx = computeCtx(fs, r);
     auto fun = fs->fun->deoptless->dispatch(ctx);
     if (!fun || recompile(fun, ctx))
       fun = deoptlessCompile(ctx);
     if (fun)
       return fun(fs);
   }
   // Rest same as normal deopt
 }
\end{lstlisting}\vskip -0.8em\end{figure}

\paragraph{Conditions and Limitations}

As mentioned, deoptless is not applied to all deoptimization events. First of
all, some deoptimizations are rather catastrophic for the compiler and prevent most
optimizations. An example would be an R environment (the dynamic
representation of variable scopes) that leaked and was non-locally modified. Under
these circumstances the \R optimizer cannot realistically optimize the code
anymore.
% Is this too strong? We do have the call target change in volcano, and the original code
% can become valid if the function is assigned the original one again
Second, when global assumptions change, \eg a library
function is redefined, we must assume that the original code is permanently
invalid and should actually be discarded.
Furthermore, we also prevent
recursive deoptless. If a deoptless continuation triggers a failing
speculation, then we give up and perform an actual deoptimization.
There are also some cases which are
not handled by our proof of concept implementation. The biggest limitation is
that we do not handle cases where more than one framestate exists, \ie we
exclude deoptimizations inside inlined code. This is not an inherent limitation,
and we might add it in the future, but so far we have avoided the implementation
complexity.

\paragraph{Context Dispatch}

Deoptless continuations are compiled under an optimization context, which
captures the conditions for which it is correct to invoke the continuation. The
context is shown in \autoref{lst:deopt-context} in full.
It contains the deoptimization target, the reason, the types of values on the
operand stack, and the types and names of bindings in the environment. The deoptimization
reason represents the kind of guard that failed, as well as an abstract
representation of the offending value. For instance, if a type guard failed, then
it contains the actual type, if a speculative inlining fails, it contains the
actual call target, and so on.

\begin{figure}\begin{lstlisting}[style=C,label={lst:deopt-context},caption={Deoptless optimization context}]
struct DeoptContext {
  Opcode* pc;
  Reason reason;

  unsigned short stackSize;
  unsigned short envSize;
  Type stack[MAX_STACK];
  tuple<Name, Type> env[MAX_ENV];

  bool operator<= (DeoptContext& other);
};
\end{lstlisting}\vskip -0.8em\end{figure}

The (de-)optimization context is used to compile
a continuation from native to native code, so why does it
contain the \c{Opcode*  pc} field, referring to the bytecode instead? Let's reexamine 
\autoref{fig:osr_deoptless}. The state is extracted from native code and
directly translated into a target native state. However, logically, what connects
these two states is the related source state. For instance, the bytecode program
counter is used as an entry point for the \R compiler. The bytecode state is never
materialized, but it bridges the origin and target native states on both ends of deoptless.

Contexts are partially ordered by the \c{<=} relation. The relation is defined such that 
we can call a continuation
with a bigger context from a smaller current context. In other words, the
\c{dispatch} function from \autoref{lst:deoptless-impl} simply scans the
increasingly sorted dispatch table of continuations for the first one with a context \c{ctx'} such that
\c{ctx <= ctx'}, where \c{ctx} is the current context. The dispatch tables uses a
linearization of this partial order. The linearization currently does not favor a
particular context, should multiple optimal ones exist. For efficiency of the
comparison we limit the maximum number of elements on the stack
to 16 and environment sizes to 32
(states with bigger contexts are skipped), and only allow up to 5 continuations in the dispatch table.

\paragraph{Compilation and Calling Convention}

Compilation of deoptless continuations is performed by the normal \R optimizer
using the same basic facilities as are used for OSR-in. Additionally,
information from the \c{DeoptContext} is used to specialize the code further.
For instance, the types of values on the operand stack can be assumed stable by
the optimizer, since context dispatch ensures only compatible continuations are
invoked. The calling convention is such that the R environment does not
have to be materialized. The local R variables, which
are described by \c{Framestate} and \c{MkEnv} instructions at the
deoptimization exit point, are passed in a buffer struct.

\paragraph{Incomplete Profile Data}

An interesting issue we encountered is
incomplete type-feedback. As depicted in \autoref{fig:compare-deopt},
normally after a deoptimization event, the execution proceeds in the lower-tier, \eg in
the interpreter, which is also responsible for capturing
run-time profile data, such as type-feedback, branch frequencies, call targets,
and so on. When an assumption fails, this typically indicates that some of this
profile was incomplete or incorrect and more data is needed.
In deoptless we can't collect more data before recompiling, therefore we lack
the updated feedback. If we were to compile the
continuation with the stale feedback data, most
probably we would end up mis-speculating. For instance
if a typecheck of a particular variable fails, then the type-feedback for
operations involving that variable is probably wrong too. We
address this problem with an additional profile data cleanup and inference pass.

The cleanup consists of marking all feedback that is connected to the program
location of the deoptimization reason, or dependent on such a location, as
stale. Additionally we check all the feedback against the current run-time state
and mark all feedback that is contradicting the actual types.
Additionally, we insert available information from the
deoptimization context. For instance, if we deoptimize due to a typecheck,
then this step injects the actual type of the value that caused the guard to fail.
Finally we use an inference pass on the non-stale feedback to fill in the blanks.
For inference we reuse the static type
inference pass of \R, but run it on the type feedback instead
and use the result to update the expected type.
These heuristics work quite well for the evaluation in the next
section, however, it is possible that stale feedback is still present and causes
us to mis-speculate in the deoptless continuation, which leads to the function being
deoptimized for good.
