\chapter{Introduction}

Just-in-time compilers are omnipresent in today's technology stacks. The
performance of the code they generate is central to the growing adoption of
languages with dynamic features such as code loading, extensible
objects, monkey-patching, introspection, or eval.
These kind of features render precise static analysis impossible
and lead to a wide range of possible behaviors, even in seemingly benign programs.
To resolve this issue compilers
specialize programs at run-time according to
observed behaviors --- they identify likely invariants and optimize%
\footnote{By \emph{optimization} we generally refer to any behavior-preserving code
transformation, with the additional caveat that some compiler engineer deemed it
to improve some metric of performance of some code.}
assuming the invariants hold.
For instance,
\begin{itemize}
\item in prototype based languages classes of likely similar objects are
  identified using hidden classes \citep{cha89},
\item duck-typing and late bound call targets are made static by
  speculating on the stability of the dynamic call-graph and
  monomorphized by splitting \citep{hol91},
\item methods are specialized to the types of arguments or other dynamic properties \citep{bez18}
\item class hierarchies are speculated to be stable \citep{pal01},
\item dynamic types of variables assumed stable \citep{hol94a}.
\end{itemize}
%
Most optimizations in just-in-time compilers are based in some way on the premise
that, from the vast range of possible behaviors only some will be exercised by
any particular execution of a program.
Specializing code to those particular
behaviors has been fundamental in making dynamic languages practical in large
applications.
These kind of optimizations involve forming assumptions about likely
behaviors, that are expected to hold true at run-time. Typically the assumptions
are based on profiling data, gathered either from the current process or from
previous invocations. This is a common practice,
even utilized in ahead-of-time compilers, where it is known as profile guided
optimizations. The advantage in a just-in-time scenario is that
specialization can be explored lazily, generating only code that is actually
needed, in contrast to the ahead-of-time case, where the generated code must
handle all possible behaviors all the time.
A fundamental problem in just-in-time compilation is therefore
how to leverage dynamic analysis, \ie an incomplete recording of the past
behavior, for optimizations? In particular, how to speculate and specialize code
for likely invariants, while still safely and efficiently handling the case
where they turn out not to hold. The implementation thereof is often scattered
around different components of the language implementation; for instance,
devirtualization in a language with class loading relies on the interplay of collecting a dynamic call-graph in the runtime, optimizing under the assumption that it is stable and retiring code invalidated by the class loader, while handling the cases where the invalidated code is still being executed.

\section{Motivation}

In this area of run-time optimizations,
there is a lack of well-documented and re-usable techniques. Specialization is typically ad-hoc, tied to peculiarities of the language or implementation, and
often even distinct for different properties in the same system.
For instance the specialization approach taken by Julia~\citep{bez12} requires a language with multi-method dispatch,
the one by Truffle~\citep{wur13} relies on their language implementation style using self-specializing AST interpreters.
% Further, such approaches can quickly lead to a path-explosion problem; an issue
% compounded by many splitting approaches not supporting the
% specialized code being shared by different callers.
Speculative optimizations in particular are poorly documented and mistaken for an implementation detail on how
to rewrite stack frames.
%% There is no formal understanding of the mechanism and therefore we lack even basic tools to reason about correctness.
% What we found most lacking in previous work is a transferable and precise account of code specialization. For instance there is no model of speculative optimizations, which leans itself to verification. We are also unaware of an existing approach for specialization
% that allows for
% sharing of specialized code and is not tied to a particular language and implementation.
This gap in the literature leads to island-solutions for each language and a
reluctance by some language implementers to include speculation in their compiler.
The reluctance is the result of a poor understanding of the underlying program
transformations and missing off-the shelf techniques.

Speculation in particular is iconic for
just-in-time compilation. Having a compiler
available at run-time allows for multiple attempts at producing optimal code.
This allows for aggressive optimization strategies, where code is expected to be
wrongly optimized sometimes, but then subsequently and transparently replaced
by updated and fixed code.
Speculative optimizations stand out from other techniques by their ability to --- at arbitrary program locations ---
exclude some parts of the source code from the code being optimized. This is achieved by
guarding against unexpected behaviors with run-time checks and bailing out of the optimized code,
back to the source code, if these guards fail. It follows that speculative optimizations allow us to trim down
the vast range of possible behaviors and instead optimize and even analyze just for the expected ones.
It also means, that some form of recovery action is necessary in the unexpected case.
We refer to that action as deoptimization and it relies on some form of
execution state rewriting
by the underlying runtime, such as on-stack-replacement or stack-switching.
%% This style of speculative optimizations can be found in many JIT
%% compilers, such as V8, JSCore, Truffle/Graal, Cog.

A non-speculating specialization on the other hand involves upfront splitting of the control-flow on a property.
I will refer to them as contextual optimizations, as they specialize code to certain predicates over the program state.
%% In contrast to speculation, the dynamically checked property is
%% used for optimizations only as long as it statically holds. For instance if a function is specialized to
%% the dynamic type of an argument, then the type of the argument is propagated,
%% \eg by static and sound alias-analysis, from the invocation.
Often contextual optimizations
result in the duplication of the code being optimized.
Real-world examples include multi-method dispatching on
argument types in Julia, where each function signature discovered through
dispatch leads to a new function being compiled and
optimized. Or, optimizations involving tail-duplication, such as message
splitting in SELF, where
the control-flow within a function is split on the type of a result of a message send
to produce optimized continuations for expected dynamic types.

As a concrete example, consider a code fragment \code{S}, which is part of a lager
program, and shall be specialized to a certain runtime context. For instance the
fragment \code{S} could have a free variable \code{x} and the goal is to
provide a particularly fast implementation for, say, the cases where \code{x} is \code{42}.
A fragment could be a function body, but also a smaller or bigger piece of code.
Let's assume \code{S} is the following expression:

\begin{lstlisting}[style=R]
if (!is.numeric(x) || x == 0)
  error()
else
  1/x
\end{lstlisting}

\noindent
For a simple non-speculating optimization we start with a transformation to duplicate the fragment into
\code{if (x == 42) then S else S}. This allows us to optimize each clone of
\code{S} independently under the contextual information whether \code{x == 42}
holds. Within the then branch, the compiler can assume \code{x == 42} to be part
of the static optimization context, and conversely within the else branch
\code{x != 42}.

Now let us contrast this approach with a speculative optimization.
In that case we simply state \code{assert(x == 42); S}. For expressions dominated by the
assertion, the fact \code{x == 42} is again part of the static optimization context.
The case where that assumption does not hold, is not part of the code emitted by
such a compiler.

In summary, a speculative optimization means that the compiler can simply assume
a likely invariant at a certain program location, and the unexpected case falls outside the compilation unit. Under
speculative optimizations we also allow instructions being executed optimistically and work
being done that has to be discarded or even reverted if the assumption
fails. A non-speculative optimization on the other hand produces code
that cannot be invalidated. It is still possible to optimize for dynamic
properties, though the properties are used in a non-speculative way.

\subsection{Example}

%% Let us reiterate the dynamic specialization techniques with the example of \R, the just-in-time
%% compiler for the R language, that will be extensively covered later in
%% \autoref{chpt:implementation}.
%% It features a blend of speculative and non-speculative specializations.
Consider
the following vector access function in R
%
%\begin{figure}[h]
\begin{lstlisting}[style=R]
at <- function(x, y)
        x[[as.numeric(y)]]
\end{lstlisting}
%\end{figure}
%
which converts the \code{y} argument to a number and then uses that number as an
index into argument \code{x}.
%% \R employs specialization at call-sites using \cd. Contrarily to the simplistic
%% example in the introduction, \cd does not duplicate code up-front. Instead, new
%% specializations are explored lazily as they appear at run-time.
Assume we call the function with three different kinds of arguments as follows:
%
%\begin{figure}[h]
\begin{lstlisting}[style=R, numbers=left]
vec <- c(1,2,3)
# index is a scalar number
at(vec, 1)
# index is string
at(vec, "1")
# index type unknown at call-site
pos <- function() sample(1:3, 1)
at(vec, pos())
\end{lstlisting}
%\end{figure}

\medskip
\noindent
In the first two cases the type of the argument is known at the call site.
This does not hold for the third case, because R evaluates arguments by need;
\code{pos()} is only invoked, when the \code{y} argument is accessed for
the first time, which happens during the execution of \code{at}.
%
If we were to clone the function \code{at} and optimize it for different types of arguments,
we can specialize it for three distinct cases: \code{number[n] × number} for the
call-site at line 3,
\code{number[n] × string} at line 5, and \code{number[n] × any} at line 8.
The first two signatures directly lead to well optimized versions --- at the
source level they would look like: 
%
%\begin{figure}[h]
\begin{lstlisting}[style=R]
at_1 <- function(x, y) {
  if (1 <= y && y <= num_vec_len(x))
    num_vec_at(x, y)
  else error(...)
}
\end{lstlisting}
%\end{figure} %
%\begin{figure}[h]
\begin{lstlisting}[style=R]
at_2 <- function(x, y) {
  y <- as.numeric(y)
  if (is.na(y))
    NA
  else
    at_1(x, y)
}
\end{lstlisting}
%\end{figure} %

The third case does not lend itself to any useful
optimizations, since at the call-site it is unclear what the expression
\code{pos()} eventually returns (excluding inter-procedural analysis for now).
This is where speculation comes into play,
and type-feedback from previous runs can be employed to narrow down the
behavior to what we expect from the past. To make lazy evaluation specific,
we'll mark the position where arguments are evaluated with \code{force}.
After forcing the argument expression, we can then speculate on its type and
shape: %, as shown in \autoref{lst:jit-intro-example-c3}.
%
%\begin{figure}[h]
\begin{lstlisting}[style=R]
at_3 <- function(x, y) {
  y <- force(y)
  assume(scalar_int(y), in_bounds(x, y))
  num_vec_at(x, y)
}
\end{lstlisting}
%\end{figure}
%
In case our assumptions are wrong, the \code{assume} instruction is supposed to fall back to the source version of
this function. It allows us to ignore unlikely
cases, \ie in this case most of the implementation of vector access, which would have to deal with
different vector types, indexing modes and possible errors.
Of course for that to actually work, \code{assume} is more complicated than an
assertion and more meta-data is required than is shown here.
%% A semantic for
%% \code{assume} will be introduced in the next chapter.


\subsection{Questions}

%% Assume instruction + context dispatch....
%% 
%% Framework for writing a compiler....
%% 
%% Using dynamic information...
%% 
%% Injected through two different instructions...
%% 
%% Compiler uses information in sound way....
%% 
%% You get a performant, modern jit....

An efficient implementation of a dynamic language must use information only
available at run-time to optimize code. In particular this information is incomplete
and changes over time. There are fundamentally two approaches to this problem,
one is to speculate on likely properties and fall-back otherwise, the other is to
somehow split control-flow on properties, which are checked up-front. Both of these
approaches present problems and open questions.

\paragraph{Speculation}
The goal of speculation is, that a compiler can base optimizations not only on static facts, but
also on likely invariants. Should a speculation turn out to be incorrect for a
particular execution, then the optimized code is discarded and the execution
switches back to unoptimized code on-the-fly.
% This approach to optimization is born out of
% necessity --- in dynamic languages most properties of interest cannot be shown
% statically --- and occasion --- having a compiler available at runtime allows
% for an iterative approach to finding the most optimal code.
But, what does it
entail to bail out of wrongly optimized code that is currently being executed, \ie to
deoptimize code with active stack frames?
Program counters must be updated, optimized execution state rewritten into corresponding
unoptimized state, potentially switching from native to interpreted code, and some parts of the
unoptimized state might not even exist and have to be synthesized and
materialized.
%% In particular, if the invalid function was inlined into another function, it is necessary to synthesize a
%% new stack frame for the caller.
% Existing JIT compilers differ in their architecture and
% in the nature and extent of their reliance on speculation.
The mechanism is typically relegated to implementation details that are neither
clearly abstracted nor documented, and scattered around different levels of the
implementation. Is there a formal and transferable way to model speculation?
How can compiler correctness be stated in the presence of speculation?
When are two versions
compiled under different assumptions equivalent?
How do traditional
optimizations have to be adapted?  Does deoptimization inhibit optimizations?

\paragraph{Specialization}
There are situations where it is beneficial to optimize code by specializing it
to multiple scenarios.
For instance if profile data suggests that a particular
variable alternatively holds one of two unrelated dynamic types, then it makes
sense to split control on that type and optimize for the two continuations
separately.
In other words, the aim of specialization is to split code,
such that contextual information about the
current program state can be used for optimizations.
%% Again, a major advantage for just-in-time compilers is, that the contexts of
%% interest can be discovered dynamically and the control flow splits introduced
%% while the program is already executing.
Many existing splitting and customization techniques fall under this
category and implementations use different
approaches to generate code specialized to certain properties of the program
state. Given such a fragmented space, is there a unified or unifying technique to describe and implement specialization?
How can a language implementation be structured to benefit from specialization?
Can specialized code be shared between compatible uses and contexts? What does it mean for
contexts to be compatible? How does specialization interact with speculation?
How can the generation of specialized code be deferred as much as possible, such
that more dynamic information can be considered.

\section{Thesis}

This dissertation presents a framework for soundly injecting
assumptions into the optimizer of a just-in-time compiler.
First, speculative optimizations with deoptimization as fallback
mechanism are formalized in the \emph{\sourir model}.
Sourir's \emph{assume} instruction enables optimizations based on arbitrary
assumptions at any point.
Second, \cd provides a generic approach for splitting on properties checked at
run-time.
\emph{\CD} allows to optimize functions lazily, up to the actually encountered contexts of
assumptions, which act like static optimization contexts in an ahead-of-time
compiler.
I will defend the thesis that

\begin{quote}
  Assume and \cd provide the basis for optimizations based on run-time
  assumptions in a competitive just-in-time compiler.
\end{quote}

\noindent
To understand that statement I will briefly introduce the two models,
summarize their contributions, and explicit the claims.

\paragraph{Sourir}

%% Additionally, the existing literature
%% does not provide a model for how speculation and deoptimization interacts
%% with compiler optimizations. To address these issues,
The \sourir model of speculation, presented in \autoref{chpt:sourir}, is a simple abstraction for speculative
optimizations that allows for formal reasoning on speculation at the IR
level. It makes the following contributions:
\begin{itemize}
\item a semantic for deoptimization points, assumptions,
  deoptimization metadata, and the key invariants required for speculative
    optimizations;
\item correctness proofs for speculative optimizations and classical
  optimizations in the presence of speculation; and
\item the \assume instruction for speculation that can be easily integrated in a compiler IR.
\end{itemize}
%
%% %
%% \begin{itemize}
%% \item deoptimization metadata can be incrementally refined from a starting point that is correct by construction,
%% \item equivalence for speculatively optimized programs must include the deoptimization mechanism and the unoptimized code,
%% \item optimizations can be applied without analyzing the code at the other end of a deoptimization point.
%% \end{itemize}

\paragraph{Context Dispatch}

%% Examples for contextual information include the type and shape of function arguments,
%% the absence of reflection, that a library function is not overloaded, and more.
Specialization by \cd, introduced in \autoref{chpt:cd}, unifies how a runtime system can exploit contextual information and provides
a simple approach to structure a just-in-time compiler around code specialization.
It allows a compiler to specialize fragments of code up to a context of assumptions,
and the specialized fragments to be shared between compatible contexts.
The contributions are how to
%
\begin{itemize}
\item add dynamic information to the static optimization context of a compiler
and combine static and dynamic analysis for specialization;
\item avoid over-specializing and support sharing of code; and
\item efficiently dispatch to an optimal version under the current program state.
\end{itemize}
%
\Sourir and \cd complement each other. I will also introduce a combined
\emph{deoptless} deoptimization strategy using \cd.
%
%% Context dispatch provides the means to keep several differently specialized function versions
%% and then dynamically select a good candidate, given the dynamic context at the
%% call-site.

\paragraph{Claims}

This thesis presents \R, a bug-compatible implementation of the R language with
a JIT compiler.
R's dynamic nature and lazy evaluation strategy requires the optimizer to be
able to optimistically speculate and specialize. The implementation effort
is presented in \autoref{chpt:implementation}, which also covers those particularities
of the R language that make it particularly resilient to optimizations.
\R uses a \cd system,
and the optimizing compiler in \R has an IR that is based closely on \sourir.
\R shows that these two pieces of theory provide blueprints for building a
language implementation with competitive performance.
In particular in \R the following claims are validated:
\begin{itemize}
\item The two approaches provide specializations on dynamic information in all
  important situations.  The optimizing compiler in \R uses dynamic information only in the form of
    \assume instructions or optimization contexts from \cd. This claim is
    substantiated in \autoref{chpt:implementation}, which describes how \sourir
    and \cd translate into the concrete implementation and how this
    implementation relies only on these two mechanisms for incorporating dynamic information.
\item The optimizer is competitive. It significantly outperforms the \R bytecode
  interpreter, the GNU R bytecode interpreter, and is comparable to Oracle's JIT
    compiling FastR,
    but with faster warmup behavior. The performance evaluation in
    \autoref{chpt:eval} shows which assumptions and contexts are required for \R
    to speed up R programs.
\end{itemize}

\paragraph{Non-Claims}

I want to stress upfront that \sourir and \cd do not cover the
entirety of the implementation.
This dissertation focuses on the optimizing compiler.
In particular the main focus is on the middle-end of
a JIT and optimizations on its IR as enabled by dynamic information.
Other details are important for a good implementation, such as efficient profile
collection, a fast garbage collector, a native backend, or a good interpreter.
\Sourir and \cd co-evolved with \R
and were instrumental in my own understanding of key aspects of JIT compilation.
The actual implementation goes beyond these building blocks --- on the one hand
it features extensions for practical reasons,
and on the other hand many more parts are necessary for a complete language
implementation. These extensions are discussed less formally, when presenting the implementation.
A complete model and potentially a verified JIT implementation remain future
work.

\subsection{Structure}

This thesis is structured as follows. The two main contributions are presented in
\autoref{chpt:sourir} and \autoref{chpt:cd}. The former presents a formalization
of speculative optimizations and discusses the relationship of the model with
implementations. The latter focuses on specialization using \cd and related
applications. To validate my claims \autoref{chpt:implementation} presents
how these contributions are usable in a real-world implementation called \R and
\autoref{chpt:eval} shows that \R is competitive. \autoref{chpt:conclusion}
concludes and presents future work.

\subsection{Publications}

\noindent
The \R virtual machine is available and developed as free software at
{\href{https://r-vm.net}{\v{r}-vm.net}}.

\bigskip

\noindent
The text of this dissertation is based or borrows from the
following peer-reviewed publications:
%
\begin{itemize}
  \item \citework{popl18}{POPL}
  \item \citework{dls19}{DLS}
  \item \citework{dls20}{DLS}
  \item \citework{oopsla20c}{OOPSLA}
  \item \citework{popl21}{POPL}
  \item \citework{pldi22}{PLDI}
\end{itemize}


\bigskip
\noindent
The experimental setup for various performance results can be reproduced by
the following artifacts:

\begin{itemize}
  \item \citework{flu20-artifact}{OOPSLA}
  \item \citework{flu22-artifact}{PLDI}
\end{itemize}


