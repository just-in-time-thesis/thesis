\section{Background}

The R language presents interesting challenges for
implementers.  R is a dynamic imperative language with vectorized
operations, copy-on-write of shared data, a call-by-need evaluation
strategy, context-sensitive lookup rules, multiple dispatch, and first-class
closures.  A rich reflective interface and a permissive native interface
allow programs to inspect and modify most of R's runtime structures.  This
section focuses on the interplay of first-class, mutable environments and lazy
evaluation. 
In particular, we focus on their impact on compiler optimizations.

One might see the presence of \c{eval} as the biggest obstacle for static
reasoning.  With \c{eval}, text can be turned to code and perform arbitrary
effects.  However, the expressive power of \c{eval} can be constrained by
careful language design. Julia, for instance, has a reflective interface
that does not hamper efficient compilation.  Even an
unconstrained \c{eval} is bound by what the language allows; for example,
most programming languages do not allow code to delete a variable.  Not so
in R.  Consider one of the most straightforward expressions in any language,
variable lookup:

\begin{lstlisting}[style=R]
f <- function(x) x
\end{lstlisting}

\noindent In most languages, it is compiled to a memory or register access.
From the point of view of a static analyzer, this expression usually leaves
the program state intact.  Not so in R.
Consider a function doubling its argument:
\begin{lstlisting}[style=R]
g <- function(x) x+x
\end{lstlisting}

\noindent In most languages, a compiler can assume it is equivalent to
\c{2*x} and generate whichever code is most efficient. At the very least,
one could expect that both lookups of \c{x} resolve to the same variable.
Not so in R.

Difficulties come from two directions at once.  R variables are bound in
environments, which are first-class values that can be modified.  In
addition, arguments are evaluated lazily; whenever an argument is accessed
for the first time, it may trigger a side-effecting computation -- which could
modify any environment.  Consequently, to optimize the body of a function, a
compiler must reason about effects of the functions that it calls,
as well as the effects from evaluating its arguments.
In the above example, \c{`+`} could walk up the call stack and
delete the binding for variable~\c x.  One could also call \c g with an
expression that deletes \c x and causes the second lookup of \c x to fail.
While unlikely, a compiler must be ready for it. Considering these
examples in combination with \c{eval}, it is impossible to statically resolve the binding
structure of R programs.

\R's optimizing compiler has a custom intermediate representation for R, called
\PIR. This IR is in single-static assignment form~\citep{ros88} and has explicit support for environments
and lazy evaluation.
In our experience, some of the
most impactful optimizations are high-level ones that require understanding
how values are used across function boundaries. We found that the GNU R
bytecode was too high level; it left too many of the
operations implicit.  In contrast, we found LLVM's IR too low
level for easily expressing some of our target optimizations.

\subsection{Environments in R}

Inspired by Scheme and departing from its predecessor S, R adopted a lexical
scoping discipline~\citep{gen00}. Variables are looked up in a list of
environments. Consider this snippet:

\begin{lstlisting}[style=R]
g <- function(a) { 
    f <- function() x+y         
    if (a) x <- 2
    f() 
}
y <- 1
\end{lstlisting}

\noindent The evaluation of \c{x+y} requires finding \c x in the enclosing
environment of the closure \c{f}, and \c y at the top level.  It is worth
pointing out that, while R is lexically scoped, the scope of a free variable
cannot be resolved statically. For instance, \c x will only be in scope in
\c g if the argument \c a evaluates to true.  

R uses a single namespace for functions and variables. Environments are used
to hold symbols like \c +.  While primarily used for variables, environments
can also be created explicitly, \eg to be used as hashmaps.  Libraries are
loaded by the \c{attach()} function that adds an environment to the list of
environments.  A number of operations allow interaction with environments:
\c{environment()} accesses the current environment; \c{ls(...)} lists bound
variables; \c{assign(...)} adds or modifies a binding; and \c{rm(...)}
removes variables from an environment.  R has functions to walk the
environment chain: \c{parent.frame()} returns the environment associated
with the caller's call frame and \c{sys.frame(...)}  provides access to the
environment of any frame on the call stack. In R, frames represent function
invocations and they have references to environments. Consider this code:

\begin{lstlisting}[style=R]
f <- function() get("x", envir=parent.frame())
g <- function() {x <- "secret"; f()}
\end{lstlisting}

\noindent Function \c{f} uses reflection to indirectly access
\c g's environment.  This illustrates that any callee may access (and
change) the caller environment.

\subsection{Laziness in R}

Since its inception, R has adopted a call-by-need evaluation strategy (also
called lazy evaluation).  Each expression passed as argument to a function
is wrapped in a \emph{promise}, a thunk that packages the expression, its
environment, and a slot to memoize the result of evaluating the expression.  A promise is only
evaluated when its value is needed.  Consider a
function that branches on its second argument:

\begin{lstlisting}[style=R]
f <- function(a, b) if(b) a
\end{lstlisting}

\noindent A call \c{f(x<-TRUE,x)} creates two promises, one for the
assignment \c{x<-TRUE}, and one to read \c x.  One could expect this call to
return \c{TRUE}, but this is not so.  The condition is evaluated before
variable \c x is defined, causing an error to be reported.  Combined with
promises, the \c{sys.frame} function allows non-local access to environments
during promise evaluation:

\begin{lstlisting}[style=R]
f <- function() sys.frame(-1)
g <- function(x) x
g(f())
\end{lstlisting}

\noindent Here \c{g} receives promise \c{f()} as argument.  When the promise
is forced, there will be three frames on the stack: frame~\c{0} is the global
scope, frame~\c{1} is \c{g}'s, and frame~\c{2} is \c{f}'s frame.

\bigskip
{\small
\ifx\HCode\undefined
\begin{tikzpicture}[stack/.style={
   rectangle split, rectangle split parts=3, draw, anchor=west, text width=5cm}]

\node [stack] (t) {
  \nodepart{one}
  \c{0:  g(f())}
  \nodepart{two}
  \c{1:  x}
  \nodepart{three}
  \c{2:  sys.frame(-1)}
};

\end{tikzpicture}
  \else
    \includegraphics{rsh/figures/stack.png}
  \fi
}
\bigskip

\noindent
During promise evaluation, \c{parent.frame} refers to the frame where the
promise was created (frame~\c{0} in this example, as promise \c{f()} occurs at
the top level).  But, \c{sys.frame(-1)} accesses a frame by index,
ignoring lexical nesting, thus extracting the environment of the forcing
context, \ie the local environment of \c g at frame~1.

We leave the reader with a rather amusing brain twister.  
R has context-sensitive lookup rules for variables in call position.
Variables that are not bound to functions are skipped:

\begin{lstlisting}[style=R]
f <- function(c) {c(1, 2) + c}
f(3)
\end{lstlisting}
 
\noindent
The lookup of \c c in \c{c(1,2)} skips the argument \c c, since it is not a
function.  Instead, primitive \c{c()} is called to construct a vector.  The
second read of \c c is not in call position, thus it returns argument \c c,
\c{3} in this case.  The result is the vector \c{[4,5]} as addition is
vectorized.  Now, consider the following variation:

\begin{lstlisting}[style=R]
bad <- function()
  rm(list="c", envir=sys.frame(-1))
f(bad())
\end{lstlisting}

\noindent
This time, evaluation ends with an error as we try to add a vector and a
function.  Evaluation of \c{c(1,2)} succeeds and returns a vector.  But,
during the lookup of \c c for that call, R first encounters the argument \c
c. In order to check if \c c is bound to a closure, it evaluates the
promise, causing \c{bad()} to delete the argument from the environment. On
the second use of \c c, the argument has been removed and a function object,
\c c, is returned.

\subsection{Obstacles for Optimizers}\label{sec:headache}

A realistic language implementation helps to test models against reality.
We can assess the interplay of optimizations and if they work in a language with complex features.
The R language presents interesting challenges for
implementers.
R's rich reflective interface and a permissive native interface
allow programs to inspect and modify most of R's runtime structures. A particular
challenge comes from the interplay of first-class, mutable environments and lazy
evaluation.
The list of challenges for optimizing R is too long to detail. We restrict
the presentation to seven headaches.

\begin{enumerate}
\item \textsf{Out of order}: A function can be called with a named list of
  arguments, thus the call to \c{add(y=1,x=2)} is valid, even if arguments \c x
  and \c y are out of order.  {Impact:} To deal with this, GNU R
  reorders its linked list of arguments on every call.  \smallskip

\item \textsf{Missing}: A function can be called with fewer arguments than
  it defines parameters.  For example, if function \c{add(x, y)} is called
  with one argument, \c{add(1)}, it will have a trailing missing
  argument. While the calls \c{add(,2)} and \c{add(y=2)} have an explicitly
  missing argument for \c{x}. These calls are all valid.  {Impact:} If
  the missing parameters have default values, those will be
  inserted. Otherwise, the implementation must report an error at the point
  a missing parameter is accessed.  \smallskip

\item \textsf{Overflow}: A function can be called with more arguments than
  it defines parameters.  {Impact:} The call sequence must include a
  check and report an error.  \smallskip

\item \textsf{Promises}: Any argument to a function may be a thunk (promise
  in R jargon) that will be evaluated on first access. Promises may contain
  arbitrary side-effecting operations.  {Impact:} A compiler must not
  perform optimizations that depend on program state that may be affected by
  promises. \smallskip

\item \textsf{Reflection}: Any expression may perform reflective operations
  such as accessing the local variables of any function on the call
  stack. {Impact:} The combination of promises and reflection requires
  implementations to be able to provide a first-class representation of
  environments.  \smallskip

\item \textsf{Vectors}: The most widely used data types in R are
  vectorized.  Scalar values are vectors of length one.  {Impact:}
  Unless it can prove otherwise, the implementation must assume that
  values are boxed and operations are vectorized.  \smallskip

\item \textsf{Objects}: Any value, even an integer constant, can have
  attributes. Attributes tag values with key-value pairs which are used,
  among other things, to implement object dispatch.  {Impact:} The
  implementation must check if values have a class attribute, and, if so,
  dispatch operations to the methods defined to handle them.
\end{enumerate}

\noindent It is noteworthy that none of the above obstacles can be
definitely ruled out at compile time. Even with the help of static program
analysis, these properties depend on the program state at the point a
function is called.  This should come as no surprise after
having discussed reflective environment access. There are however much more
mundane parts of the language with an equally surprising dynamic behavior.
To illustrate this, consider the number of arguments
passed to a function. The following code calls \c{add()} twice, once with a
statically known number of arguments and the second time with the result of
expanding the \emph{varargs} parameter:
\begin{lstlisting}[style=R,linewidth=0.945\linewidth]
g <- function(...) add(1,3) + add(...)  
\end{lstlisting}

\noindent
The triple dots expand at run time to the list of arguments passed into
\code{g}. Thus, to know the number of arguments of \c{add} requires knowing
the number of arguments of \c{g}. The following are all legal invocations:
\begin{lstlisting}[style=R,linewidth=0.945\linewidth]
g(); g(1); g(,1); g(1,2,3); g(b=1, a=2);
g(..., a=1);
\end{lstlisting}

\noindent
Further, implementations of the \code{add} function can exert the full name-based
argument matching facilities, having arguments dynamically matched based on the names of arguments passed to
\code{g}.
