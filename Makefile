SHELL := /bin/bash

LATEXMK := $(shell command -v latexmk 2> /dev/null)
LATEX   := lualatex
TEXFILE := main

TEXFILES := $(shell find . -maxdepth 2 -type f -regex ".*\.tex")

BIB    := bib/pir.bib

all: main.pdf

$(TEXFILE).pdf: $(TEXFILE).tex $(TEXFILES) $(BIB)
	make -C sourir
	make -C cd
	make -C rsh
ifndef LATEXMK
	$(LATEX) $<
	bibtex $(subst .tex,.aux,$<)
	$(LATEX) $<
	$(LATEX) $<
else
	latexmk -lualatex $<
endif

html/main.html: $(TEXFILE).pdf
	make4ht -u --lua $(TEXFILE).tex "pic-align" --shell-escape -d html
	sed -i "s/font-family: sans-serif/font-weight: 300; font-family: 'Poppins', sans-serif/" html/main.css
	sed -i "s/font-family: monospace/font-weight: 400; font-family: 'Source Code Pro', monospace/" html/main.css

warnings:
	$(LATEX) main | grep -E '(W|w)arning'

watch:  all
	evince main.pdf&
	while true; do inotifywait $(TEXFILES) $(BIB) && make; done

clean:
	rm -f *.aux *.blg *.bbl *.cut *.log *~ *.out *.pdf
	rm -rf html

spellcheck:
	hunspell -d en_US -p hunspell_dictionary *.tex */*.tex

%.png: %.tex
	lualatex --output-directory=$(shell dirname $<) $<
	convert -density 600 $(basename $<).pdf -quality 95 $@

figures: cd/figs/cd-contexts.png cd/figs/cd-example.png cd/figs/cd-order.png cd/figs/cd-example-split.png sourir/tikz/sim1.png sourir/tikz/sim2.png sourir/tikz/sim3.png corejit/tikz/sim.png rsh/figures/stack.png

.PHONY: clean spellcheck warnings figures $(PHONY)
