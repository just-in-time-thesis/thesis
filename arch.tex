\R is a just-in-time compiler that plugs into the GNU R environment and is
fully compliant with the language's semantics. It passes all GNU R
regression tests as well as those of recommended packages with only minor
modifications.\footnote{Some error messages which are not fully stable in GNU R
either can change and some tests need a longer timeout to accommodate compile times.} \R
follows R's native API which exposes a large part of the language run-time
to user-defined code. It is also binary compatible in terms of data
structure layout, even though this is costly as GNU R's implementation of
environments is not efficient.
%% The hooks required to load \R are small
%% enough to be easily ported to newer releases. 
%% Compliance is checked
%% automatically at each commit.

\begin{figure}
  \centering
  \includegraphics[width=0.85\linewidth]{../profiling/images/r_pipeline.png}
  \caption{\R compilation pipeline}
  \label{fig:rPipeline}
\end{figure}
As
shown in \autoref{fig:rPipeline}, \R features a
two-tier optimization pipeline with a bytecode called RIR, an optimizing
compiler with a custom, SSA based intermediate representation called PIR, and an LLVM based
native backend. For some unsupported operation the VM can also fall back to the
GNU R AST interpreter.

%% We establish baseline performance of
%% \R with \cd by comparing it to the GNU R bytecode interpreter~\cite{tie19}
%% and the FastR just-in-time compiler~\cite{sta16}.  GNU R is the reference
%% implementation of the R language with limited optimization opportunities,
%% while FastR is built leveraging a high-performance optimizing compiler
%% running on top of a JVM.
%% \R has
%% competitive performance. It is faster than GNU R
%% ($\resultoverallGnurMinRounded$--$\resultoverallGnurMaxRounded\times$,
%% average $\resultoverallGnurMedRounded\times$) and slower than FastR
%% ($\resultoverallFastrMinRounded$--$\resultoverallFastrMaxRounded\times$,
%% average $\resultoverallFastrMedRounded\times$). Unlike FastR, \R's
%% performance is never significantly worse than GNU R.
%
Source code is translated to RIR, which is then interpreted. For hot functions, RIR is
translated PIR. Optimizations such as global value numbering, dead store
and load removal, hoisting, escape analysis, and inlining are all performed
on PIR code. It is
noteworthy that many of the optimizations in \R are also provided by LLVM.
However, in PIR they can be be applied at a higher level.  For instance,
function inlining is complex due to first-class environments and therefore
impossible without specific support for them. There are
also R specific optimizations, such as scope resolution, which lowers local
variables in first-class environments to PIR registers; promise inlining; or
optimizations for eager functions.

\R relies on speculative optimizations. Profiling information from previous
runs is used to speculate on types, on shapes (scalars vs. vectors), on the
absence of attributes, and so on.
The RIR interpreter gathers type feedback that is later used for
optimization decisions. To that end there are several recording bytecodes that
can be used to track values, types, call targets and branch targets. This profiling
information is then used to annotate PIR values in the optimizer, and consumed
by speculative optimization passes.
\R also performs speculative dead code
elimination, speculative escape analysis to avoid materializing
environments, and speculative inlining of closures and builtins.
Speculation is orthogonal to \cd; every version of a function can have its
own additional speculative optimizations.
When speculation fails, a deoptimization mechanism
transfers execution back to the RIR interpreter.
The design of speculative
optimizations is based on \sourir. As a novel feature, PIR
allows scheduling of arbitrary instructions, such as allocations, only
on deoptimization.
