\subsection{Results}\label{sec:results}

In this section, we assess scope resolution and promise inlining as means to
statically resolve bindings and reduce the number of environments and
promises needed by R programs.  To do so, we present three
experiments,%
each designed to answer one of the following questions:

\begin{itemize}
\item[{RQ1}] \emph{What proportion of function definitions do not require an environment after optimizations?}
\item[{RQ2}] \emph{What proportion of function invocations do not require an environment after optimizations?}
\item[{RQ3}] \emph{What is the performance impact of scope resolution and promise inlining on the rest of the optimizations?}
\end{itemize}


\paragraph{Methodology.}
Measurements are gathered using \R.  Our first and second experiments rely
on instrumenting the compiler to record information about code being compiled
and also dynamic counters of events happening at runtime. For the last
experiment, which looks at the impact of optimizations, we selected programs
whose performance was impacted by \R. To gather the measurements, we ran 5
invocations with 15 iterations of each benchmark on an Intel i7-3520M CPU,
stepping 9, microcode version 0x21, a clock pinned at 1.2 GHz, on a Fedora
28 running Linux 5.0.16-100, with SpeedStep and lower C-States disabled.
Our compiler has not been written for speed or optimized, so we discard the
first 5 iterations to amortize compilation time.%
\footnote{The experiments are published in runnable form at
  \url{gitlab.com/rirvm/rir\_experiments/container\_registry}. All reported
  numbers are for revision:\\ \tiny
  dba88e9bc417325a29c91acb088df7fe8109ca39e427e03931114e0715513bfafcd59a267812dcb1.}
We do not provide results
for real-world applications, as our system is not ready to compete with
mature R implementations.

\subsubsection{Static Environment Reduction}

\begin{figure}[!t]
\fbox{\begin{minipage}{0.45\textwidth} \small\raggedright \textbf{Code
      usage} is \c{check_code_usage_in_packages}, an analysis function
    shipped with GNU R. \textbf{Demos} runs all demos from the base
    packages.  \textbf{Tcltk}, \textbf{Stats}, and \textbf{Utils} run all
    examples included, respectively, in the \c{tcltk}, \c{stats} and
    \c{utils} packages.  \textbf{Pidigits} is from the language shootout
    benchmarks.  \textbf{Mandelbrot} is from the
    \emph{are-we-fast} benchmarks~\citep{mar16}.
\end{minipage}}
\skip -0.5em
\caption{Description of benchmarks for {RQ1} and {RQ2}.}
\label{fig:example-progs}
\end{figure}

For {RQ1}, we count the number of {\MkEnv}s in compiled code.  We
distinguish between stubs and standard environments.  Note that we ignore
the environments created in deoptimization branches to allow transferring
from optimized code to the baseline version.  \autoref{fig:example-progs}
lists the programs analyzed. \autoref{fig:static-elision} lists the number
of closures that are compiled (Closures), the percentage of
closures which have environments (Env), the percentage of closures that use
stubs (Stub), and the percentage of closures that have no \MkEnv instruction (No
env).  Stubs are inserted when our analysis determines that an environment
is only accessible through reflection.  Adding Stub and No Env, between 12\%
and 65\% of environments are elided.

\begin{figure}[!h]\small\begin{tabular}{l r r r r}
  Program & Closures & Env & Stub & No env \\ \hline
  Code usage & 971  & 82\% & 1\% & 16\% \\
  Demos      & 381  & 81\% & 16\% &3\% \\
  Tcltk      & 18   & 83\% & 11\% &5\% \\
  Stats      & 871  & 85\% & 13\% &2\% \\
  Utils      & 471  & 88\% & 10\% &2\% \\
  Pidigits   & 139  & 79\% & 4\% & 17\% \\
  Mandelbrot & 14   & 36\% & 29\% &36\% \\
\end{tabular}
\caption{Ratio of statically elided environments}\label{fig:static-elision}
\end{figure}

\subsubsection{Dynamic Environment Reduction}

To answer {RQ2}, we counted {\MkEnv}s executed at runtime with and
without optimizations.  \autoref{fig:dynamic-elision} lists the number of
environments allocated in the non-optimized version (Baseline), the
percentage reduction in allocated environments (Reduction), and the
percentage of the reduction that is due to stubs (Stubbed). For instance,
consider a program where the baseline allocates 100 environments and the optimized version only
50, with 10 stubs. This means we reduced the number of
environments by 50\%, and 20\% of that reduction is achieved by stubbing.
The data suggest that, for our benchmarks, 28\% to 87\% fewer environments
were created.

\begin{figure}[!h]\small
\begin{tabular}{l r r r r}
  Program & Baseline & Reduction & Stubbed \\
  \hline
  Code usage     & 2445996 & 36\% & 2\% \\
  Demos          & 192772  & 28\% & 5\% \\
  Tcltk          & 1271    & 43\% & 3\% \\
  Stats          & 3046614 & 28\% & 7\% \\
  Utils          & 2792534 & 33\% & 2\% \\
  Pidigits       & 9032031 & 31\% & 2\% \\
  Mandelbrot     & 8460641 & 87\% & 8\% \\
\end{tabular}
\caption{Reduction in environments allocated}\label{fig:dynamic-elision}
\end{figure}

\subsubsection{Effects on Optimizations}

Finally, we address {RQ3} by providing slowdowns between \R with and
without optimizations. \R performs traditional optimizations, such as
constant folding, dead code elimination, and global value numbering.
We posit that those optimization are helped by scope resolution and promise
inlining.
To test this hypothesis, we measure the impact of disabling those
passes on running time.
We run in four configurations: (1) default, (2)
without promise inlining,
\begin{figure}[b]\begin{centering}
\includegraphics[width=0.47\textwidth]{figures/scope_resolution.png}
\caption{Slowdown with promise inlining (orange), scope resolution (purple), or all
optimizations (white) turned off.}\label{fig:perf}\end{centering}
\end{figure}
(3) without scope resolution, and (4) with no
optimizations. \autoref{fig:perf} shows slowdowns relative to configuration
(1).  
Disabling all optimizations slows down code by a range from
2\% to 32\%. Much of this slowdown can be attributed to the scope resolution
pass, which when turned off, slows down execution from -1\% to
55\%. Disabling promise inlining has a smaller effect, ranging from a 0\% to
a 35\% slowdown.  Looking at the programs individually, we observe that
\c{Mandelbrot} is surprising in that turning off scope resolution generates
code that is slower than with no optimizations at all.  A reason is that in
this configuration, we still create stub environments and guards, while
{scope resolution} would be the main consumer of this speculation. Here they
only add overheads for the additional deoptimization points.  Disabling
scope resolution contributes significantly to the slowdown in
\c{Fastaredux_naive}.  \c{Storage}, \c{fannkuch_redux}, \c{fasta},
\c{fasta_2}, \c{fasta_naive} all have the expected behavior. As for
\c{binarytrees} and \c{binarytrees_naive}, they show little slowdown in any
configuration.



